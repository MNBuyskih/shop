<?php
/**
 * index.php
 * Date: 29.07.13
 * Time: 14:03
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 */

require_once "../protected/library/App.php";
$app           = App::createApplication(realpath(dirname(__FILE__) . '/protected/config/install.php'));
$app->basePath = realpath(dirname(__FILE__) . '/../');
$app->baseUrl  = str_replace(App::documentRoot(), '', $app->basePath);
$app->run();