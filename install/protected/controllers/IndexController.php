<?php

/**
 * IndexController.php
 * Date: 29.07.13
 * Time: 14:14
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 */
class IndexController extends Controller {

    public $mainTitle = "Установка";

    public $pageTitle = "Установка";

    public function actionIndex() {
        $this->render('index');
    }

    public function actionDb() {
        App::import('/install/protected/models/DbForm');
        $model = new DbForm();

        if (isset($_POST['DbForm'])) {
            $model->setAttributes($_POST['DbForm']);
            if ($model->save()) {
                $this->redirect('/install/index/ref');
            }
        } else {
            $model->host = "localhost";
        }

        $this->render('db', array('model' => $model));
    }

    public function actionRef() {
        App::import('/install/protected/models/RefForm');
        $model = new RefForm();

        if (isset($_POST['RefForm'])) {
            $model->setAttributes($_POST['RefForm']);
            if ($model->save()) {
                $this->redirect('/install/index/password');
            }
        }

        $this->render('ref', array('model' => $model));
    }

    public function actionPassword() {
        App::import('/install/protected/models/PasswordForm');
        $model = new PasswordForm();

        if (isset($_POST['PasswordForm'])) {
            $model->setAttributes($_POST['PasswordForm']);
            if ($model->save()) {
                $this->redirect('/install/index/setup');
            }
        }

        $this->render('password', array('model' => $model));
    }

    public function actionSetup() {
        App::import('/install/protected/models/Setup');
        $model = new Setup();
        if ($model->execute()) {
            $this->render('setup', array('model' => $model));
        } else {
            $this->render('setupError', array('model' => $model));
        }
    }

    public function actionRewrite() {
        echo "Ok";
    }
}