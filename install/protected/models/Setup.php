<?php
/**
 * Setup.php
 * Date: 29.07.13
 * Time: 16:25
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 */
class Setup extends Model {
    public $state;

    public $password;

    public $ref;

    public $key;

    public function execute() {
        App::import('/install/protected/models/DbForm');
        $db = new DbForm();
        $db->restore();
        $connection = $db->connect();

        if ($connection instanceof DbException) {
            $this->addError('state', 'Не возможно подключится к базе данных. Повторите процесс установки сначала');

            return false;
        }

        App::import('/install/protected/models/PasswordForm');
        $password = new PasswordForm();
        $password->restore();
        $this->password = $password->password;

        App::import('/install/protected/models/RefForm');
        $refForm = new RefForm();
        $refForm->restore();
        $this->ref = $refForm->ref;
        $this->key = $refForm->key;

        $this->saveConfig($db, $connection);

        if ($db->truncate) {
            $connection->query("DROP TABLE `brands`, `categories`, `doc`, `menu_items`, `products`, `product_fotos`, `properties`, `properties_values`, `update_history`, `variants`;");
        }
        $this->saveDb($connection);

        App::app()->closeSession();

        return true;
    }

    private function saveConfig(DbForm $db, Db $connection) {
        $file       = App::app()->basePath . '/protected/config/db.php';
        $data       = file_exists($file) ? require $file : array();
        $data['db'] = array(
            'class'    => get_class($connection),
            'user'     => $db->username,
            'password' => $db->password,
            'host'     => $db->host,
            'db'       => $db->dbName,
        );

        $data['admin'] = md5($this->password);
        $data['ref']   = array(
            'ref' => $this->ref,
            'key' => $this->key,
        );

        $data = var_export($data, true);
        $data = "<?\nreturn $data;";

        file_put_contents($file, $data);
    }

    private function saveDb(Db $connection) {
        $connection->query("CREATE  TABLE IF NOT EXISTS `doc` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(45) NOT NULL ,
  `alias` VARCHAR(45) NOT NULL ,
  `text` TEXT NULL ,
  `keywords` TEXT NULL ,
  `addMain` SMALLINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `alias_UNIQUE` (`alias` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `menu_items` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(45) NOT NULL ,
  `url` VARCHAR(250) NOT NULL ,
  `position` INT NOT NULL ,
  `doc_id` INT NULL ,
  `blank` SMALLINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `doc_id_UNIQUE` (`doc_id` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `update_history` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `date` INT NOT NULL ,
  `status` SMALLINT(1) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `brands` (
  `brand_id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `url` VARCHAR(45) NULL ,
  `meta_title` VARCHAR(250) NULL ,
  `meta_keywords` TEXT NULL ,
  `meta_description` TEXT NULL ,
  `description` TEXT NULL ,
  `delete` SMALLINT(1) NULL DEFAULT 0 ,
  UNIQUE INDEX `brand_id_UNIQUE` (`brand_id` ASC) ,
  PRIMARY KEY (`brand_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `categories` (
  `category_id` INT NOT NULL ,
  `parent` INT NULL ,
  `name` VARCHAR(45) NULL ,
  `single_name` VARCHAR(45) NULL ,
  `meta_title` VARCHAR(250) NULL ,
  `meta_keywords` TEXT NULL ,
  `meta_description` TEXT NULL ,
  `description` TEXT NULL ,
  `url` VARCHAR(250) NULL ,
  `image` TEXT NULL ,
  `order_num` INT NULL ,
  `enabled` SMALLINT(1) NULL ,
  `delete` SMALLINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`category_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `products` (
  `product_id` INT NOT NULL ,
  `url` VARCHAR(250) NULL ,
  `category_id` INT NULL ,
  `brand_id` INT NULL ,
  `model` VARCHAR(250) NULL ,
  `description` TEXT NULL ,
  `body` TEXT NULL ,
  `enabled` SMALLINT(1) NULL ,
  `hit` INT NULL ,
  `order_num` INT NULL ,
  `small_image` TEXT NULL ,
  `large_image` TEXT NULL ,
  `download` VARCHAR(45) NULL ,
  `meta_title` TEXT NULL ,
  `meta_keywords` TEXT NULL ,
  `meta_description` TEXT NULL ,
  `created` VARCHAR(45) NULL ,
  `modified` VARCHAR(45) NULL ,
  `is_new` SMALLINT(1) NULL ,
  `discount` INT NULL ,
  `is_edited` SMALLINT(1) NULL ,
  `cells` VARCHAR(45) NULL ,
  `delete` SMALLINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`product_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `variants` (
  `variant_id` INT NOT NULL ,
  `product_id` INT NULL ,
  `sku` VARCHAR(250) NULL ,
  `name` VARCHAR(250) NULL ,
  `price` VARCHAR(45) NULL ,
  `price_opt` VARCHAR(45) NULL ,
  `stock` SMALLINT(1) NULL ,
  `position` SMALLINT(1) NULL ,
  `delete` SMALLINT(1) NULL ,
  PRIMARY KEY (`variant_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `properties` (
  `property_id` INT NOT NULL ,
  `name` VARCHAR(250) NULL ,
  `in_product` SMALLINT(1) NULL ,
  `in_filter` SMALLINT(1) NULL ,
  `in_compare` SMALLINT(1) NULL ,
  `order_num` INT NULL ,
  `enabled` SMALLINT(1) NULL ,
  `options` TEXT NULL ,
  `delete` SMALLINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`property_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `properties_values` (
  `product_id` INT NOT NULL ,
  `property_id` INT NOT NULL ,
  `value` TEXT NULL ,
  `delete` SMALLINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`product_id`, `property_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `product_fotos` (
  `product_id` INT NOT NULL ,
  `foto_id` INT NOT NULL ,
  `filename` VARCHAR(250) NULL,
  `delete` SMALLINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`product_id`, `foto_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `delivery_methods` (
  `delivery_method_id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(250) NULL ,
  `description` TEXT NULL ,
  `free_from` VARCHAR(45) NULL ,
  `price` VARCHAR(45) NULL ,
  `enabled` SMALLINT(1) NULL ,
  `delete` SMALLINT(1) NULL ,
  PRIMARY KEY (`delivery_method_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `currencies` (
  `currency_id` INT NOT NULL AUTO_INCREMENT ,
  `main` INT NULL ,
  `def` INT NULL ,
  `name` VARCHAR(45) NULL ,
  `sign` VARCHAR(45) NULL ,
  `code` VARCHAR(45) NULL ,
  `rate_from` VARCHAR(45) NULL ,
  `rate_to` VARCHAR(45) NULL ,
  `delete` SMALLINT(1) NULL ,
  PRIMARY KEY (`currency_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `payment_methods` (
  `payment_method_id` INT NOT NULL AUTO_INCREMENT ,
  `module` VARCHAR(45) NULL ,
  `name` VARCHAR(45) NULL ,
  `description` TEXT NULL ,
  `currency_id` INT NULL ,
  `params` TEXT NULL ,
  `delete` SMALLINT(1) NULL ,
  `enabled` SMALLINT(1) NULL ,
  PRIMARY KEY (`payment_method_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("CREATE  TABLE IF NOT EXISTS `delivery_payments` (
  `delivery_method_id` INT NULL ,
  `payment_method_id` INT NULL ,
  `delete` SMALLINT(1) NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci");

        $connection->query("INSERT IGNORE INTO `menu_items` (`id`, `title`, `url`, `position`, `doc_id`, `blank`) VALUES
(2, 'Оплатить заказ', '//intellectmoney.a4h.ru/', 10, 0, 1),
(4, 'Товары и качество', '/doc/tovar_q.html', 20, 8, 0),
(5, 'Размеры', '/doc/size.html', 30, 9, 0),
(6, 'Доставка и оплата', '/doc/dost_oplata.html', 40, 10, 0);");

        $connection->query("INSERT IGNORE INTO `doc` (`id`, `title`, `alias`, `text`, `keywords`, `addMain`) VALUES
(8, 'Товары и качество', 'tovar_q', '<p>Майки изготовлены из 100% хлопка высокого качества, женские плотность 195 г/м2 (95% хб + 5% лайкра, короткий рукав), мужские плотность 180 г/м2. Изображение наносится методом термотрансферной печати и выдерживает многократные стирки при соблюдении правил эксплуатации:</p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Допускается стирка в стиральной машине при температуре воды до 40&deg;C</p>\r\n	</li>\r\n	<li>\r\n	<p>Допускается гладить майку только с обратной стороны - ни в коем случае не гладьте по изображению.</p>\r\n	</li>\r\n	<li>\r\n	<p>Перед стиркой необходимо вывернуть изделие наизнанку!</p>\r\n	</li>\r\n</ul>\r\n', '', 0),
(9, 'Размеры', 'size', '<h2>Мужская футболка</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/man.jpg?ref=126679\" style=\"height:131px; width:121px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>XS</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n			<th>XXXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>42-44</td>\r\n			<td>44-46</td>\r\n			<td>46-48</td>\r\n			<td>48-50</td>\r\n			<td>50-52</td>\r\n			<td>52-54</td>\r\n			<td>54</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>48</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n			<td>54</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>62</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>66</td>\r\n			<td>68</td>\r\n			<td>70</td>\r\n			<td>72</td>\r\n			<td>74</td>\r\n			<td>76</td>\r\n			<td>78</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Мужская футболка с рукавом</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/man-long.jpg?ref=126679\" style=\"height:104px; width:121px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n			<th>XXXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>44-46</td>\r\n			<td>46-48</td>\r\n			<td>48-50</td>\r\n			<td>50-52</td>\r\n			<td>52-54</td>\r\n			<td>54</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n			<td>54</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>62</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>68</td>\r\n			<td>70</td>\r\n			<td>72</td>\r\n			<td>74</td>\r\n			<td>76</td>\r\n			<td>78</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Рукав, см</td>\r\n			<td>61</td>\r\n			<td>62</td>\r\n			<td>63</td>\r\n			<td>64</td>\r\n			<td>66</td>\r\n			<td>68</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Футболка поло</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/img/editor/polo/56a006e350a800e68a17c2e19320b4bc.jpg?ref=126679\" style=\"height:143px; width:133px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n			<th>XXXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>44-46</td>\r\n			<td>46-48</td>\r\n			<td>48-50</td>\r\n			<td>50-52</td>\r\n			<td>52-54</td>\r\n			<td>54</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А), см</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n			<td>54</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>60</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б), см</td>\r\n			<td>68</td>\r\n			<td>70</td>\r\n			<td>72</td>\r\n			<td>74</td>\r\n			<td>76</td>\r\n			<td>78</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Мужская майка</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/img/markup_doc/img-99.png?ref=126679\" style=\"height:143px; width:133px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n			<th>XXXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>48</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n			<td>54</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А), см</td>\r\n			<td>43</td>\r\n			<td>45</td>\r\n			<td>47</td>\r\n			<td>49</td>\r\n			<td>51</td>\r\n			<td>52</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б), см</td>\r\n			<td>68</td>\r\n			<td>72</td>\r\n			<td>75</td>\r\n			<td>78</td>\r\n			<td>80</td>\r\n			<td>82</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Мужская борцовка</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/img/markup_doc/man_borcovka_size.jpg?ref=126679\" style=\"height:143px; width:133px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n			<th>XXXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>48</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n			<td>54</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А), см</td>\r\n			<td>43</td>\r\n			<td>45</td>\r\n			<td>47</td>\r\n			<td>49</td>\r\n			<td>51</td>\r\n			<td>52</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б), см</td>\r\n			<td>68</td>\r\n			<td>72</td>\r\n			<td>75</td>\r\n			<td>78</td>\r\n			<td>80</td>\r\n			<td>82</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Женская футболка</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/woman.jpg?ref=126679\" style=\"height:132px; width:120px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>XS</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>42-44</td>\r\n			<td>44-46</td>\r\n			<td>46-48</td>\r\n			<td>48-50</td>\r\n			<td>50-52</td>\r\n			<td>52-54</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>38</td>\r\n			<td>40</td>\r\n			<td>42</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>50</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>55</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>60</td>\r\n			<td>62</td>\r\n			<td>64</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Женская футболка с длинным рукавом</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/woman-long.jpg?ref=126679\" style=\"height:101px; width:121px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>XS</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>44-46</td>\r\n			<td>46-48</td>\r\n			<td>48-50</td>\r\n			<td>50-52</td>\r\n			<td>52-54</td>\r\n			<td>54</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>38</td>\r\n			<td>40</td>\r\n			<td>42</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>50</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>55</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>60</td>\r\n			<td>62</td>\r\n			<td>64</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Рукав, см</td>\r\n			<td>55</td>\r\n			<td>58</td>\r\n			<td>59</td>\r\n			<td>60</td>\r\n			<td>62</td>\r\n			<td>64</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Женская майка</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/img/markup_doc/woman_tshirt_size_small.jpg?ref=126679\" style=\"height:132px; width:120px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>XS</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>42-44</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>48</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>38</td>\r\n			<td>40</td>\r\n			<td>42</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>50</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>55</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>60</td>\r\n			<td>62</td>\r\n			<td>64</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Женская борцовка</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/img/markup_doc/woman_borcovka_small.jpg?ref=126679\" style=\"height:132px; width:120px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>XS</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>42-44</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>48</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>38</td>\r\n			<td>40</td>\r\n			<td>42</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>50</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>55</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>60</td>\r\n			<td>62</td>\r\n			<td>64</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n			<td>1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Толстовка</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/hoodie.jpg?ref=126679\" style=\"height:152px; width:120px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n			<th>XXXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>44-46</td>\r\n			<td>46-48</td>\r\n			<td>48-50</td>\r\n			<td>50-52</td>\r\n			<td>52-54</td>\r\n			<td>54-56</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n			<td>54</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>60</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>68</td>\r\n			<td>70</td>\r\n			<td>72</td>\r\n			<td>74</td>\r\n			<td>76</td>\r\n			<td>78</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Рукав, см</td>\r\n			<td>61</td>\r\n			<td>62</td>\r\n			<td>63</td>\r\n			<td>64</td>\r\n			<td>66</td>\r\n			<td>68</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Толстовка утепленная</h2>\r\n\r\n<p><img alt=\"\" src=\"http://static.vsemayki.ru/mail_images/warm_hoodie.png?ref=126679\" style=\"height:152px; width:120px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n			<th>XXXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>44-46</td>\r\n			<td>46-48</td>\r\n			<td>48-50</td>\r\n			<td>50-52</td>\r\n			<td>52-54</td>\r\n			<td>54-56</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n			<td>54</td>\r\n			<td>56</td>\r\n			<td>58</td>\r\n			<td>60</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>68</td>\r\n			<td>70</td>\r\n			<td>72</td>\r\n			<td>74</td>\r\n			<td>76</td>\r\n			<td>78</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Рукав, см</td>\r\n			<td>61</td>\r\n			<td>62</td>\r\n			<td>63</td>\r\n			<td>64</td>\r\n			<td>66</td>\r\n			<td>68</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Футболка для беременных</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/baby.jpg?ref=126679\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n			<th>XL</th>\r\n			<th>XXL</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Ширина (А)</td>\r\n			<td>43</td>\r\n			<td>44</td>\r\n			<td>45</td>\r\n			<td>49</td>\r\n			<td>50</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б)</td>\r\n			<td>54</td>\r\n			<td>55</td>\r\n			<td>58</td>\r\n			<td>59</td>\r\n			<td>62</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (В)</td>\r\n			<td>65</td>\r\n			<td>66</td>\r\n			<td>67</td>\r\n			<td>69</td>\r\n			<td>70</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Детская футболка</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/children.jpg?ref=126679\" style=\"height:129px; width:120px\" /></p>\r\n\r\n<table>\r\n	<tbody>\r\n		<tr>\r\n			<td>Размер</td>\r\n			<td style=\"text-align:center\">5XS</td>\r\n			<td style=\"text-align:center\">4XS</td>\r\n			<td style=\"text-align:center\">3XS</td>\r\n			<td style=\"text-align:center\">2XS</td>\r\n			<td style=\"text-align:center\">XS</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Российский размер</td>\r\n			<td style=\"text-align:center\">24-26</td>\r\n			<td style=\"text-align:center\">28-30</td>\r\n			<td style=\"text-align:center\">32-34</td>\r\n			<td style=\"text-align:center\">36-38</td>\r\n			<td style=\"text-align:center\">38-40</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Возраст</td>\r\n			<td style=\"text-align:center\">3-4 года</td>\r\n			<td style=\"text-align:center\">5-6 лет</td>\r\n			<td style=\"text-align:center\">7-8 лет</td>\r\n			<td style=\"text-align:center\">9-10 лет</td>\r\n			<td style=\"text-align:center\">11-12 лет</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Рост, см</td>\r\n			<td style=\"text-align:center\">98-104</td>\r\n			<td style=\"text-align:center\">110-116</td>\r\n			<td style=\"text-align:center\">128-140</td>\r\n			<td style=\"text-align:center\">146</td>\r\n			<td style=\"text-align:center\">152</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Ширина (А), см</td>\r\n			<td style=\"text-align:center\">31</td>\r\n			<td style=\"text-align:center\">34</td>\r\n			<td style=\"text-align:center\">37</td>\r\n			<td style=\"text-align:center\">40</td>\r\n			<td style=\"text-align:center\">43</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Длина (Б), см</td>\r\n			<td style=\"text-align:center\">45</td>\r\n			<td style=\"text-align:center\">48</td>\r\n			<td style=\"text-align:center\">51</td>\r\n			<td style=\"text-align:center\">54</td>\r\n			<td style=\"text-align:center\">57</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Допуск (+/-), см</td>\r\n			<td style=\"text-align:center\">1</td>\r\n			<td style=\"text-align:center\">1</td>\r\n			<td style=\"text-align:center\">1</td>\r\n			<td style=\"text-align:center\">1</td>\r\n			<td style=\"text-align:center\">1</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<h2>Трусы мужские</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/pants-man.jpg?ref=126679\" style=\"height:94px; width:119px\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>46</td>\r\n			<td>50</td>\r\n			<td>52</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Обхват (А), см*</td>\r\n			<td>98</td>\r\n			<td>112</td>\r\n			<td>120</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>* размер без учета натяжения резинки</p>\r\n\r\n<h2>Трусы женские</h2>\r\n\r\n<p><img alt=\"\" src=\"http://www.vsemayki.ru/i/sizes/pants-woman.jpg?ref=126679\" /></p>\r\n\r\n<table>\r\n	<thead>\r\n		<tr>\r\n			<th>Размер</th>\r\n			<th>S</th>\r\n			<th>M</th>\r\n			<th>L</th>\r\n		</tr>\r\n	</thead>\r\n	<tbody>\r\n		<tr>\r\n			<td>Соответствует</td>\r\n			<td>44</td>\r\n			<td>46</td>\r\n			<td>48</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Обхват (А), см*</td>\r\n			<td>92-96</td>\r\n			<td>96-100</td>\r\n			<td>100-104</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>* размер без учета натяжения резинки</p>\r\n', '', 0),
(10, 'Доставка и оплата', 'dost_oplata', '<p><a href=\"#moskow\">Доставка по Москве</a><br />\r\n<a href=\"#piter\">Доставка по Санкт-Петербургу</a><br />\r\n<a href=\"#region\">Доставка по России</a><br />\r\n<a href=\"#sng\">Международная доставка</a> &nbsp;<a name=\"moskow\"></a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Самовывоз из Москвы</h3>\r\n\r\n<p><strong>Пункт выдачи - Москва (ул. Большая серпуховская, дом 31, корпус 6)</strong></p>\r\n\r\n<p>Название: курьерская служба &quot;День в день&quot;</p>\r\n\r\n<p>Срок доставки: 3 - 5 дней</p>\r\n\r\n<p>Станция метро: Серпуховская</p>\r\n\r\n<p>Время работы:</p>\r\n\r\n<p>с 10:00 до 19:00 (в будни)</p>\r\n\r\n<p>с 10:00 до 18:00 (в выходные)</p>\r\n\r\n<p>Стоимость доставки: бесплатно</p>\r\n\r\n<p>Телефон:</p>\r\n\r\n<p>+7 (499) 237-20-19</p>\r\n\r\n<p>+7 (916) 803-12-52</p>\r\n\r\n<p><a href=\"http://www.vsemayki.ru/doc/serpuhovskaya?ref=126679\">Подробнее о пункте выдачи...</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h3>Курьерская доставка по Москве</h3>\r\n\r\n<p><strong>Срок доставки</strong> 3 - 5 дней.<br />\r\n<strong>Стоимость доставки</strong>&nbsp;заказа до 5 кг. в пределах МКАД 190 руб.</p>\r\n\r\n<p>Доплата за превышение веса в 5 кг (за каждый, в т.ч. Неполный кг) 25 руб.</p>\r\n\r\n<p>Время доставки с 11:00 до 18:00. (по рабочим дням)</p>\r\n\r\n<p><strong>Стоимость доставки за пределы МКАД</strong> будет выше, надбавка зависит от степени дальности и рассчитывается по тарифу:&nbsp;</p>\r\n\r\n<p>До 5 км от МКАД (до 5 кг) 270 руб.</p>\r\n\r\n<p>От 5 км и далее (за каждый километр) 20руб.</p>\r\n\r\n<p><strong>Оплата отказа</strong></p>\r\n\r\n<p>Оплата поездки курьера в случае отказа от заказа полностью в пределах МКАД 150 руб.</p>\r\n\r\n<p>Оплата поездки курьера в случае отказа от заказа полностью за МКАД 160 руб.</p>\r\n\r\n<p><a name=\"piter\"></a></p>\r\n\r\n<h3>Курьерская доставка по Санкт-Петербургу</h3>\r\n\r\n<p><strong>Срок доставки</strong> 5 - 7 дней</p>\r\n\r\n<p><strong>Стоимость доставки</strong>: 265 руб.&nbsp;<br />\r\nВремя доставки с 11:00 до 18:00. (по рабочим дням)</p>\r\n\r\n<p><a name=\"piter\"></a> <a name=\"region\"></a></p>\r\n\r\n<h3>Доставка Почтой России (Отправление 1 класса)</h3>\r\n\r\n<p><strong>Доставка с наложенным платежом</strong> осуществляется только по территории РФ, кроме Чеченской Республики (Почта России не принимает данный вид отправления в это направление)<br />\r\nТак же временами сезонно закрыты некоторые северные регионы, в частности Якутск и область.<br />\r\nЗаказы высылаются Почтой России отправлением 1 класса.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Доставка за пределы Российской Федерации в любую точку мира</strong>&nbsp;осуществляется только после полной предоплаты заказа.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Сроки доставки</strong>: до 20 дней, для крупных городов около 10 дней</p>\r\n\r\n<p>подробнее о сроках доставки на сайте <a href=\"http://russianpost.ru/rp/servise/ru/home/postuslug/1class/1class_deadline\">Почта России</a>.<br />\r\n<br />\r\n<strong>Стоимость доставки:</strong></p>\r\n\r\n<p>1 футболка235руб. + тариф на денежный перевод<br />\r\n2 футболки285руб. + тариф на денежный перевод3 футболки335руб. + тариф на денежный перевод4 футболки395руб. + тариф на денежный перевод5 футболок435руб. + тариф на денежный перевод6 футболок465руб. + тариф на денежный перевод7 футболок515руб. + тариф на денежный перевод8 футболок и более575руб. + тариф на денежный перевод<br />\r\n<strong>Тарифы на денежный перевод</strong>. При получении посылки с наложенным платежом на почте взимается комиссия за почтовый перевод в размере 1,8% от суммы, но не менее 35 рублей.</p>\r\n\r\n<h3>Самовывоз из Новосибирска</h3>\r\n\r\n<p><strong>Пункт выдачи - Новосибирск (пр. К. Маркса, дом 47)</strong></p>\r\n\r\n<p>Срок доставки: 3 - 7 дней</p>\r\n\r\n<p>Станция метро:Студенческая</p>\r\n\r\n<p>Время работы: с 10:00 до 20:00, без выходных</p>\r\n\r\n<p>Стоимость доставки: бесплатно</p>\r\n\r\n<p>Телефон: +7 (383)&nbsp; 233-29-09</p>\r\n\r\n<p><a href=\"http://www.vsemayki.ru/doc/nsk/?ref=126679\">Подробнее</a></p>\r\n\r\n<p><br />\r\n<strong>Пункт выдачи - Новосибирск (ул.Крылова, дом 7)</strong></p>\r\n\r\n<p>Срок доставки: 3 - 7 дней</p>\r\n\r\n<p>Станция метро: Красный проспект</p>\r\n\r\n<p>Время работы: с 10:00 до 20:00, без выходных</p>\r\n\r\n<p>Стоимость доставки: бесплатно</p>\r\n\r\n<p>Телефон: +7 (383)&nbsp; 233-29-28</p>\r\n\r\n<p><a href=\"http://www.vsemayki.ru/doc/nsk/?ref=126679\">Подробнее</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><a name=\"sng\"></a></p>\r\n\r\n<h3>Международная доставка</h3>\r\n\r\n<p>Международная доставка временно не осуществляется.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Внимание!</h2>\r\n\r\n<ul>\r\n	<li>Минимальная сумма заказа 190руб.</li>\r\n	<li>Заказы на сумму свыше 5000руб. без полной предоплаты не отправляются.</li>\r\n	<li>Стоимость доставки одной кружки приравнивается к стоимости доставки одной футболки.</li>\r\n	<li>Стоимость доставки одного коврика для мыши приравнивается к стоимости доставки одной футболки.</li>\r\n	<li>Стоимость доставки от 1 до 20 значков приравнивается к стоимости доставки одной футболки<br />\r\n	от 21 до 40 - к стоимости доставки двух футболок и так далее.<br />\r\n	Если, кроме значков, в заказ входят еще какие-либо товары, то первые 20 значков в цене доставки не учитываются.</li>\r\n	<li>Стоимость доставки одной бейсболки приравнивается к стоимости доставки одной футболки.</li>\r\n	<li>Доставка с оплатой при получении возможна только по России! Во все остальные страны - 100% предоплата.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Оплата</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Возможны следующие способы оплаты:</strong><br />\r\n&nbsp;</p>\r\n\r\n<ul>\r\n	<li><img alt=\"\" src=\"http://www.vsemayki.ru/img/card_visa.gif?ref=126679\" /><img alt=\"\" src=\"http://www.vsemayki.ru/img/card_mastercard.gif?ref=126679\" /><!--<img src=\"http://www.vsemayki.ru/img/card_dinersclub.gif?ref=126679\" _mce_src=\"http://www.vsemayki.ru/img/card_dinersclub.gif?ref=126679\">--><br />\r\n	Оплата заказа кредитной картой системы VISA/VISA electron, Mastercard/Maestro <!--или Diners Club--> через платежный интерфейс <a href=\"http://www.cyberplat.ru\">Cyberplat</a>. Оплата производится сразу после оформления заказа. В этом случае Вы получаете скидку <strong>10%</strong>, кроме товаров, изготовленных на заказ.<br />\r\n	&nbsp;</li>\r\n	<li><img alt=\"\" src=\"http://www.vsemayki.ru/tmp/webmoney.gif?ref=126679\" /><br />\r\n	Оплата заказа с помощью платежной системы <strong>WebMoney</strong>. Оплата производится сразу после оформления заказа. В этом случае Вы получаете скидку <strong>10%</strong>, кроме товаров, изготовленных на заказ.<br />\r\n	<br />\r\n	&nbsp;</li>\r\n	<li><img alt=\"\" src=\"http://www.vsemayki.ru/tmp/yamoney.gif?ref=126679\" /><br />\r\n	Оплата заказа с помощью платежной системы <strong>Яндекс.Деньги</strong>. Оплата производится сразу после оформления заказа. В этом случае Вы получаете скидку <strong>10%</strong> кроме товаров, изготовленных на заказ.<br />\r\n	&nbsp;</li>\r\n	<li><img alt=\"\" src=\"http://www.vsemayki.ru/tmp/postrus.gif?ref=126679\" /><br />\r\n	Наложенный платеж - это возможность оплаты заказа на почте в момент его получения. Оплата наложенным платежом возможна только при условии доставки почтой России. При получении и оплате заказа, доставленного наложенным платежом, взимается плата по тарифу на пересылку внутренних простых почтовых переводов. Процент оплаты зависит от пересылаемой суммы и не зависит от технологии пересылки денежных средств и фактически составляет:<br />\r\n	5% +7 рублей если сумма менее 1000 рублей<br />\r\n	4% +57 рублей если сумма от 1000 до 5000 рублей.<br />\r\n	В связи с этим просим Вас приготовиться к оплате этого небольшого сбора, если Вы выбираете Почту России как способ доставки. Внимание!Товары на заказ нельзя оплатить наложенным платежем.<br />\r\n	&nbsp;</li>\r\n	<li><img alt=\"\" src=\"http://www.vsemayki.ru/img/bank.jpg?ref=126679\" /><br />\r\n	При оплате банковским переводом Вы получаете <strong>скидку 10%</strong>, кроме товаров, изготовленных на заказ. Оплату можно произвести в любом банке, который способен принимать переводы без открытия счёта, если у вас нет расчетного счета, либо перевести средства со своего расчетного счета. Средняя комиссия банков за перевод 1,5%-3%, однако многие банки устанавливают минимальные суммы стоимости перевода от 30 до 100 рублей. Рекомендуем пользоваться услугами Сбербанка России, комиссия 1,5% минимальная сумма 30 рублей.</li>\r\n</ul>\r\n\r\n<table><!-- /Курьерская доставка по Санкт-Петербургу --><!-- top --><!-- Внимание! -->\r\n</table>\r\n', '', 0);");

    }
}