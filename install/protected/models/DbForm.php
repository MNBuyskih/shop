<?php

/**
 * Db.php
 * Date: 29.07.13
 * Time: 15:24
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 */
class DbForm extends Model {

    public $host;

    public $dbName;

    public $username;

    public $password;

    public $truncate;

    public function attributeLabels() {
        return array(
            'host'     => 'Хост',
            'dbName'   => 'Название базы данных',
            'username' => 'Имя пользователя базы данных',
            'password' => 'Пароль от базы данных',
            'truncate' => 'Очистить базу данных перед импортом',
        );
    }

    public function save($validate = true) {
        if ($validate && !$this->validate()) {
            return false;
        }

        App::app()->startSession();
        $_SESSION['db']             = array();
        $_SESSION['db']['host']     = $this->host;
        $_SESSION['db']['dbName']   = $this->dbName;
        $_SESSION['db']['username'] = $this->username;
        $_SESSION['db']['password'] = $this->password;
        $_SESSION['db']['truncate'] = $this->truncate;

        return true;
    }

    public function restore() {
        App::app()->startSession();
        if (empty($_SESSION['db'])) {
            return false;
        }

        $this->setAttributes($_SESSION['db']);

        return true;
    }

    public function validate($attributes = array()) {
        $this->host     = trim($this->host);
        $this->dbName   = trim($this->dbName);
        $this->username = trim($this->username);

        if (empty($this->host)) {
            $this->addError('host', $this->getAttributeLabel('host') . ' не может быть пустым');
        }
        if (empty($this->dbName)) {
            $this->addError('dbName', $this->getAttributeLabel('dbName') . ' не может быть пустым');
        }
        if (empty($this->username)) {
            $this->addError('username', $this->getAttributeLabel('username') . ' не может быть пустым');
        }

        if (!parent::validate()) {
            return false;
        }

        /** @var Db|DbException $result */
        $result = $this->connect();
        if (!$result instanceof Db) {
            switch ($result->getCode()) {
                case DbException::ERROR_SELECT_DB:
                    $this->addError('dbName', "Не возможно выбрать базу данных: " . $result->getMessage());
                    break;
                default:
                    $this->addError('host', "Не возможно подключится к базе данных: " . $result->getMessage());
                    break;
            }
        }

        return parent::validate();
    }

    public function connect() {
        App::import("//library/Db");
        try {
            @$connection = Db::create($this->host, $this->username, $this->password, $this->dbName, $this->getClass());

            return $connection;
        } catch (DbException $e) {
            return $e;
        }
    }

    public function getClass() {
        if (extension_loaded('pdo_mysql')) {
            return "DbPDO";
        }

        return "DbMySQL";
    }
}