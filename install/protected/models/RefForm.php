<?php
/**
 * RefForm.php
 * Date: 29.07.13
 * Time: 16:11
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 */

class RefForm extends Model {

    public $ref;

    public $key;

    public function attributeLabels() {
        return array(
            'ref' => 'ID партнёра',
            'key' => 'Ключ',
        );
    }

    public function validate($attributes = array()) {
        $this->key = trim($this->key);
        $this->ref = trim($this->ref);

        if (empty($this->key)) {
            $this->addError('key', "Ключ не может быть пустым");
        }
        if (empty($this->ref)) {
            $this->addError('ref', "ID партнёра не может быть пустым");
        }

        if (!parent::validate()) {
            return false;
        }

        $url = "http://partners.mmamarket.ru/write/check.asp";
        $ch  = curl_init($url);
        curl_setopt_array($ch, array(
                                    CURLOPT_POST           => true,
                                    CURLOPT_POSTFIELDS     => array(
                                        'ref' => $this->ref,
                                        'key' => $this->key
                                    ),
                                    CURLOPT_RETURNTRANSFER => true,
                               ));
        $result = (int)curl_exec($ch);
        curl_close($ch);

        if ($result !== 1) {
            $this->addError('key', 'Ключ или ID партнёра введены не верно.');
        }

        return parent::validate();
    }

    public function save($validate = true) {
        if (!$this->validate()) {
            return false;
        }

        App::app()->startSession();
        $_SESSION['ref'] = $this->ref;
        $_SESSION['key'] = $this->key;

        return true;
    }

    public function restore() {
        App::app()->startSession();
        $this->ref = $_SESSION['ref'];
        $this->key = $_SESSION['key'];
    }

}