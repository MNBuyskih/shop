<?php
/**
 * PasswordForm.php
 * Date: 30.07.13
 * Time: 8:07
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class PasswordForm extends Model {
    public $password;

    public $confirmPassword;

    public function save($validate = true) {
        if ($validate && !$this->validate()) {
            return false;
        }

        App::app()->startSession();
        $_SESSION['password'] = $this->password;

        return true;
    }

    public function restore() {
        App::app()->startSession();
        $this->password = $_SESSION['password'];
    }

    public function attributeLabels() {
        return array(
            'password'        => 'Пароль',
            'confirmPassword' => 'Повторите пароль',
        );
    }

    public function validate($attributes = array()) {
        $this->password        = trim($this->password);
        $this->confirmPassword = trim($this->confirmPassword);

        if (empty($this->password)) {
            $this->addError('password', $this->getAttributeLabel('password') . ' не может быть пустым');
        }
        if (empty($this->confirmPassword)) {
            $this->addError('confirmPassword', $this->getAttributeLabel('confirmPassword') . ' не может быть пустым');
        }

        if (!parent::validate()) {
            return false;
        }

        if ($this->password !== $this->confirmPassword) {
            $this->addError('confirmPassword', 'Пароли не совпадают');
        }

        return parent::validate();
    }

}