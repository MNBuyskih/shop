<?php
$config                    = require realpath(dirname(__FILE__) . '/../../../protected/config/main.php');
$config['theme']           = 'install';
$config['name']            = 'Shop install';
$config['controllersPath'] = '/install/protected/controllers';

return $config;