<?php
/**
 * IndexController.php
 * Date: 22.07.13
 * Time: 12:39
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
App::import('//library/AdminController');

class IndexController extends AdminController {
    public function actionIndex() {
        $this->render('index');
    }
}