<?php
/**
 * UpdateProcessController.php
 * Date: 25.07.13
 * Time: 8:43
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

App::import('//library/AdminController');

class UpdateProcessController extends AdminController {

    public $saveDir;

    public function init() {
        if (App::app()->functionEnabled('set_time_limit')) {
            set_time_limit(0);
        }
        $this->saveDir = App::app()->basePath . '/admin/update-temp';
        parent::init();
    }

    public function actionDownload() {
        // Сохраняем новую запись в истории. Статус = 0 - неуспешно.
        // Если всё будет ок - в конце всей этой истории обновим статус = 1
        App::app()->db()->query("INSERT INTO `update_history` (date, status) VALUES(:date, 0)", array(":date" => time()));

        $url = App::app()->config['update']['serverUrl'] . '/xml/update.zip';
        @$data = file_get_contents($url);

        if (!$data) {
            throw new HttpException(500, 'Не возможно получить архив обновлений');
        }

        file_put_contents($this->saveDir . '/update.zip', $data);

        echo json_encode(array('step' => 2));
        App::app()->end();
    }

    public function actionUnzip() {
        $zip = new ZipArchive();
        $zip->open($this->saveDir . '/update.zip');
        $zip->extractTo($this->saveDir);
        $zip->close();

        echo json_encode(array('step' => 3));
        App::app()->end();
    }

    public function actionBrands() {
        $xml    = $this->saveDir . '/brands.xml';
        $xml    = file_get_contents($xml);
        $brands = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `brands`
SET
    `brand_id` = :brand_id,
    `name` = :name,
    `url` = :url,
    `meta_title` = :meta_title,
    `meta_keywords` = :meta_keywords,
    `meta_description` = :meta_description,
    `description` = :description,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `name` = :name,
    `url` = :url,
    `meta_title` = :meta_title,
    `meta_keywords` = :meta_keywords,
    `meta_description` = :meta_description,
    `description` = :description,
    `delete` = 0";

        $this->updateTable($brands, 'brand', 'brands', $query, array(
                                                                    'brand_id',
                                                                    'name',
                                                                    'url',
                                                                    'meta_title',
                                                                    'meta_keywords',
                                                                    'meta_description',
                                                                    'description'
                                                               ), 3);
    }

    public function actionCategories() {
        $xml        = $this->saveDir . '/categories.xml';
        $xml        = file_get_contents($xml);
        $categories = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `categories`
SET
    `category_id` = :category_id,
    `parent` = :parent,
    `name` = :name,
    `single_name` = :single_name,
    `meta_title` = :meta_title,
    `meta_keywords` = :meta_keywords,
    `meta_description` = :meta_description,
    `description` = :description,
    `url`  = :url,
    `image` = :image,
    `order_num` = :order_num,
    `enabled` = :enabled,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `parent` = :parent,
    `name` = :name,
    `single_name` = :single_name,
    `meta_title` = :meta_title,
    `meta_keywords` = :meta_keywords,
    `meta_description` = :meta_description,
    `description` = :description,
    `url`  = :url,
    `image` = :image,
    `order_num` = :order_num,
    `enabled` = :enabled,
    `delete` = 0";

        $this->updateTable($categories, 'category', 'categories', $query, array(
                                                                               'category_id',
                                                                               'parent',
                                                                               'name',
                                                                               'single_name',
                                                                               'meta_title',
                                                                               'meta_keywords',
                                                                               'meta_description',
                                                                               'description',
                                                                               'url',
                                                                               'image',
                                                                               'order_num',
                                                                               'enabled',
                                                                          ), 4);
    }

    public function actionProducts() {
        $xml      = $this->saveDir . '/products.xml';
        $xml      = file_get_contents($xml);
        $products = new SimpleXMLElement($xml);

        $query = "
INSERT DELAYED INTO
    `products`
SET
    `product_id` = :product_id,
    `url` = :url,
    `category_id` = :category_id,
    `brand_id` = :brand_id,
    `model` = :model,
    `description` = :description,
    `body` = :body,
    `enabled` = :enabled,
    `hit` = :hit,
    `order_num` = :order_num,
    `small_image` = :small_image,
    `large_image` = :large_image,
    `download` = :download,
    `meta_title` = :meta_title,
    `meta_keywords` = :meta_keywords,
    `meta_description` = :meta_description,
    `created` = :created,
    `modified` = :modified,
    `is_new` = :is_new,
    `discount` = :discount,
    `is_edited` = :is_edited,
    `cells` = :cells,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `url` = :url,
    `category_id` = :category_id,
    `brand_id` = :brand_id,
    `model` = :model,
    `description` = :description,
    `body` = :body,
    `enabled` = :enabled,
    `hit` = :hit,
    `order_num` = :order_num,
    `small_image` = :small_image,
    `large_image` = :large_image,
    `download` = :download,
    `meta_title` = :meta_title,
    `meta_keywords` = :meta_keywords,
    `meta_description` = :meta_description,
    `created` = :created,
    `modified` = :modified,
    `is_new` = :is_new,
    `discount` = :discount,
    `is_edited` = :is_edited,
    `cells` = :cells,
    `delete` = 0";

        $this->updateTable($products, 'product', 'products', $query, array(
                                                                          'product_id',
                                                                          'url',
                                                                          'category_id',
                                                                          'brand_id',
                                                                          'model',
                                                                          'description',
                                                                          'body',
                                                                          'enabled',
                                                                          'hit',
                                                                          'order_num',
                                                                          'small_image',
                                                                          'large_image',
                                                                          'download',
                                                                          'meta_title',
                                                                          'meta_keywords',
                                                                          'meta_description',
                                                                          'created',
                                                                          'modified',
                                                                          'is_new',
                                                                          'int:discount',
                                                                          'is_edited',
                                                                          'cells',
                                                                     ), 5);
    }

    public function actionVariants() {
        $xml      = $this->saveDir . '/variants.xml';
        $xml      = file_get_contents($xml);
        $variants = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `variants`
SET
    `variant_id` = :variant_id,
    `product_id` = :product_id,
    `sku` = :sku,
    `name` = :name,
    `price_opt` = :price_opt,
    `price` = :price,
    `stock` = :stock,
    `position` = :position,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `variant_id` = :variant_id,
    `product_id` = :product_id,
    `sku` = :sku,
    `name` = :name,
    `price_opt` = :price_opt,
    `price` = :price,
    `stock` = :stock,
    `position` = :position,
    `delete` = 0";

        $this->updateTable($variants, 'variant', 'variants', $query, array(
                                                                          'variant_id',
                                                                          'product_id',
                                                                          'sku',
                                                                          'name',
                                                                          'price_opt',
                                                                          'price',
                                                                          'stock',
                                                                          'position',
                                                                     ), 6);
    }

    public function actionProperties() {
        $xml        = $this->saveDir . '/properties.xml';
        $xml        = file_get_contents($xml);
        $properties = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `properties`
SET
    `property_id` = :property_id,
    `name` = :name,
    `in_product` = :in_product,
    `in_filter` = :in_filter,
    `in_compare` = :in_compare,
    `order_num` = :order_num,
    `enabled` = :enabled,
    `options` = :options,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `property_id` = :property_id,
    `name` = :name,
    `in_product` = :in_product,
    `in_filter` = :in_filter,
    `in_compare` = :in_compare,
    `order_num` = :order_num,
    `enabled` = :enabled,
    `options` = :options,
    `delete` = 0";

        $this->updateTable($properties, 'property', 'properties', $query, array(
                                                                               'property_id',
                                                                               'name',
                                                                               'in_product',
                                                                               'in_filter',
                                                                               'in_compare',
                                                                               'order_num',
                                                                               'enabled',
                                                                               'options',
                                                                          ), 7);
    }

    public function actionPropertiesValues() {
        $xml               = $this->saveDir . '/properties_values.xml';
        $xml               = file_get_contents($xml);
        $properties_values = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `properties_values`
SET
    `product_id` = :product_id,
    `property_id` = :property_id,
    `value` = :value,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `product_id` = :product_id,
    `property_id` = :property_id,
    `value` = :value,
    `delete` = 0";

        $this->updateTable($properties_values, 'value', 'properties_values', $query, array(
                                                                                          'product_id',
                                                                                          'property_id',
                                                                                          'value',
                                                                                     ), 8);
    }

    public function actionPhoto() {
        $xml           = $this->saveDir . '/product_fotos.xml';
        $xml           = file_get_contents($xml);
        $product_fotos = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `product_fotos`
SET
    `product_id` = :product_id,
    `foto_id` = :foto_id,
    `filename` = :filename,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `product_id` = :product_id,
    `foto_id` = :foto_id,
    `filename` = :filename,
    `delete` = 0";

        $this->updateTable($product_fotos, 'foto', 'product_fotos', $query, array(
                                                                                 'product_id',
                                                                                 'foto_id',
                                                                                 'filename',
                                                                            ), 9, false);
    }

    public function actionDeliveryMethods() {
        $xml = $this->saveDir . '/delivery_methods.xml';
        $xml = file_get_contents($xml);

        $delivery_methods = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `delivery_methods`
SET
    `delivery_method_id` = :delivery_method_id,
    `name` = :name,
    `description` = :description,
    `free_from` = :free_from,
    `price` = :price,
    `enabled` = :enabled,
    `delete` = 0
    ON DUPLICATE KEY UPDATE
    `delivery_method_id` = :delivery_method_id,
    `name` = :name,
    `description` = :description,
    `free_from` = :free_from,
    `price` = :price,
    `enabled` = :enabled,
    `delete` = 0";

        $this->updateTable($delivery_methods, 'delivery_method', 'delivery_methods', $query, array(
                                                                                                  'delivery_method_id',
                                                                                                  'name',
                                                                                                  'description',
                                                                                                  'free_from',
                                                                                                  'price',
                                                                                                  'enabled',
                                                                                             ), 10);
    }

    public function actionCurrencies() {
        $xml = $this->saveDir . '/currencies.xml';
        $xml = file_get_contents($xml);

        $currencies = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `currencies`
SET
    `currency_id` = :currency_id,
    `main` = :main,
    `def` = :def,
    `name` = :name,
    `sign` = :sign,
    `code` = :code,
    `rate_from` = :rate_from,
    `rate_to` = :rate_to,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `currency_id` = :currency_id,
    `main` = :main,
    `def` = :def,
    `name` = :name,
    `sign` = :sign,
    `code` = :code,
    `rate_from` = :rate_from,
    `rate_to` = :rate_to,
    `delete` = 0";

        $this->updateTable($currencies, 'currency', 'currencies', $query, array(
                                                                               'currency_id',
                                                                               'main',
                                                                               'def',
                                                                               'name',
                                                                               'sign',
                                                                               'code',
                                                                               'rate_from',
                                                                               'rate_to',
                                                                          ), 11);
    }

    public function actionPaymentMethods() {
        $xml = $this->saveDir . '/payment_methods.xml';
        $xml = file_get_contents($xml);

        $payment_methods = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `payment_methods`
SET
    `payment_method_id` = :payment_method_id,
    `module` = :module,
    `name` = :name,
    `description` = :description,
    `currency_id` = :currency_id,
    `params` = :params,
    `enabled` = :enabled,
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `payment_method_id` = :payment_method_id,
    `module` = :module,
    `name` = :name,
    `description` = :description,
    `currency_id` = :currency_id,
    `params` = :params,
    `enabled` = :enabled,
    `delete` = 0";

        $this->updateTable($payment_methods, 'payment_method', 'payment_methods', $query, array(
                                                                                               'payment_method_id',
                                                                                               'module',
                                                                                               'name',
                                                                                               'description',
                                                                                               'currency_id',
                                                                                               'params',
                                                                                               'enabled',
                                                                                          ), 12);
    }

    public function actionDeliveryPayments() {
        $xml = $this->saveDir . '/delivery_payments.xml';
        $xml = file_get_contents($xml);

        $delivery_payments = new SimpleXMLElement($xml);

        $query = "
INSERT INTO
    `delivery_payments`
SET
    `delivery_method_id` = :delivery_method_id, 
    `payment_method_id` = :payment_method_id, 
    `delete` = 0
ON DUPLICATE KEY UPDATE
    `delivery_method_id` = :delivery_method_id,
    `payment_method_id` = :payment_method_id,
    `delete` = 0";

        $this->updateTable($delivery_payments, 'delivery_payment', 'delivery_payments', $query, array(
                                                                                                     'delivery_method_id',
                                                                                                     'payment_method_id',
                                                                                                ), 13);
    }

    public function actionClear() {
        $files = glob($this->saveDir . '/*');
        foreach ($files as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }

        // Обновляем статус последнего обновления на 1
        $result = App::app()->db()->fetch("SELECT id FROM `update_history` ORDER BY `date` DESC");
        App::app()->db()->query("UPDATE `update_history` SET `status` = 1 WHERE `id` = :id", array(':id' => $result['id']));

        echo json_encode(array('step' => 15));
        App::app()->end();
    }

    protected function updateTable($rows, $rowName, $tableName, $query, $params, $step, $clearTable = false) {
        $echo = array('step' => $step);

        if (count($rows)) {
            if (!isset($_SESSION['update']['currentRow'])) {
                $_SESSION['update']['rows']       = count($rows);
                $_SESSION['update']['currentRow'] = 0;
                $_SESSION['update']['timeStart']  = time();

                if ($clearTable) {
                    // Если нужно очистить таблицу - очищаем
                    App::app()->db()->query("TRUNCATE TABLE `{$tableName}`");
                } else {
                    // Всем записям основной таблицы устанавливаем delete = 1
                    App::app()->db()->query("UPDATE `{$tableName}` SET `delete` = 1");
                }
            }

            $part = $_SESSION['update']['currentRow'] + 1000;
            if ($part > $_SESSION['update']['rows']) {
                $part = $_SESSION['update']['rows'];
            }

            App::app()->db()->query("START TRANSACTION");

            // Поочереди вставляем записи в основную таблицу, если нужно - обновляем
            for ($i = $_SESSION['update']['currentRow']; $i < $part; $i++) {
                $row = $rows->{$rowName}[$i];
                $_SESSION['update']['currentRow']++;

                if ($row) {
                    $_params = $this->prepareParams($params, $row);
                    App::app()->db()->query($query, $_params);
                }

                $echo['ready']    = round($_SESSION['update']['currentRow'] / $_SESSION['update']['rows'] * 100, 2);
                $timeElapsed      = time() - $_SESSION['update']['timeStart'];
                $echo['timeLeft'] = date('i минут s секунд', ((100 * $timeElapsed) / $echo['ready']) - $timeElapsed);
            }

            App::app()->db()->query("COMMIT");

            if ($part >= $_SESSION['update']['rows']) {
                unset($_SESSION['update']['currentRow']);
                unset($_SESSION['update']['rows']);
                unset($_SESSION['update']['timeStart']);

                $echo['step'] = $step + 1;
                unset($echo['ready']);

                // Удаляем данные из основной таблицы
                App::app()->db()->query("DELETE FROM `{$tableName}` WHERE `delete` = 1");
            }
        }

        echo json_encode($echo);
        App::app()->end();
    }

    protected function prepareParams($params, $row) {
        $return = array();
        foreach ($params as $param) {
            if (strpos($param, "int:") === 0) {
                $param                  = preg_replace('/^int:/', '', $param);
                $return["int:{$param}"] = (string)$row->$param;
            } else {
                $return[":{$param}"] = (string)$row->$param;
            }
        }

        return $return;
    }
}