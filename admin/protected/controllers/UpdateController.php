<?php
/**
 * UpdateController.php
 * Date: 24.07.13
 * Time: 13:48
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

App::import('//library/AdminController');

class UpdateController extends AdminController {

    public $mainTitle = "Обновление каталога";

    public function init() {
        App::import('/admin/protected/models/Update');
        App::import('/admin/protected/models/UpdateHistory');
        parent::init();
    }

    public function actionIndex() {
        $availableUpdate = (string)Update::available();
        $lastUpdate      = UpdateHistory::create()->findByQuery("SELECT * FROM `update_history` ORDER BY `id` DESC");

        $this->render('index', array(
                                    'availableUpdate' => $availableUpdate,
                                    'lastUpdate'      => $lastUpdate
                               ));
    }

    public function actionHistory() {
        $models = UpdateHistory::create()->findAll();
        $this->render('history', array('models' => $models));
    }

    public function actionUpdate() {
        $this->render('update');
    }
}