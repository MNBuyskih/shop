<?php
/**
 * LoginController.php
 * Date: 24.07.13
 * Time: 9:45
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class LoginController extends Controller {

    public $layout = "login";

    public function actionIndex() {
        App::import('/admin/protected/models/LoginForm');
        $model = new LoginForm();

        if (isset($_POST['LoginForm'])) {
            $model->setAttributes($_POST['LoginForm']);
            if ($model->login()) {
                $this->redirect('/admin');
            }
        }

        $this->render('index', array('model' => $model));
    }

    public function actionLogout() {
        if (App::app()->isAdmin()) {
            session_destroy();
        }
        $this->redirect('/admin');
    }
}