<?php
/**
 * UpdateCoreController.php
 * Date: 13.08.13
 * Time: 13:44
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

App::import('//library/AdminController');

class UpdateCoreController extends AdminController {

    public function actionIndex() {
        App::import('/admin/protected/models/UpdateCore');
        $update = UpdateCore::available();

        $this->render('index', array('update' => $update));
    }

    public function actionProcess() {
        $this->render('process');
    }

    public function actionDoit() {
        if (App::app()->functionEnabled('set_time_limit')) {
            set_time_limit(0);
        }

        App::import('/admin/protected/models/UpdateCore');
        $update  = new UpdateCore();
        $updates = $update->getBetween();

        foreach ($updates as $version => $update) {
            UpdateCore::apply($version);
        }

        UpdateCore::resetAvailable();
        $this->redirect('/admin/updateCore');
    }
}