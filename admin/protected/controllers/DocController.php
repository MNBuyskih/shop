<?php
/**
 * DocController.php
 * Date: 22.07.13
 * Time: 13:04
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

App::import('//library/AdminController');

class DocController extends AdminController {

    public $mainTitle = "Статический контент";

    public function init() {
        App::import('//models/Doc');
        parent::init();
    }

    public function actionIndex() {
        $models = Doc::create()->findAll();
        $this->render('index', array('models' => $models));
    }

    public function actionCreate() {
        $this->forward('update');
    }

    public function actionUpdate() {
        $id    = App::app()->getRequest()->getParam('id');
        $model = $id ? Doc::create()->find($id) : new Doc();

        if (isset($_POST['Doc'])) {
            $model->setAttributes($_POST['Doc']);
            if ($model->save()) {
                $this->redirect('/admin/doc');
            }
        }

        $this->render('form', array('model' => $model));
    }

    public function actionDelete() {
        $model = Doc::create()->find(App::app()->getRequest()->getParam('id'));
        $model->delete();
        $this->redirect('/admin/doc');
    }
}