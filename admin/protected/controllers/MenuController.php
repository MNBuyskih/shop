<?php
/**
 * Menu.php
 * Date: 23.07.13
 * Time: 13:37
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
App::import('//library/AdminController');

class MenuController extends AdminController {
    public $mainTitle = 'Пункты меню';

    public function init() {
        App::import('//models/MenuItem');
        parent::init();
    }

    public function actionIndex() {
        $models = MenuItem::create()->findAll();
        $this->render('index', array('models' => $models));
    }

    public function actionCreate() {
        $this->forward('update');
    }

    public function actionUpdate() {
        $id = App::app()->getRequest()->getParam('id');
        /** @var MenuItem $model */
        $model = $id ? MenuItem::create()->find($id) : new MenuItem();

        if (isset($_POST['MenuItem'])) {
            $model->setAttributes($_POST['MenuItem']);
            if ($model->save()) {
                $this->redirect('/admin/menu');
            }
        } else {
            $model->maxPosition();
        }

        $this->render('form', array('model' => $model));
    }

    public function actionDelete() {
        $model = MenuItem::create()->find(App::app()->getRequest()->getParam('id'));
        $model->delete();
        $this->redirect('/admin/menu');
    }
}