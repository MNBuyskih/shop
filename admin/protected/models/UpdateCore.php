<?php

/**
 * UpdateCore.php
 * Date: 20.08.13
 * Time: 13:19
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
class UpdateCore extends Model {

    protected $string;

    protected $xml;

    protected $updates;

    public function getString() {
        if (!$this->string) {
            $file   = App::app()->config['update']['serverUrl'] . '/core/new.xml';
            $string = @file_get_contents($file);

            if (!$string) {
                throw new HttpException(500, 'Не возможно получить информацию о доступных обновлениях. Проверьте настройки подключения к серверу обновлений.');
            }

            $this->string = $string;
        }

        return $this->string;
    }

    public function getXml() {
        if (!$this->xml) {
            $this->xml = new SimpleXMLElement($this->getString());
        }

        return $this->xml;
    }

    public function getUpdates() {
        if (!$this->updates) {
            $this->updates = array();
            foreach ($this->getXml()->update as $update) {
                $this->updates[(string)$update->version] = array(
                    'time'    => (int)$update->time,
                    'version' => (string)$update->version,
                    'notes'   => (string)$update->notes,
                );
            }
        }

        return $this->updates;
    }

    public function get($version) {
        $updates = $this->getUpdates();

        return isset($updates[$version]) ? $updates['version'] : null;
    }

    public function getLast() {
        $updates = $this->getUpdates();

        return array_pop($updates);
    }

    /**
     * Сравнивает текущую и последниюю доступную версии.
     * Возвращает true если доступна новая версия
     *
     * @param int $repoV
     * @param int $currentV
     *
     * @return mixed
     */
    public function compare($repoV = null, $currentV = null) {
        if ($repoV === null) {
            $repoV = $this->getLast();
            $repoV = $repoV['version'];
        }

        if ($currentV === null) {
            $currentV = App::app()->version;
        }

        if (strpos($repoV, 'v') === 0 && strpos($currentV, 'v') !== 0) {
            $currentV = 'v' . $currentV;
        }

        return version_compare($repoV, $currentV, '>');
    }

    public function getBetween() {
        $currentVersion = App::app()->version;

        $updates = array();
        foreach ($this->getUpdates() as $version => $update) {
            if ($this->compare($version, $currentVersion)) {
                $updates[$version] = $update;
            }
        }

        return $updates;
    }

    public static function available() {
        App::app()->startSession();
        if (!isset($_SESSION['update-available-core'])) {
            $update = new self();
            if ($update->compare()) {
                $_SESSION['update-available-core'] = $update->getLast();
            } else {
                $_SESSION['update-available-core'] = false;
            }
        }

        return $_SESSION['update-available-core'];
    }

    public static function resetAvailable() {
        App::app()->startSession();
        unset($_SESSION['update-available-core']);
    }

    public static function apply($version) {
        $file   = App::app()->config['update']['serverUrl'] . '/core/update-' . $version . '.zip';
        $string = @file_get_contents($file);

        if (!$string) {
            throw new HttpException(500, 'Не возможно скачать обновление. Возможно сервер обновлений не доступен. Повторите попытку позже');
        }

        $dirName  = App::app()->getPathOfAlias('/admin/update-temp');
        $saveDir  = $dirName . '/' . $version;
        $fileName = $dirName . '/update-' . $version . '.zip';

        file_put_contents($fileName, $string);

        if (!file_exists($saveDir) && !is_dir($saveDir)) {
            mkdir($saveDir, 0777, true);
        }

        $zip = new ZipArchive();
        $zip->open($fileName);
        $zip->extractTo($saveDir);
        $zip->close();

        self::applyDir($saveDir, $saveDir);
        self::removeDir($saveDir);
        unlink($fileName);
    }

    protected static function applyDir($dir, $saveDir) {
        $files = glob($dir . "/*");
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::applyDir($file, $saveDir);
            } else {
                self::applyFile($file, $saveDir);
            }
        }
    }

    protected static function applyFile($srcFile, $saveDir) {
        $dFile = self::destFile($srcFile, $saveDir);
        if ($dFile) {
            $content = file_get_contents($srcFile);
            if (!@file_put_contents($dFile, $content)) {
                throw new HttpException(500, 'Не удалось применить обновление. Необходимо дать доступ вебсерверу на перезапись файлов сайта.');
            }
        }
    }

    protected static function destFile($srcFile, $saveDir) {
        $excluded = array('/update.xml');
        $destFile = str_replace($saveDir, App::app()->basePath, $srcFile);
        foreach ($excluded as $file) {
            $file = App::app()->basePath . $file;
            if ($file === $destFile) {
                return false;
            }
        }

        return $destFile;
    }

    protected static function removeDir($dir) {
        $files = glob($dir . '/*');
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::removeDir($file);
            } else {
                unlink($file);
            }
        }

        return rmdir($dir);
    }
}