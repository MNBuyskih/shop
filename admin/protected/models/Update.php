<?php
/**
 * Update.php
 * Date: 24.07.13
 * Time: 14:36
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Update extends Model {
    /**
     * @param string $className
     *
     * @return Update
     */
    public static function create($className = __CLASS__) {
        return parent::create($className);
    }

    public static function available() {
        App::app()->startSession();
        if (empty($_SESSION['update']['available'])) {
            $file   = App::app()->config['update']['serverUrl'] . '/xml/new.xml';
            $string = @file_get_contents($file);

            if (!$string) {
                throw new HttpException(500, 'Не возможно получить информацию о доступных обновлениях. Проверьте настройки подключения к серверу обновлений.');
            }

            $time = $_SESSION['update']['available'] = (string)new SimpleXMLElement($string);
        } else {
            $time = $_SESSION['update']['available'];
        }

        $model = self::create()->findByQuery("SELECT * FROM `update_history` WHERE `date` >= :date AND `status` = 1", array(':date' => $time));
        if (!$model) {
            return $time;
        }

        return null;
    }

    public static function availableCore() {
        App::app()->startSession();
        if (empty($_SESSION['updateCore']['availableCore'])) {
            $file   = App::app()->config['update']['serverUrl'] . '/core/new.xml';
            $string = @file_get_contents($file);

            if (!$string) {
                throw new HttpException(500, 'Не возможно получить информацию о доступных обновлениях. Проверьте настройки подключения к серверу обновлений.');
            }

            $xml     = new SimpleXMLElement($string);
            $updates = array();
            if ($xml->count() === 1) {
                $updates[] = $xml->update;
            } else {
                foreach ($xml->update as $update) {
                    $updates[] = $update;
                }
            }

            $lastUpdate = (array)array_pop($updates);

            $version = $_SESSION['updateCore']['version'] = $lastUpdate['version'];
        } else {
            $version = $_SESSION['updateCore']['version'];
        }

        if (version_compare($version, App::app()->version) !== 0) {
            return $version;
        }

        return null;
    }
}