<?php
/**
 * .php
 * Date: 24.07.13
 * Time: 9:46
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class LoginForm extends Model {
    public $username;

    public $password;

    public function validate($attributes = array()) {
        $this->username = trim($this->username);
        $this->password = trim($this->password);

        if (empty($this->username)) {
            $this->addError('username', 'Все поля обязательны для заполнения');
        }

        if (empty($this->password)) {
            $this->addError('password', 'Все поля обязательны для заполнения');
        }

        $password = md5($this->password);
        if (parent::validate() && ($this->username !== 'admin' || $password !== App::app()->config['admin'])) {
            $this->addError('password', 'Не верный логин или пароль');
        }

        return parent::validate();
    }

    public function attributeLabels() {
        return array(
            'username' => 'Имя пользователя',
            'password' => 'Пароль',
        );
    }

    public function login() {
        if (!$this->validate()) {
            return false;
        }

        if (!isset($_SESSION)) {
            session_start();
        }

        $_SESSION['admin'] = 1;

        return true;
    }
}