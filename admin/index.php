<?php
/**
 * index.php
 * Date: 22.07.13
 * Time: 12:13
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

require_once "../protected/library/App.php";
$app           = App::createApplication(realpath(dirname(__FILE__) . '/../protected/config/admin.php'));
$app->basePath = realpath(dirname(__FILE__) . '/../');
$app->baseUrl  = str_replace(App::documentRoot(), '', $app->basePath);
$app->run();