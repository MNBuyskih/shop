<?php
/**
 * @var $controller Controller
 * @var $content    string
 */

?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title><?php echo  $controller->pageTitle ?></title>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
</head>
<body>
<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="<?php echo  App::app()->baseUrl ?>"><?php echo  App::app()->config['name'] ?></a>
            <ul class="nav pull-right">
                <li><a href="<?php echo  App::app()->createUrl() ?>" target="_blank">Перейти на сайт</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container" id="page">
    <?php echo  $content ?>
</div>
</body>
</html>