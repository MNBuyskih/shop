<?php
/**
 * @var $controller Controller
 * @var $content    string
 */

$defaultTheme = new Theme('default');
if (empty($controller->pageTitle) && !empty($controller->breadcrumbs)) {
    ob_start();
    $pageTitle = $controller->widget('Breadcrumbs', array(
                                                         'links'    => $controller->breadcrumbs,
                                                         'mainPage' => false
                                                    ));
    $pageTitle = ob_get_clean();

    $controller->pageTitle = strip_tags($pageTitle);
    $controller->pageTitle = str_replace("\n", " ", $controller->pageTitle);
}
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title><?php echo  $controller->pageTitle ?></title>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo  $defaultTheme->getBaseUrl() ?>/css/style.css"/>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="<?php echo  App::app()->baseUrl ?>/"><?php echo  App::app()->config['name'] ?></a>
            <?php if (App::app()->isAdmin()) { ?>
                <ul class="nav">
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Управление контентом
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo  App::app()->createUrl('admin/doc') ?>">Статический контент</a></li>
                            <li><a href="<?php echo  App::app()->createUrl('admin/menu') ?>">Меню контента</a></li>
                        </ul>
                    </li>
                    <li class="divider-vertical"></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            Настройки
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo  App::app()->createUrl('admin/update') ?>"><i class="icon-refresh"></i> Обновление каталога</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav pull-right">
                    <li><a href="<?php echo  App::app()->baseUrl ?>/" target="_blank">Перейти на сайт</a></li>
                    <li><a href="<?php echo  App::app()->createUrl('/admin/login/logout') ?>">Выход</a></li>
                </ul>
            <?php } ?>
        </div>
    </div>
</div>
<div class="container" id="page">
    <div class="row-fluid">
        <?php $controller->widget('NewUpdate') ?>
        <?php $controller->widget('NewUpdateCore') ?>
        <?php $controller->widget('InstallDir') ?>
    </div>
    <?php
    if ($controller->breadcrumbs) {
        $controller->widget('Breadcrumbs', array(
                                                'mainPageUrl' => 'admin',
                                                'links'       => $controller->breadcrumbs
                                           ));
    }
    if ($controller->mainTitle) {
        echo "<h1>{$controller->mainTitle}</h1>";
    }
    if ($controller->mainDescription) {
        echo "<h4>{$controller->mainDescription}</h4>";
    }
    if ($controller->breadcrumbs || $controller->mainTitle || $controller->mainDescription) {
        echo "<hr>";
    }
    ?>
    <?php echo  $content ?>
</div>
<div class="footer container">
    <hr>
    <a target="_blank" href="//www.mmamarket.ru">www.mmamarket.ru</a> партнёрский магазин
    <?php echo  App::app()->version ?>
</div>
</body>
</html>