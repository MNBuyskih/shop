<?php
/**
 * form.php
 * Date: 22.07.13
 * Time: 14:28
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller DocController
 * @var $model      Doc
 * @var $form       ActiveFormHorizontal
 */

$controller->mainDescription = $model->id ? "Редактирование статической страницы" : "Добавление статической страницы";
$controller->breadcrumbs     = array(
    $controller->mainTitle => array('/admin/doc'),
    $controller->mainDescription,
);

$form = $controller->widget('ActiveFormHorizontal', array('model' => $model));

echo $form->errors();

echo $form->textFieldRow('title');
echo $form->textFieldRow('alias');
$controller->widget('CKEditor', array(
                                     'form'      => $form,
                                     'attribute' => 'text'
                                ));
echo $form->textAreaRow('keywords');
echo $form->checkBoxRow('addMenu');
echo $form->checkBoxRow('addMain');
echo $form->submitRow('Сохранить', array('class' => 'btn-primary'));

$form->endForm();
