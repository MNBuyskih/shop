<?php
/**
 * @var DocController $controller
 * @var Doc[]         $models
 */

$controller->mainTitle = "Статический контент";
$controller->mainDescription = "Управление статическим контентом.";
$controller->breadcrumbs = array(
    $controller->mainTitle
);
?>
    <a href="<?php echo  $controller->createUrl('admin/doc/create') ?>">Добавить страницу</a>

<?php
$controller->widget('GridView', array(
                                     'data'    => $models,
                                     'columns' => array(
                                         array(
                                             'name'   => 'title',
                                             'header' => "Название"
                                         ),
                                         array(
                                             'class'           => "LinkColumn",
                                             'header'          => "URL",
                                             'urlExpression'   => '$data->url()',
                                             'labelExpression' => '$data->url()',
                                         ),
                                         array(
                                             'class'    => 'ButtonsColumn',
                                             'template' => "{update} {delete}",
                                             'buttons'  => array(
                                                 'update' => array(
                                                     'urlExpression' => 'App::app()->createUrl(array("/admin/doc/update", "id"=>$data->id))',
                                                     'label'         => "Редактировать",
                                                     'icon'          => 'pencil',
                                                 ),
                                                 'delete' => array(
                                                     'urlExpression' => 'App::app()->createUrl(array("/admin/doc/delete", "id"=>$data->id))',
                                                     'label'         => "Удалить",
                                                     'icon'          => 'trash',
                                                     'htmlOptions'   => array(
                                                         'onclick' => 'return confirm(\'Удалить строку?\')'
                                                     ),
                                                 ),
                                             ),
                                         ),
                                     ),
                                ));
?>