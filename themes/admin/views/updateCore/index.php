<?php
/**
 * index.php
 * Date: 20.08.13
 * Time: 13:19
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller UpdateCoreController
 * @var $update     array
 */
?>
<?php if ($update) { ?>
    <div class="hero-unit">
        <h1>Новая версия: <?php echo  $update['version'] ?></h1>

        <div><?php echo  $update['notes'] ?></div>
        <a href="<?php echo  App::app()->createUrl('/admin/updateCore/process') ?>" class="btn btn-primary btn-large">Начать обновление</a>
    </div>
<?php } else { ?>
    <div class="hero-unit">
        <h1>Ваша система не требует обновления</h1>
    </div>
<?php } ?>