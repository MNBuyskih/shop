<?php
/**
 * process.php
 * Date: 20.08.13
 * Time: 14:08
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller UpdateCoreController
 */
?>

<div class="alert alert-error"><strong>Не закрывайте и не обновляйте страницу во избежание ошибок</strong></div>
<div class="progress progress-striped active">
    <div class="bar" style="width: 100%;"></div>
</div>
<div class="alert alert-info">Выполняется обновление. Подождите...</div>
<script type="text/javascript">
    document.location.href = "<?php echo App::app()->createUrl('/admin/updateCore/doit')?>";
</script>