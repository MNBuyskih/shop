<?php
/**
 * index.php
 * Date: 24.07.13
 * Time: 9:52
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller  LoginController
 * @var $model       LoginForm
 * @var $form        ActiveFormHorizontal
 */

$controller->pageTitle = "Авторизация";
$controller->breadcrumbs = false;
?>
<div class="modal" style="top: 127px;">
    <?php $form = $controller->widget('ActiveFormHorizontal', array(
                                                                'model'       => $model,
                                                                'htmlOptions' => array(
                                                                    'method' => 'post',
                                                                    'style'  => 'margin: 0'
                                                                )
                                                           )); ?>
    <div class="modal-header">
        <h4>Авторизация</h4>
    </div>
    <div class="modal-body">
        <?php
        echo $form->textFieldRow('username');
        echo $form->textFieldRow('password', array('type' => 'password'));
        ?>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Войти</button>
    </div>
    <?php $form->endForm(); ?>
</div>