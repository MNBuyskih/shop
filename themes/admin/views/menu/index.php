<?php
/**
 * index.php
 * Date: 23.07.13
 * Time: 13:42
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller   MenuController
 * @var $models       array
 */

$controller->mainDescription = "Просмотр пунктов меню.";
$controller->breadcrumbs = array(
    $controller->mainTitle
);

?>
    <a href="<?php echo  $controller->createUrl('admin/menu/create') ?>">Добавить пункт меню</a>

<?php
$controller->widget('GridView', array(
                                     'data'    => $models,
                                     'columns' => array(
                                         array(
                                             'name'   => 'title',
                                             'header' => "Название"
                                         ),
                                         array(
                                             'class'           => "LinkColumn",
                                             'header'          => "URL",
                                             'urlExpression'   => '$data->url',
                                             'labelExpression' => '$data->url',
                                         ),
                                         array(
                                             'class'    => 'ButtonsColumn',
                                             'template' => "{update} {delete}",
                                             'buttons'  => array(
                                                 'update' => array(
                                                     'urlExpression' => 'App::app()->createUrl(array("/admin/menu/update", "id"=>$data->id))',
                                                     'label'         => "Редактировать",
                                                     'icon'          => 'pencil',
                                                 ),
                                                 'delete' => array(
                                                     'urlExpression' => 'App::app()->createUrl(array("/admin/menu/delete", "id"=>$data->id))',
                                                     'label'         => "Удалить",
                                                     'icon'          => 'trash',
                                                     'htmlOptions'   => array(
                                                         'onclick' => 'return confirm(\'Удалить строку?\')'
                                                     ),
                                                 ),
                                             ),
                                         ),
                                     ),
                                ));
?>