<?php
/**
 * form.php
 * Date: 23.07.13
 * Time: 13:48
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller MenuController
 * @var $model      MenuItem
 * @var $form       ActiveFormHorizontal
 */

$controller->mainDescription = ($model->id ? "Редактирование" : "Добавление") . " пункта меню";
$controller->breadcrumbs     = array(
    $controller->mainTitle => array('/admin/menu'),
    $controller->mainDescription,
);

$form = $controller->widget('ActiveFormHorizontal', array('model' => $model));

echo $form->errors();

echo $form->textFieldRow('title');
echo $form->textFieldRow('url');
echo $form->textFieldRow('position');
echo $form->checkBoxRow('blank');
echo $form->submitRow('Сохранить', array('class' => 'btn-primary'));

$form->endForm();
