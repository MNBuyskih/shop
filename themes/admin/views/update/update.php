<?php
/**
 * update.php
 * Date: 24.07.13
 * Time: 16:28
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller UpdateController
 */

$controller->mainDescription = "Обновить каталог";
$controller->breadcrumbs = array(
    $controller->mainTitle => array('/admin/update'),
    $controller->mainDescription
);

?>
<div class="alert">
    <strong>Внимание!</strong> Не закрывайте и не обновляйте страницу во избежание ошибок.
</div>

<div class="progress active progress-striped">
    <div class="bar" id="bar"></div>
</div>

<div class="progress active progress-striped" style="height: 10px">
    <div class="bar" id="subBar"></div>
</div>

<div class="alert alert-info">
    <strong>Выполняется</strong>: <span id="info"></span>
</div>

<script type="text/javascript">
    var Updater = function (options) {

        var $this = this;

        this.options = options;

        this.progress = 0;

        this.progressInc = Math.round(100 / 14);

        this.step = 0;

        this.setProgress = function (progress) {
            this.progress = progress !== undefined ? progress : this.progress + this.progressInc;
            this.options.bar.width(this.progress + '%');
        };

        this.setSubProgress = function (progress) {
            this.options.subBar.width(progress + '%');
        };

        this.setInfo = function (message) {
            this.options.info.html(message);
        };

        this.start = function () {
            this.setProgress(0);
            this.setInfo('Начинаем обновление');
            $.ajaxSetup({
                type: 'post',
                dataType: 'json'
            });
            $(document).ajaxError($this.ajaxError);
            $(document).ajaxComplete($this.nextStep);

            this.nextStep(undefined, {responseJSON: {step: 1}});
        };

        /**
         *
         * @param message
         * @param data
         * @param data.timeLeft
         * @param data.ready
         */
        this.step = function (message, data) {
            this.setInfo(message);

            if (data.ready) {
                this.setInfo(message + '. Готово: ' + data.ready + '%. Осталось: ' + data.timeLeft);
                $this.setSubProgress(data.ready);
            } else {
                $this.setSubProgress(0);
                this.setProgress();
            }
        };

        this.step1 = function () {
            this.setInfo('Загрузка архива обновлений');
            this.setProgress();
            $.ajax(this.options.baseUrl + '/admin/updateProcess/download');
        };

        this.step2 = function () {
            this.setInfo('Распаковка архива обновлений');
            this.setProgress();
            $.ajax(this.options.baseUrl + '/admin/updateProcess/unzip');
        };

        this.step3 = function (data) {
            this.step('Обновляем бренды', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/brands');
        };

        this.step4 = function (data) {
            this.step('Обновляем категории', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/categories');
        };

        this.step5 = function (data) {
            this.step('Обновляем товары', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/products');
        };

        this.step6 = function (data) {
            this.step('Обновляем варианты товаров', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/variants');
        };

        this.step7 = function (data) {
            this.step('Обновляем свойства товаров', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/properties');
        };

        this.step8 = function (data) {
            this.step('Обновляем значения свойств', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/propertiesValues');
        };

        this.step9 = function (data) {
            this.step('Обновляем фото товаров', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/photo');
        };

        this.step10 = function (data) {
            this.step('Обновляем способы доставки', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/deliveryMethods');
        };

        this.step11 = function (data) {
            this.step('Обновляем валюты', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/currencies');
        };

        this.step12 = function (data) {
            this.step('Обновляем способы оплаты', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/paymentMethods');
        };

        this.step13 = function (data) {
            this.step('Обновляем привязки способов оплаты к способам доставки', data);
            $.ajax(this.options.baseUrl + '/admin/updateProcess/deliveryPayments');
        };

        this.step14 = function () {
            this.setInfo('Удаляем временные файлы');
            this.setProgress();
            $.ajax(this.options.baseUrl + '/admin/updateProcess/clear');
        };

        this.finish = function () {
            this.setInfo('Обновление успешно завершено');
            $this.options.info.closest('div').removeClass('alert-info').addClass('alert-success');
            this.setProgress(100);
            setTimeout(function () {
                document.location.href = $this.options.baseUrl + '/admin/update';
            }, 1000);
        };

        this.nextStep = function (event, xhr) {
            var data = xhr.responseJSON;
            if (!data || !data.step) return;
            switch (data.step) {
                case 1:
                    $this.step1();
                    break;
                case 2:
                    $this.step2();
                    break;
                case 3:
                    $this.step3(data);
                    break;
                case 4:
                    $this.step4(data);
                    break;
                case 5:
                    $this.step5(data);
                    break;
                case 6:
                    $this.step6(data);
                    break;
                case 7:
                    $this.step7(data);
                    break;
                case 8:
                    $this.step8(data);
                    break;
                case 9:
                    $this.step9(data);
                    break;
                case 10:
                    $this.step10(data);
                    break;
                case 11:
                    $this.step11(data);
                    break;
                case 12:
                    $this.step12(data);
                    break;
                case 13:
                    $this.step13(data);
                    break;
                case 14:
                    $this.step14();
                    break;
                case 15:
                    $this.finish();
                    break;
            }
        };

        this.ajaxError = function (event, xhr) {
            var div = $this.options.info.closest('div');
            div.removeClass('alert-info').addClass('alert-error').html(div.html() + '<br>Произошла ошибка во время последней операции: <strong>' + xhr.status + ": " + xhr.statusText + '</strong><br>Если ошибка повторится обратитесь к разработчикам.');
        };
    };

    var update = new Updater({
        baseUrl: "<?php echo App::app()->baseUrl?>",
        info: $('#info'),
        bar: $('#bar'),
        subBar: $('#subBar')
    });
    update.start();
</script>