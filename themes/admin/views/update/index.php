<?php
/**
 * index.php
 * Date: 24.07.13
 * Time: 13:55
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller      UpdateController
 * @var $availableUpdate integer|bool
 * @var $lastUpdate      UpdateHistory
 */

$controller->mainDescription = "Обновление данных каталога, товаров, цен и способов доставки.";
$controller->breadcrumbs = array(
    $controller->mainTitle
);
?>

<div class="span12">
    <div class="thumbnails">
        <div class="span4">
            <div class="thumbnail">
                <h5>Последнее обновление:</h5>

                <?php if (!$lastUpdate) { ?>
                    <p class="text-warning">Обновлениея еще не производились</p>
                <?php } else { ?>
                    проводилось <?php echo  date('d.m.Y H:i', $lastUpdate->date) ?><br>
                    статус <?php echo  ($lastUpdate->status) ? "<strong class='text-success'>успешно</strong>" : "<strong class='text-error'>с ошибкой</strong>" ?><br><br>
                    <a class="btn btn-primary" href="<?php echo  App::app()->createUrl('/admin/update/history') ?>">История обновления</a>
                <?php } ?>

            </div>
        </div>
        <div class="span8">
            <div class="thumbnail">
                <h5>Доступное обновление:</h5>

                <?php if ($availableUpdate) { ?>
                    <div class="alert alert-error">
                        Доступно обновление от <?php echo  date('d.m.Y H:i', $availableUpdate) ?>
                        <a class="btn btn-danger pull-right" href="<?php echo  App::app()->createUrl('/admin/update/update') ?>"><i class="icon-refresh icon-white"></i> Обновить!</a>

                        <div class="clearfix"></div>
                    </div>
                <?php } else { ?>
                    <p class="text-success">Доступных обновлений нет. Ваша система обновлена до сомой последней версии.</p>
                    <!--<a class="btn btn-primary" href="--><?//= App::app()->createUrl('/admin/update/historyLast') ?><!--">Посмотреть список изменений последнего обновления</a>-->
                <?php } ?>
            </div>
        </div>
    </div>
</div>