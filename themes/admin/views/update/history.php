<?php
/**
 * history.php
 * Date: 24.07.13
 * Time: 14:17
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller UpdateController
 * @var $models     UpdateHistory[]
 * @var $data       UpdateHistory
 */

$controller->mainDescription = "История обновлений";
$controller->breadcrumbs     = array(
    $controller->mainTitle => array('/admin/update'),
    $controller->mainDescription
);

$controller->widget('GridView', array(
                                     'data'    => $models,
                                     'columns' => array(
                                         array(
                                             'header' => 'Дата',
                                             'value'  => 'date("d.m.Y H:i", $data->date)'
                                         ),
                                         array(
                                             'name'   => 'status',
                                             'header' => 'Статус',
                                             'value'  => '$data->status ? "Успешно" : "С ошибкой"'
                                         ),
                                     ),
                                ));