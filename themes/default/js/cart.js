$.fn.CartForm = function () {
    $(this).submit(function (e) {
        e.preventDefault();

        var $this = $(this);
        $.ajax({
            url:      $this.attr('action'),
            type:     'post',
            data:     $this.serialize(),
            dataType: 'json',
            /**
             * @param response
             * @param response.total
             */
            success:  function (response) {
                $('#cart-total').html("(" + response.total + ")");
                var title = $this.find('p.title').html(),
                    size = $this.find('option:selected').html(),
                    modalWindow = new Modal({
                        id:                "MyWindow",
                        header:            "Успешно!",
                        footer:            true,
                        footerCloseButton: "Ok"
                    });
                modalWindow.getBody().html('Товар <strong>"' + title + '"</strong> размер <strong>"' + size +
                    '"</strong> успешно добавлен в корзину');
                modalWindow.window.find('.btn').addClass('btn-primary');
                modalWindow.show();
            },
            error:    function (xhr) {
                alert("Неожиданная ошибка:\n" + xhr.status + ", " + xhr.statusText + " (" + xhr.responseText + ")");
            }
        });
    });
};