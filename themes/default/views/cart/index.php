<?php
/**
 * index.php
 * Date: 07.08.13
 * Time: 13:12
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $data       array
 * @var $controller CartController
 */

$controller->mainTitle = "Корзина покупок";
$controller->breadcrumbs = array($controller->mainTitle);

?>
<form action="<?php echo  App::app()->createUrl('/cart/calculate') ?>" method="post" class="cart-grid">
    <?php
    $controller->widget('GridView', array(
                                         'emptyText' => 'Ваша корзина пуста',
                                         'data'      => $data,
                                         'columns'   => array(
                                             array(
                                                 'header' => 'Товар',
                                                 'value'  => 'Html::tag("a", array("href" => $data["product"]->url(), "class" => "img"), Html::tag("img", array("src" => $data["product"]->small_image))) . " " . $data["product"]->model',
                                             ),
                                             array(
                                                 'header' => 'Варианат',
                                                 'value'  => '$data["variant"]->name',
                                             ),
                                             array(
                                                 'header' => 'Цена',
                                                 'value'  => '$data["variant"]->price',
                                             ),
                                             array(
                                                 'header' => 'Количество',
                                                 'value'  => '"<input min=\"0\" type=\"number\" class=\"input-mini\" max=\"". $data["variant"]->stock . "\" value=\"" . $data["count"] . "\" name=\"count[". $data["variant"]->variant_id ."]\"/>"',
                                             ),
                                             array(
                                                 'header' => 'Сумма',
                                                 'value'  => '$data["variant"]->price * $data["count"]',
                                             ),
                                             array(
                                                 'header' => 'Удалить',
                                                 'value'  => '"<a href=\"". App::app()->createUrl(array("/cart/delete", "variant" => $data["variant"]->variant_id)) ."\" class=\"btn btn-primary\"><i class=\"icon-remove icon-white\"></i></a>"'
                                             ),
                                         )
                                    ));

    ?>

    <button type="submit" class="btn btn-primary"><i class="icon-refresh icon-white"></i> Сохранить изменения</button>
    <a class="btn btn-danger" href="<?php echo  App::app()->createUrl('/cart/clear') ?>"><i class="icon-trash icon-white"></i> Очистить корзину</a>
    <button name="process" type="submit" class="btn btn-success"><i class="icon-ok icon-white"></i> Оформить заказ</button>
</form>