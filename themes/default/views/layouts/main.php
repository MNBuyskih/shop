<?php
/**
 * @var $controller Controller
 * @var $content    string
 */
App::import('//models/MenuItem');
if (empty($controller->pageTitle) && !empty($controller->breadcrumbs)) {
    ob_start();
    $pageTitle = $controller->widget('Breadcrumbs', array(
                                                         'links'    => $controller->breadcrumbs,
                                                         'mainPage' => false
                                                    ));
    $pageTitle = ob_get_clean();

    $controller->pageTitle = strip_tags($pageTitle);
    $controller->pageTitle = str_replace("\n", " ", $controller->pageTitle);
}

$controller->menu = Catalog::fullMenu();
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title><?php echo  $controller->pageTitle ?></title>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo  App::app()->getTheme()->getBaseUrl() ?>/css/style.css"/>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <script src="http://rawgithub.com/MNBuyskih/6214470/raw/53b99a86089c04019ba42f0cc178382548c4f08e/theme.js"
            type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo  App::app()->getTheme()->getBaseUrl() ?>/js/cart.js"></script>
    <script type="text/javascript">$(document).ready(function () {$('.cart-form').CartForm()});</script>
</head>
<body>
<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="<?php echo  App::app()->createUrl('/') ?>"><?php echo  App::app()->config['name'] ?></a>
            <?php
            $controller->widget('Menu', array('items' => MenuItem::menuList()));

            App::import('//models/CartCookie');
            $controller->widget('Menu', array(
                                             'htmlOptions' => array('class' => 'pull-right'),
                                             'items'       => array(
                                                 array(
                                                     'url'   => App::app()->createUrl('/cart'),
                                                     'label' => '<i class="icon-shopping-cart icon-white"></i> Корзина <span id="cart-total">' . (CartCookie::create()->getTotalCount() ? "(" . CartCookie::create()->getTotalCount() . ")" : "") . '</span>',
                                                 )
                                             ),
                                        ));
            $controller->widget('Menu', array(
                                             'htmlOptions' => array('class' => 'pull-right'),
                                             'items'       => array(
                                                 array(
                                                     'url'     => App::app()->createUrl('/admin'),
                                                     'label'   => 'Админка',
                                                     'visible' => App::app()->isAdmin()
                                                 ),
                                                 array(
                                                     'url'     => App::app()->createUrl('/admin/login/logout'),
                                                     'label'   => 'Выход',
                                                     'visible' => App::app()->isAdmin()
                                                 ),
                                             )
                                        ));
            ?>
        </div>
    </div>
</div>
<div class="container" id="page">
    <div class="row-fluid">
        <?php if ($controller->menu) { ?>
        <div class="span3">
            <div class="sidebar well">
                <?php $controller->widget('Menu', array(
                                                    'items'       => $controller->menu,
                                                    'htmlOptions' => array('class' => 'nav-list')
                                               )) ?>
            </div>
        </div>
        <div class="span9">
            <?php } else { ?>
            <div class="span12">
                <?php } ?>
                <?php
                if ($controller->breadcrumbs) {
                    $controller->widget('Breadcrumbs', array(
                                                            'links' => $controller->breadcrumbs
                                                       ));
                }
                if ($controller->mainTitle) {
                    echo "<h1>{$controller->mainTitle}</h1>";
                }
                if ($controller->mainDescription) {
                    echo "<h4>{$controller->mainDescription}</h4>";
                }

                echo $content;
                ?>
            </div>
        </div>
        <div class="footer">
            <hr>
            &copy;<?php echo  date('Y') ?> My Company LTD
        </div>
    </div>
</body>
</html>