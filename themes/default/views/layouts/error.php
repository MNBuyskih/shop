<?php
/**
 * @var $controller ErrorController
 * @var $content    string
 */
App::import('//models/MenuItem');
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title><?php echo  $controller->pageTitle ?></title>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo  App::app()->getTheme()->getBaseUrl() ?>/css/style.css"/>
</head>
<body>
<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="<?php echo  App::app()->baseUrl ?>"><?php echo  App::app()->config['name'] ?></a>
        </div>
    </div>
</div>
<div class="container" id="page">
    <div class="row-fluid">
        <div class="span12">
            <?php
            if ($controller->breadcrumbs) {
                $controller->widget('Breadcrumbs', array(
                                                        'links' => $controller->breadcrumbs
                                                   ));
            }
            if ($controller->mainTitle) {
                echo "<h1>{$controller->mainTitle}</h1>";
            }
            if ($controller->mainDescription) {
                echo "<h4>{$controller->mainDescription}</h4>";
            }

            echo $content;
            ?>
        </div>
    </div>
    <div class="footer">
        <hr>
        &copy;<?php echo  date('Y') ?> My Company LTD
    </div>
</div>
</body>
</html>