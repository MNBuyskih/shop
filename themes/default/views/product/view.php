<?php
/**
 * view.php
 * Date: 25.07.13
 * Time: 16:30
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller ProductController
 * @var $model      Products
 * @var $form       ActiveForm
 * @var $cartForm   CartForm
 */

$controller->mainTitle = htmlspecialchars($model->model);

/** @var Variants[] $variants */
$variants = $model->variants();
$photos = $model->photos();

$image = count($photos) ? $photos[0]['filename'] : $model->large_image;
?>

<div class="row-fluid">
    <div class="span4">
        <div id="bigImage" class="thumbnails">
            <div class="thumbnail"><img src="<?php echo  $image ?>" alt=""/></div>
        </div>
        <?php if (count($photos) > 1) { ?>
            <ul class="thumbnails photos">
                <?php foreach ($photos as $photo) { ?>
                    <li class="span4">
                        <div class="thumbnail">
                            <a href="<?php echo  $photo['filename'] ?>"><img src="<?php echo  $photo['filename'] ?>" alt=""/></a>
                        </div>
                    </li>
                <?php } ?>
            </ul>
        <?php } ?>
    </div>

    <div class="span8">
        <?php $form = $controller->widget('ActiveForm', array(
                                                          'model'       => $cartForm,
                                                          'htmlOptions' => array(
                                                              'action' => App::app()->createUrl('/cart/add'),
                                                              'method' => 'post',
                                                              'id'     => 'cart-form',
                                                          )
                                                     )) ?>

        <p class="title hide"><?php echo  $model->model ?></p>
        <p class="price">Цена: <strong><?php echo  $variants[0]->price ?></strong></p>

        <p class="variant">
            <?php echo  $form->dropDownRow('variant', Html::listData($variants, 'variant_id', 'name')) ?>
        </p>

        <?php foreach ($variants as $variant) { ?>
            <input type="hidden" id="price_<?php echo  $variant->variant_id ?>" value="<?php echo  $variant->price ?>"/>
            <input type="hidden" id="stock_<?php echo  $variant->variant_id ?>" value="<?php echo  $variant->stock ?>"/>
        <?php } ?>
        <?php echo
        $form->textFieldRow('count', array(
                                          'value' => 1,
                                          'type'  => 'number',
                                          'min'   => 1,
                                          'max'   => $variants[0]->stock,
                                     )) ?><br>
        <?php echo  $form->submit('В корзину', array('class' => 'btn btn-primary')) ?>

        <?php $form->endForm() ?>
    </div>
</div>

<script type="text/javascript">
    $('#CartForm_variant').change(function () {
        var count = $('#CartForm_count');
        var val = $(this).val();
        $('p.price strong').html($('#price_' + val).val());
        count.attr('max', $('#stock_' + val).val());
        if (count.val() > count.attr('max')) count.val(count.attr('max'));
    });

    $('.photos a').click(function (e) {
        e.preventDefault();
        $('div#bigImage img').attr('src', $(this).attr('href'));
    });

    $('#cart-form').CartForm();
</script>