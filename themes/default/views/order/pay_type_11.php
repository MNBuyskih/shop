<?php
/**
 * pay_type_11.php
 * Date: 19.08.13
 * Time: 15:25
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $order      Order
 * @var $payType    Payment
 */

/** @var Payment $method */
$params = unserialize($payType->params);
$inv_desc = 'Оплата заказа №' . $order->id;
$crc = md5("{$params['robokassa_login']}:{$order->totalPrice}:{$order->id}:{$params['robokassa_password1']}:Shp_item={$method->payment_method_id}");

?>
<hr>
<h3><?php echo  $payType->name ?></h3>
<div class="row-fluid">
    <div class="span2"><img src="<?php echo  App::app()->getTheme()->getBaseUrl() ?>/css/images/robocassa.png" alt="<?php echo  $payType->name ?>"/></div>
    <div class="span10">
        <?php echo  $payType->description ?>
        <a target="_blank" class="btn btn-primary" href="https://merchant.roboxchange.com/Index.aspx?MrchLogin=<?php echo  $params['robokassa_login'] ?>&OutSum=<?php echo  $order->totalPrice ?>&InvId=<?php echo  $order->id ?>&Desc=<?php echo  $inv_desc ?>&SignatureValue=<?php echo  $crc ?>&Shp_item=<?php echo  $method->payment_method_id ?>&IncCurrLabel=PCR&Culture=<?php echo  $params['robokassa_language'] ?>">
            Перейти к оплате
            <i class="icon-arrow-right icon-white"></i>
        </a>
    </div>
</div>