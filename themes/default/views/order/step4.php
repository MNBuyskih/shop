<?php
/**
 * step4.php
 * Date: 09.08.13
 * Time: 15:08
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $model      Order
 * @var $form       ActiveFormHorizontal
 */

$controller->mainTitle = "Оформить заказ";
$controller->mainDescription = "Шаг 4 из 5";
$controller->breadcrumbs = array(
    "Оформить заказ. Шаг 4"
);

$form = $controller->widget('ActiveFormHorizontal', array('model' => $model));

echo $form->errors();
echo $form->radioRow('paytype_id', Payment::data($model->delivery_id));

echo Html::openTag('div', array('class' => 'form-actions'));
echo Html::tag('button', array(
                              'class' => 'btn btn-primary',
                              'id'    => 'back',
                         ), '<i class="icon-arrow-left icon-white"></i> Назад');
echo " ";
echo Html::tag('button', array(
                              'type'  => 'submit',
                              'class' => 'btn btn-success',
                         ), 'Далее <i class="icon-arrow-right icon-white"></i>');
echo Html::closeTag('div');

$form->endForm();
?>
<script type="text/javascript">
    $('#back').click(function (e) {
        e.preventDefault();
        history.go(-1);
    });
</script>