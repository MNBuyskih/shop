<?php
/**
 * _bank.php
 * Date: 20.08.13
 * Time: 9:53
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $model      Order
 * @var $controller OrderController
 * @var $type       string
 */

$ceil = ceil($model->totalPrice);
$decimal = fmod($model->totalPrice, 1);
?>
<tr>
    <td style="width: 300px" class="base">
        <p class="text-right"><?php echo  $type ?></p>

        <p class="cashier">Кассир</p>
    </td>
    <td class="base">
        <p class="bottom">OOO &laquo;ММАМАРКЕТ.ру&raquo;</p>

        <p class="note">(наименование получателя платежа)</p>
        <table>
            <tr>
                <td>
                    <p class="bottom">7729663048</p>

                    <p class="note">(ИНН получателя платежа)</p>
                </td>
                <td>
                    <p class="bottom">40702810300000018493</p>

                    <p class="note">(номер счета получателя платежа)</p>
                </td>
            </tr>
        </table>
        <p class="bottom">ООО КБ «Преодоление»</p>

        <p class="note">(наименование банка получателя платежа)</p>
        <table>
            <tr>
                <td>
                    <p>БИК <span class="bottom">044583871</span></p>
                </td>
                <td>
                    <p class="bottom">№ 30101810100000000871</p>

                    <p class="note">(номер кор./сч. банка получателя платежа)</p>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>
                    <p class="bottom">Оплата заказа №<?php echo  $model->id ?></p>

                    <p class="note">(наименование платежа)</p>
                </td>
                <td>
                    <p class="bottom">&nbsp;</p>

                    <p class="note">(номер лицевого счета (код) плательщика)</p>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="width: 150px">
                    <p>Ф.И.О. плательщика</p>
                </td>
                <td>
                    <p class="bottom">&nbsp;</p>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    <p>Адрес плательщика</p>
                </td>
                <td>
                    <p class="bottom">&nbsp;</p>
                </td>
            </tr>
        </table>

        <p class="text-right">Сумма платежа: <?php echo  $ceil ?> руб. <?php echo  $decimal ?> коп.</p>

        <p class="text-right"><strong>Итого: <?php echo  $ceil ?> руб. <?php echo  $decimal ?> коп.</strong></p>

        <p class="bottom" style="width: 50%">&nbsp;</p>

        <p class="note" style="width: 50%">(подпись плательщика)</p>
    </td>
</tr>