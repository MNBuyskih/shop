<?php
/**
 * order_email.php
 * Date: 20.08.13
 * Time: 9:11
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $model      Order
 */

$url = App::app()->createAbsoluteUrl(array(
                                          '/order/view',
                                          'code' => $model->code
                                     ));
?>
<h1>Благодарим за покупку</h1>
<table>
    <tr>
        <td>Заказ №</td>
        <td><?php echo  $model->id ?></td>
    </tr>
</table>
Вы всегда сможете проверить состояние заказа по ссылке <a href="<?php echo  $url ?>"><?php echo  $url ?></a>