<?php
/**
 * pay_type_12.php
 * Date: 19.08.13
 * Time: 15:25
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $order      Order
 * @var $payType    Payment
 */

$url = "http://w.qiwi.ru/setInetBill.do?";

$parts = array();
$parts['summ'] = $order->totalPrice;
$parts['com'] = "Оплата заказа №{$order->id}";
$parts['from'] = 9660;
$parts['to'] = $order->phone;
$parts['lifetime'] = 10;
$parts['txn_id'] = $order->id;
$parts['txn_id'] = $order->id;

$url .= http_build_query($parts);

?>
<hr>
<h3><?php echo  $payType->name ?></h3>
<div class="row-fluid">
    <div class="span2"><img src="<?php echo  App::app()->getTheme()->getBaseUrl() ?>/css/images/qiwi.png" alt="<?php echo  $payType->name ?>"/></div>
    <div class="span10">
        <?php echo  $payType->description ?>
        <a target="_blank" class="btn btn-primary" href="<?php echo  $url ?>">Перейти к оплате <i class="icon-arrow-right icon-white"></i></a>
    </div>
</div>