<?php
/**
 * bank.php
 * Date: 19.08.13
 * Time: 16:27
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $model      Order
 * @var $controller OrderController
 */

?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title>Квитанция Заказа №<?php echo  $model->id ?></title>
    <style type="text/css">
        body {
            font-family: Arial, sans-serif;
            font-size: 14px;
        }
        table.base {
            width: 1000px;
            margin: 0 auto;
            border-collapse: collapse
        }
        table table {
            width: 100%;
        }
        td {
            vertical-align: top;
        }
        td.base {
            border: 1px dashed #000;
            padding: 10px;
        }
        td td {
            padding-right: 30px;
        }
        td td:last-child {
            padding-right: 0;
        }
        p {
            margin: 5px 0 0 0;
        }
        .text-right {
            text-align: right;
        }
        .bottom {
            border-bottom: 1px solid #000;
        }
        .note {
            margin: 0;
            font-size: 10px;
            text-align: center;
        }
        .cashier {
            margin-top: 250px;
        }
    </style>
</head>
<body>
<table class="base">
    <?php
    $controller->renderPartial('_bank', array(
                                             'model' => $model,
                                             'type'  => 'Извещение'
                                        ));
    $controller->renderPartial('_bank', array(
                                             'model' => $model,
                                             'type'  => 'Квитанция'
                                        ));
    ?>
</table>
<script type="text/javascript">
    window.print();
</script>
</body>
</html>