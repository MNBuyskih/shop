<?php
/**
 * _variant.php
 * Date: 19.08.13
 * Time: 14:13
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $variant Variants
 * @var $count   integer
 */

$product = $variant->getProduct();
?>
<div>
    <a href="<?php echo  $product->url() ?>"><?php echo  $product->model ?> (<?php echo  $variant->name ?>)</a>

    <div class="pull-right">
        <?php echo  $count ?> × <?php echo  $variant->price ?>
    </div>
</div>