<?php
/**
 * step2.php
 * Date: 09.08.13
 * Time: 12:48
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller  OrderController
 * @var $model       Order
 * @var $form        ActiveFormHorizontal
 */

$controller->mainTitle = "Оформить заказ";
$controller->mainDescription = "Шаг 2 из 5";
$controller->breadcrumbs = array(
    "Оформить заказ. Шаг 2"
);

$form = $controller->widget('ActiveFormHorizontal', array('model' => $model));

echo $form->errors();

echo $form->textFieldRow('country');
echo $form->textFieldRow('index');
echo $form->textFieldRow('region');
echo $form->textFieldRow('city');
echo $form->textAreaRow('address');
echo $form->textFieldRow('fooname');
echo $form->textFieldRow('name');
echo $form->textFieldRow('lastname');
echo $form->textFieldRow('phone');
echo $form->textFieldRow('email');
echo $form->textAreaRow('comment');

echo Html::openTag('div', array('class' => 'form-actions'));
echo Html::tag('button', array(
                              'class' => 'btn btn-primary',
                              'id'    => 'back',
                         ), '<i class="icon-arrow-left icon-white"></i> Назад');
echo " ";
echo Html::tag('button', array(
                              'type'  => 'submit',
                              'class' => 'btn btn-success',
                         ), 'Далее <i class="icon-arrow-right icon-white"></i>');
echo Html::closeTag('div');

$form->endForm();

?>
<script type="text/javascript">
    $('#back').click(function (e) {
        e.preventDefault();
        history.go(-1);
    });
</script>