<?php
/**
 * step5.php
 * Date: 09.08.13
 * Time: 15:27
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $model      Order
 */

$controller->mainTitle = "Оформить заказ";
$controller->mainDescription = "Шаг 5 из 5";
$controller->breadcrumbs = array(
    "Оформить заказ. Шаг 5"
);
?>
<dl class="dl-horizontal">
    <dt>Фамилия Имя Отчество</dt>
    <dd><?php echo  $model->fooname ?> <?php echo  $model->name ?> <?php echo  $model->lastname ?></dd>
    <dt>Адрес доставки</dt>
    <dd><?php echo  $model->getAddress() ?></dd>
    <dt>Телефон</dt>
    <dd><?php echo  $model->phone ?></dd>
    <dt>Способ оплаты</dt>
    <dd><?php echo  $model->getPaytype()->name ?></dd>
    <dt>Сумма с доставкой<br>(<?php echo  $model->getDelivery()->name ?>)</dt>
    <dd><?php echo  CartCookie::create()->getTotalSum() + $model->getDelivery()->price ?> руб.</dd>
</dl>

<a class="btn btn-primary" href="#" id="back"><i class="icon-arrow-left icon-white"></i> Назад</a>
<a class="btn btn-success" href="<?php echo  App::app()->createUrl('/order/finish') ?>"><i class="icon-ok icon-white"></i> Завершить</a>

<script type="text/javascript">
    $('#back').click(function (e) {
        e.preventDefault();
        history.go(-1);
    });
</script>