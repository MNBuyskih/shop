<?php
/**
 * robokassa.php
 * Date: 19.08.13
 * Time: 15:18
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $order      Order
 */
?>

<a target="_blank" class="btn btn-primary" href="<?php echo  App::app()->createUrl('/order/robokassa') ?>">Перейти к оплате <i class="icon-arrow-right icon-white"></i></a>