<?php
/**
 * finish.php
 * Date: 15.08.13
 * Time: 14:47
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $model      Order
 */

$controller->mainTitle = "Благодарим за покупку!";
$controller->breadcrumbs = array($controller->mainTitle);
?>
<div class="alert alert-block alert-success">
    <p>Ваш заказ успешно отправлен.</p>

    <p>
        Вы всегда можете проверить состояние заказа по ссылке:<br>
        <a href="<?php echo
        App::app()->createAbsoluteUrl(array(
                                           '/order/view',
                                           'code' => $model->code
                                      )) ?>"><?php echo
            App::app()->createAbsoluteUrl(array(
                                               '/order/view',
                                               'code' => $model->code
                                          )) ?></a>
    </p>
</div>