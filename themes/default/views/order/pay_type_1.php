<?php
/**
 * pay_type_1.php
 * Date: 19.08.13
 * Time: 15:25
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $order      Order
 * @var $payType    Payment
 */
?>
<hr>
<h3><?php echo  $payType->name ?></h3>
<div class="row-fluid">
    <div class="span2"><img src="<?php echo  App::app()->getTheme()->getBaseUrl() ?>/css/images/courier.png" alt="<?php echo  $payType->name ?>"/></div>
    <div class="span10"><?php echo  $payType->description ?></div>
</div>