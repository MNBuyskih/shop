<?php
/**
 * step1.php
 * Date: 09.08.13
 * Time: 9:46
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $data       array
 * @var $form       ActiveFormHorizontal
 */

$controller->mainTitle = "Оформить заказ";
$controller->mainDescription = "Шаг 1 из 5";
$controller->breadcrumbs = array(
    "Оформить заказ. Шаг 1"
);
$controller->widget('GridView', array(
                                     'emptyText' => 'Ваша корзина пуста',
                                     'data'      => $data,
                                     'columns'   => array(
                                         array(
                                             'header' => 'Товар',
                                             'value'  => '$data["product"]->model',
                                         ),
                                         array(
                                             'header' => 'Варианат',
                                             'value'  => '$data["variant"]->name',
                                         ),
                                         array(
                                             'header' => 'Цена',
                                             'value'  => '$data["variant"]->price',
                                         ),
                                         array(
                                             'header' => 'Количество',
                                             'value'  => '$data["count"]',
                                         ),
                                         array(
                                             'header' => 'Сумма',
                                             'value'  => '$data["variant"]->price * $data["count"]',
                                         ),
                                     )
                                ));
?>
<a class="btn btn-primary" href="<?php echo  App::app()->createUrl('/cart') ?>"><i class="icon-share-alt icon-white icon-mirror"></i> Редактировать товары в корзине</a>
<a class="btn btn-success pull-right" href="<?php echo
App::app()->createUrl(array(
                           '/order/process',
                           'step' => 2
                      )) ?>">Далее <i class="icon-arrow-right icon-white"></i></a>