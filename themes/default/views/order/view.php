<?php
/**
 * view.php
 * Date: 19.08.13
 * Time: 13:09
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller OrderController
 * @var $order      Order
 * @var $delivery   Delivery
 * @var $variants   Variants[]
 * @var $counts     array
 */

$controller->pageTitle = $controller->mainTitle = "Заказ №" . $order->id;
$controller->breadcrumbs = array($controller->mainTitle);
$totalPrice = $delivery->price;
?>

<div class="row-fluid">
    <div class="span6">
        <div class="well">
            <div class="row-fluid">
                <?php
                foreach ($variants as $variantId => $variant) {
                    $totalPrice += $counts[$variant->variant_id] * $variant->price;
                    $controller->renderPartial('_variant', array(
                        'variant' => $variant,
                        'count'   => $counts[$variant->variant_id]
                    ));
                }
                ?>
                <hr class="dotted">
                <div>
                    <?php echo  $delivery->name ?>
                    <div class="pull-right"><?php echo  $delivery->price ?></div>
                    руб.
                </div>
                <hr class="dotted">
                <div class="text-right">
                    <strong>К оплате: <?php echo  $totalPrice ?> руб.</strong>
                </div>
            </div>
        </div>

        <div class="well">
            <div class="clearfix">
                <div class="pull-left">
                    Стутус оплаты:
                </div>
                <div class="pull-right text-right">
                    <?php echo  ($order->payment_status ? "Оплачен" : "Не оплачен") ?>
                </div>
            </div>
            <?php if (!$order->payment_status) { ?>
                <div class="clearfix">
                    <div class="pull-right text-right">
                        <a class="btn btn-success"
                           href="//intellectmoney.a4h.ru/?id=<?php echo  $order->id ?>&email=<?php echo  $order->email ?>"
                           target="blank"><i class="icon-shopping-cart icon-white"></i> Перейти к оплате</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="span6">
        <div class="well">
            <div class="clearfix">
                <div class="pull-left">
                    Имя:
                </div>
                <div class="pull-right text-right">
                    <?php echo  $order->fooname ?> <?php echo  $order->name ?> <?php echo  $order->lastname ?>
                </div>
            </div>
            <hr class="dotted">
            <div class="clearfix">
                <div class="pull-left">
                    Телефон:
                </div>
                <div class="pull-right text-right">
                    <?php echo  $order->phone ?>
                </div>
            </div>
            <hr class="dotted">
            <div class="clearfix">
                <div class="pull-left">
                    Адрес доставки:
                </div>
                <div class="pull-right text-right">
                    <?php echo  $order->getAddress() ?>
                </div>
            </div>
            <hr class="dotted">
            <div class="clearfix">
                <div class="pull-left">
                    Комментарий:
                </div>
                <div class="pull-right text-right">
                    <?php echo  $order->comment ?>
                </div>
            </div>
        </div>
    </div>
</div>