<?php
/**
 * filter.php
 * Date: 12.08.13
 * Time: 12:48
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller CatalogController
 * @var $filter     FilterForm
 * @var $category   Catalog
 * @var $form       ActiveForm
 */
?>

<div class="well filter">
    <?php
    $form = $controller->widget('ActiveForm', array('model' => $filter));
    echo $form->dropDown('brand', Brands::data($category, array('*' => 'Выберите бренд')));
    echo " ";
    echo $form->dropDown('size', Variants::data($category, array('*' => 'Выберите размер')));
    $form->endForm();
    ?>
</div>

<script type="text/javascript">
    $('.filter select').change(function () {
        var baseUrl = "<?php echo $category->url()?>";
        baseUrl += "/" + $('#FilterForm_brand').val() + "/" + $("#FilterForm_size").val();
        location.href = baseUrl;
    });
</script>