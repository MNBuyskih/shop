<?php
/**
 * index.php
 * Date: 25.07.13
 * Time: 15:05
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller CatalogController
 * @var $category   Catalog
 * @var $filter     FilterForm
 * @var $products   Products
 * @var $pagination Pagination
 */

$controller->mainTitle = $category->name;

$controller->renderPartial('filter', array(
    'filter'   => $filter,
    'category' => $category
));

$controller->widget('ListView', array(
    'data'     => $products,
    'itemView' => '_index'
));
$controller->widget('Pager', array('pagination' => $pagination));