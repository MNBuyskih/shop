<?php
/**
 * view.php
 * Date: 24.07.13
 * Time: 10:36
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller DocController
 * @var $model      Doc
 */

$controller->mainTitle = $model->title;
$controller->breadcrumbs = array(
    $model->title
);
echo $model->text;