<?php
/**
 * index.php
 * Date: 24.07.13
 * Time: 9:02
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller ErrorController
 * @var $exception  HttpException
 */

$controller->pageTitle = "Error - " . $exception->statusCode;
$controller->breadcrumbs = array(
    'Error - ' . $exception->statusCode,
);

?>
<div class="hero-unit">
    <h1><?php echo  $exception->statusCode ?></h1>

    <div class="caption"><?php echo  $exception->getMessage() ?></div>
</div>