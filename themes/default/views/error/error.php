<?php
/**
 * index.php
 * Date: 24.07.13
 * Time: 9:02
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller ErrorController
 * @var $error      array
 */

$controller->pageTitle = "Error";
$controller->breadcrumbs = array(
    'Error'
);

switch ($error['errorNumber']) {
    case E_ERROR:
        $h1 = "E_ERROR";
        break;
    case E_WARNING :
        $h1 = "E_WARNING";
        break;
    case E_PARSE  :
        $h1 = "E_PARSE";
        break;
    case E_NOTICE   :
        $h1 = "E_NOTICE";
        break;
    case E_CORE_ERROR:
        $h1 = "E_CORE_ERROR";
        break;
    case E_CORE_WARNING:
        $h1 = "E_CORE_WARNING";
        break;
    case E_COMPILE_ERROR:
        $h1 = "E_COMPILE_ERROR";
        break;
    case E_COMPILE_WARNING:
        $h1 = "E_COMPILE_WARNING";
        break;
    case E_USER_ERROR:
        $h1 = "E_USER_ERROR";
        break;
    case E_USER_WARNING:
        $h1 = "E_USER_WARNING";
        break;
    case E_USER_NOTICE:
        $h1 = "E_USER_NOTICE";
        break;
    case E_STRICT:
        $h1 = "E_STRICT";
        break;
    case E_RECOVERABLE_ERROR:
        $h1 = "E_RECOVERABLE_ERROR";
        break;
    case E_DEPRECATED:
        $h1 = "E_DEPRECATED";
        break;
    case E_USER_DEPRECATED:
        $h1 = "E_USER_DEPRECATED";
        break;
    case E_ALL:
        $h1 = "E_ALL";
        break;

    default;
        $h1 = "Unknown error";
        break;
}

?>
    <h1><?php echo  $h1 ?></h1>
    <h4><?php echo  $error['errorString'] ?></h4>
    <h5>В файле: <code><?php echo  $error['errorFile'] ?></code>. В строке: <code><?php echo  $error['errorLine'] ?></code></h5>
<?php

if (APP_DEBUG) {
    echo "<pre>";
    debug_print_backtrace();
    echo "</pre>";
}