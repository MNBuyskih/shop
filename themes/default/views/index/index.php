<?php
/**
 * index.php
 * Date: 22.07.13
 * Time: 10:55
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller IndexController
 */

$controller->mainTitle = "Новинки";
$controller->pageTitle = "Новинки";

$controller->widget('CatalogNew');