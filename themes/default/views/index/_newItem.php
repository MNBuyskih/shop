<?php
/**
 * _index.php
 * Date: 25.07.13
 * Time: 15:57
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller CatalogController
 * @var $model      Products
 * @var $form       ActiveForm
 */

$variants = $model->variants();
$cart = new CartForm($model);

?>
<li class="span3">
    <div class="thumbnail product">
        <a href="<?php echo  $model->url() ?>" class="thumb" style="background-image: url(<?php echo  $model->small_image ?>)"></a>

        <?php $form = $controller->widget('ActiveForm', array(
                                                          'model'       => $cart,
                                                          'htmlOptions' => array(
                                                              'action' => App::app()->createUrl('/cart/add'),
                                                              'method' => 'post',
                                                              'class'  => 'cart-form'
                                                          )
                                                     )) ?>

        <p class="title"><?php echo  htmlspecialchars($model->model) ?></p>

        <p class="price">Цена: <strong><?php echo  $variants[0]->price ?></strong></p>

        <div class="row-fluid">
            <?php echo  $form->dropDown('variant', Html::listData($variants, 'variant_id', 'name'), array('class' => 'span6')) ?>
        </div>
        <?php foreach ($variants as $variant) { ?>
            <input type="hidden" id="price_<?php echo  $variant->variant_id ?>" value="<?php echo  $variant->price ?>"/>
        <?php } ?>
        <?php echo
        $form->textField('count', array(
                                       'type'  => 'hidden',
                                       'value' => 1
                                  )) ?>
        <?php echo  $form->submit('В корзину', array('class' => 'btn btn-primary')) ?>

        <?php $form->endForm() ?>
    </div>
</li>