<?php
/**
 * main.php
 * Date: 29.07.13
 * Time: 14:06
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 *
 * @var $controller Controller
 * @var $content    string
 */

$defaultTheme = new Theme('default');
?>
<!doctype html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">

    <title><?php echo  $controller->pageTitle ?></title>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo  $defaultTheme->getBaseUrl() ?>/css/style.css"/>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="navbar navbar-fixed-top navbar-inverse">
    <div class="navbar-inner">
        <div class="container">
            <a class="brand" href="<?php echo  App::app()->createUrl('/install') ?>">Shop install</a>
        </div>
    </div>
</div>
<div class="container" id="page">
    <?php
    if ($controller->breadcrumbs) {
        $controller->widget('Breadcrumbs', array(
                                                'mainPageUrl' => 'admin',
                                                'links'       => $controller->breadcrumbs
                                           ));
    }
    if ($controller->mainTitle) {
        echo "<h1>{$controller->mainTitle}</h1>";
    }
    if ($controller->mainDescription) {
        echo "<h4>{$controller->mainDescription}</h4>";
    }
    if ($controller->breadcrumbs || $controller->mainTitle || $controller->mainDescription) {
        echo "<hr>";
    }
    ?>
    <?php echo  $content ?>
</div>
</body>
</html>