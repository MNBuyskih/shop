<?php
/**
 * index.php
 * Date: 29.07.13
 * Time: 14:19
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 *
 * @var $controller IndexController
 */

$controller->pageTitle       = "Установка";
$controller->mainDescription = "Шаг 1";

$configFile = App::app()->basePath . DIRECTORY_SEPARATOR . "protected" . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "main.php";
$updateDir  = App::app()->basePath . DIRECTORY_SEPARATOR . "admin" . DIRECTORY_SEPARATOR . "update-temp";

$isConfig        = is_writable($configFile);
$isUpdate        = is_writable($updateDir);
$isPhp           = version_compare(phpversion(), 5.3) >= 0;
$isPdo           = extension_loaded('pdo_mysql');
$isMysql         = extension_loaded('mysql');
$isZip           = extension_loaded('zip');
$isXml           = extension_loaded('xml');
$isAllowUrlFopen = ini_get('allow_url_fopen');

$state = $isConfig && $isUpdate && $isPhp && ($isPdo || $isMysql) && $isZip && $isXml && $isAllowUrlFopen;

?>

<table class="table table-striped">
    <tr>
        <th>Параметр</th>
        <th>Требуемое значение</th>
        <th>Значение сервера</th>
    </tr>

    <tr>
        <td>
            Права на запись файла
            <code><?php echo $configFile ?></code>
        </td>
        <td><i class="icon-ok"></i></td>
        <td>
            <span class="label label-<?php echo($isConfig ? "success" : "important") ?>"><i
                    class="icon-<?php echo($isConfig ? "ok" : "remove") ?> icon-white"></i></span>
        </td>
    </tr>

    <tr>
        <td>
            Права на запись в директорию
            <code><?php echo $updateDir ?></code>
        </td>
        <td><i class="icon-ok"></i></td>
        <td>
            <span class="label label-<?php echo($isUpdate ? "success" : "important") ?>"><i
                    class="icon-<?php echo($isUpdate ? "ok" : "remove") ?> icon-white"></i></span>
        </td>
    </tr>

    <tr>
        <td>
            Версия PHP
        </td>
        <td><strong>>= 5.3</strong></td>
        <td>
            <span class="label label-<?php echo($isPhp ? "success" : "important") ?>"><i
                    class="icon-<?php echo($isPhp ? "ok"
                        : "remove") ?> icon-white"></i> <?php echo phpversion() ?></span>
        </td>
    </tr>

    <tr>
        <td>Расширения для работы с MySQL</td>
        <td>PDO или MySQL</td>
        <td>
            <span class="label label-<?php echo($isPdo ? "success" : "important") ?>"><i
                    class="icon-<?php echo($isPdo ? "ok" : "remove") ?> icon-white"></i> PDO</span>
            <span class="label label-<?php echo($isMysql ? "success" : "important") ?>"><i
                    class="icon-<?php echo($isMysql ? "ok" : "remove") ?> icon-white"></i> MySQL</span>
        </td>
    </tr>

    <tr>
        <td>Расширение для работы с Zip</td>
        <td><i class="icon-ok"></i></td>
        <td>
            <span class="label label-<?php echo($isZip ? "success" : "important") ?>"><i
                    class="icon-<?php echo($isZip ? "ok" : "remove") ?> icon-white"></i></span>
        </td>
    </tr>

    <tr>
        <td>Расширение для работы с XML</td>
        <td><i class="icon-ok"></i></td>
        <td>
            <span class="label label-<?php echo($isXml ? "success" : "important") ?>"><i
                    class="icon-<?php echo($isXml ? "ok" : "remove") ?> icon-white"></i></span>
        </td>
    </tr>

    <tr>
        <td>Включеный модуль Apache mod_rewrite</td>
        <td><i class="icon-ok"></i></td>
        <td>
            <span id="rewrite-icon" class="label label-<?php echo($isXml ? "success" : "important") ?>">
                <i class="icon-<?php echo($isXml ? "ok" : "remove") ?> icon-white"></i></span>
        </td>
    </tr>

    <tr>
        <td>Включеная директива <code>allow_url_fopen</code> в <code>php.ini</code></td>
        <td><i class="icon-ok"></i></td>
        <td>
            <span id="rewrite-icon" class="label label-<?php echo($isAllowUrlFopen ? "success" : "important") ?>">
                <i class="icon-<?php echo($isAllowUrlFopen ? "ok" : "remove") ?> icon-white"></i></span>
        </td>
    </tr>
</table>

<form method="post" action="<?php echo App::app()->createUrl('/install/index/db') ?>" class="text-right">
    <div class="form-actions">
        <input class="btn btn-success" id="submit" type="submit" value="Перейти к следующему шагу"/>
    </div>
</form>
<script type="text/javascript">
    var stat = <?php echo ($state ? "true" : "false") ?>,
        setStatus = function (s) {
            var c = ['label label-important', 'label label-success'],
                ci = ['icon-remove icon-white', 'icon-ok icon-white'];

            $('#submit').attr('disabled', !(s && stat));
            $('#rewrite-icon').attr('class', c[+s]).find('i').attr('class', ci[+s]);
        };

    if (stat) {
        setStatus(true);
    }

    $.ajax({
        url: "<?php echo App::app()->createUrl('/install/index/rewrite') ?>",
        success: function () {
            setStatus(true);
        },
        error: function () {
            setStatus(false);
        }
    });
</script>