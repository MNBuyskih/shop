<?php
/**
 * password.php
 * Date: 30.07.13
 * Time: 8:13
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller IndexController
 * @var $model      PasswordForm
 * @var $form       ActiveFormHorizontal
 */

$controller->mainDescription = "Шаг 4. Пароль администратора";

$form = $controller->widget('ActiveFormHorizontal', array('model' => $model));

echo $form->errors();

echo $form->textFieldRow('password', array('type' => 'password'));
echo $form->textFieldRow('confirmPassword', array('type' => 'password'));

echo $form->submitRow('Перейти к следующему шагу', array('class' => 'btn-success'));

$form->endForm();