<?php
/**
 * setupError.php
 * Date: 30.07.13
 * Time: 8:53
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 *
 * @var $controller IndexController
 * @var $model      Setup
 * @var $form       ActiveFormHorizontal
 */

$controller->mainDescription = "Ошибка.";

$form = $controller->widget('ActiveFormHorizontal', array('model' => $model));

echo $form->errors();

$form->endForm();