<?php
/**
 * ref.php
 * Date: 29.07.13
 * Time: 16:12
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 * @var $controller IndexController
 * @var $model      RefForm
 * @var $form       ActiveFormHorizontal
 */

$controller->mainDescription = "Шаг 3. Параметры соединения с сервером магазина.";

$form = $controller->widget('ActiveFormHorizontal', array(
                                                         'model'       => $model,
                                                         'htmlOptions' => array('method' => 'post')
                                                    ));

echo $form->errors();

echo $form->textFieldRow('ref');
echo $form->textFieldRow('key');

echo $form->submitRow('Проверить и перейкти к следующему шагу', array('class' => 'btn-success'));

$form->endForm();