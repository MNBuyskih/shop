<?php
/**
 * @var $controller IndexController
 * @var $model      Setup
 * @var $form       ActiveFormHorizontal
 */

$controller->mainDescription = "Шаг 4. Установка завершена.";

$form = $controller->widget('ActiveFormHorizontal', array('model' => $model));

echo $form->errors();

$form->endForm();

?>
<div class="alert alert-success">
    <strong>Поздравляем!</strong> Установка успешно завершена.
</div>
<div class="alert alert-error">
    Не забудьте удалить папку install из корневой папки вашего сайта.
</div>

<h3>Параметры входа в админ панель:</h3>
<dl class="dl-horizontal">
    <dt>Логин</dt>
    <dd><code>admin</code></dd>
    <dt>Пароль</dt>
    <dd><code><?php echo  $model->password ?></code></dd>
</dl>

<a class="btn btn-primary" href="<?php echo  App::app()->createUrl('/') ?>">Перейти на главную страницу сайта</a>
<a class="btn btn-success" href="<?php echo  App::app()->createUrl('/admin') ?>">Перейти в админ панель</a>