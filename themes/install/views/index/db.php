<?php
/**
 * db.php
 * Date: 29.07.13
 * Time: 15:20
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package mma
 *
 * @var $controller IndexController
 * @var $form       ActiveFormHorizontal
 * @var $model      DbForm
 */

$controller->mainDescription = "Шаг 2. Параметры соединения с базой данных";

$form = $controller->widget('ActiveFormHorizontal', array(
                                                         'model'       => $model,
                                                         'htmlOptions' => array('method' => 'post')
                                                    ));

echo $form->errors();

echo $form->textFieldRow('host');
echo $form->textFieldRow('dbName');
echo $form->textFieldRow('username');
echo $form->textFieldRow('password', array('type' => 'password'));
echo $form->checkBoxRow('truncate');

echo $form->submitRow('Проверить и перейти к следующему шагу', array('class' => 'btn-success'));

$form->endForm();