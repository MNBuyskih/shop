<?php
/**
 * LinkColumn.php
 * Date: 23.07.13
 * Time: 12:26
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class LinkColumn extends ColumnAbstract {
    public $url;

    public $urlExpression;

    public $label;

    public $labelExpression;

    public function renderHeader() {
        return $this->header;
    }

    public function renderContent($data, $num) {
        $label = $this->label ? $this->label : App::app()->evaluate($this->labelExpression, array(
                                                                                                 'data' => $data,
                                                                                                 'num'  => $num
                                                                                            ));
        $url   = $this->url ? $this->url : App::app()->evaluate($this->urlExpression, array(
                                                                                           'data' => $data,
                                                                                           'num'  => $num
                                                                                      ));

        return Html::tag('a', array('href' => $url), $label);
    }

}