<?php
/**
 * ColumnAbstract.php
 * Date: 23.07.13
 * Time: 11:03
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

abstract class ColumnAbstract {

    public $header;

    public $name;

    public function __construct($params) {
        foreach ($params as $param => $value) {
            if (!property_exists($this, $param)) {
                throw new Exception("Property {$param} not found in class " . get_class($this));
            }
            $this->$param = $value;
        }

        $this->init();
    }

    public function init() {

    }

    abstract public function renderHeader();

    abstract public function renderContent($data, $num);
}