<?php
/**
 * DataColumn.php
 * Date: 23.07.13
 * Time: 11:04
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class DataColumn extends ColumnAbstract {

    public $name;

    public $value;

    public function init() {
        if (empty($this->name) && empty($this->value)) {
            throw new HttpException(500, 'Необходимо указать как минимум один из параметров: name или value');
        }
    }

    public function renderHeader() {
        return $this->header;
    }

    public function renderContent($data, $num) {
        if ($this->value) {
            return App::app()->evaluate($this->value, array(
                                                           'data' => $data,
                                                           'num'  => $num
                                                      ));
        }

        return $data->{$this->name};
    }

}