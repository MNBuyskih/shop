<?php
/**
 * ButtonsColumn.php
 * Date: 23.07.13
 * Time: 11:11
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class ButtonsColumn extends ColumnAbstract {
    public $template;

    public $buttons;

    public function renderHeader() {
        return $this->header;
    }

    public function renderContent($data, $num) {
        $return = $this->template;
        foreach ($this->buttons as $button => $buttonData) {
            $return = str_replace("{" . $button . "}", $this->renderButton($data, $num, $button, $buttonData), $return);
        }

        return $return;
    }

    protected function renderButton($data, $num, $button, $buttonData) {
        $htmlOptions = isset($buttonData['htmlOptions']) ? $buttonData['htmlOptions'] : array();
        Html::addClass($button, $htmlOptions);

        $label = "";
        if (!empty($buttonData['label'])) {
            $htmlOptions['rel']        = "tooltip";
            $htmlOptions['data-title'] = $buttonData['label'];
        } else {
            $label = '&nbsp;';
        }

        if (!empty($buttonData['icon'])) {
            $icon = '<i class="icon-' . $buttonData['icon'] . '"></i>';
        } else {
            $icon = $label;
        }

        if (isset($buttonData['urlExpression'])) {
            $htmlOptions['href'] = App::app()->evaluate($buttonData['urlExpression'], array(
                                                                                           'data' => $data,
                                                                                           'num'  => $num
                                                                                      ));
        } elseif (isset($buttonData['url'])) {
            $htmlOptions['href'] = $buttonData['url'];
        } else {
            $htmlOptions['href'] = '#';
        }

        return Html::tag('a', $htmlOptions, $icon);
    }

}