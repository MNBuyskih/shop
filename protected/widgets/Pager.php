<?php
/**
 * Pager.php
 * Date: 26.07.13
 * Time: 9:32
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Pager extends Widget {
    /**
     * @var Pagination
     */
    public $pagination;

    public $firstPage = 1;

    public $previousPage = "<";

    public $lastPage = true;

    public $nextPage = ">";

    public $pagesLeft = 2;

    public $pagesRight = 2;

    private $totalPages = 0;

    private $currentPage = 1;

    public function run() {
        if (!$this->pagination) {
            throw new HttpException(500, 'Не обходимо указать параметр pagination');
        }
        if (!$this->pagination instanceof Pagination) {
            throw new HttpException(500, 'Параметр pagination должен быть экземпляром класса Pagination');
        }

        $this->totalPages  = $this->pagination->getTotalPages();
        $this->currentPage = $this->pagination->getCurrentPage();

        if ($this->totalPages < 10) {
            $this->pagesLeft  = 10;
            $this->pagesRight = 10;
        }

        if ($this->totalPages > 1) {
            echo Html::openTag('div', array('class' => 'pagination'));
            echo Html::openTag('ul');

            echo $this->firstPage();
            echo $this->pagesLeft();
            echo $this->currentPage();
            echo $this->pagesRight();
            echo $this->lastPage();

            echo "</ul></div>";
        }
    }

    protected function firstPage() {
        if ($this->currentPage + 1 > $this->pagesLeft && !$this->firstPage) {
            return "";
        }

        return $this->renderItem(1);
    }

    protected function pagesLeft() {
        $return = "";

        $hiddenPagesCount = $this->currentPage - $this->pagesLeft - 2;

        if ($hiddenPagesCount > 1) {
            $return .= $this->renderSpan();
        } else {
            $this->pagesLeft++;
        }

        for ($i = $this->currentPage - $this->pagesLeft; $i < $this->currentPage; $i++) {
            if ($i > 1) {
                $return .= $this->renderItem($i);
            }
        }

        return $return;
    }

    protected function currentPage() {
        if ($this->currentPage == 1) {
            return '';
        }

        return $this->renderSpan($this->currentPage, true);
    }

    protected function pagesRight() {
        $return = "";

        $hiddenPagesCount = $this->totalPages - $this->currentPage - $this->pagesRight - 1;
        if ($hiddenPagesCount <= 1) {
            $this->pagesRight++;
        }

        for ($i = $this->currentPage + 1; $i <= $this->pagesRight + $this->currentPage; $i++) {
            if ($i < $this->totalPages) {
                $return .= $this->renderItem($i);
            }
        }

        if ($hiddenPagesCount > 1) {
            $return .= $this->renderSpan();
        }

        return $return;
    }

    protected function lastPage() {
        if ($this->totalPages <= $this->currentPage) {
            return "";
        }

        return $this->renderItem($this->totalPages);
    }

    protected function renderItem($page) {
        if ($this->currentPage == $page) {
            return $this->renderSpan($page, true);
        }

        return Html::tag('li', array(), Html::tag('a', array('href' => $this->href($page)), $page));
    }

    protected function renderSpan($content = '...', $active = false) {
        $htmlOptions = array();
        if ($active) {
            Html::addClass('active', $htmlOptions);
        } else {
            Html::addClass('disabled', $htmlOptions);
        }

        return Html::tag('li', $htmlOptions, Html::tag('span', array(), $content));
    }

    protected function href($page) {
        if ($page == 1) {
            return App::app()->createUrl(App::app()->getRequest()->getUri());
        }

        return App::app()->createUrl(array(
                                          App::app()->getRequest()->getUri(),
                                          $this->pagination->pageVar => $page
                                     ));
    }
}