<?php
/**
 * InstallDir.php
 * Date: 30.07.13
 * Time: 9:22
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class InstallDir extends Widget {
    public function run() {
        $dirName = App::app()->basePath . '/install';
        if (file_exists($dirName) && !is_file($dirName)) {
            ?>
            <div class="alert"><strong>Необходимо удалить директорию install из корневой папки сайта</strong></div>
        <?php
        }
    }

}