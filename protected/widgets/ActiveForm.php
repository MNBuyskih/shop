<?php
/**
 * ActiveForm.php
 * Date: 22.07.13
 * Time: 14:37
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class ActiveForm extends Widget {

    /**
     * @var Model
     */
    public $model;

    public $htmlOptions = array('method' => 'post');

    public function run() {
        echo Html::openTag('form', $this->htmlOptions);
    }

    public function endForm() {
        echo "</form>";
    }

    public function errors() {
        $return = '';
        $errors = $this->model->getErrors();

        if (count($errors)) {
            $return = '<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Исправте, пожалуйста, следующие ошибки:</strong><br><ul>';
            foreach ($errors as $error) {
                foreach ($error as $e) {
                    $return .= '<li>' . $e . '</li>';
                }
            }
            $return .= '</ul></div>';
        }

        return $return;
    }

    public function label($attribute, $htmlOptions = array()) {
        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }

        if (!isset($htmlOptions['for']) || $htmlOptions['for'] !== false) {
            $htmlOptions['for'] = Html::resolveId($this->model, $attribute);
        }
        if ($htmlOptions['for'] === false) {
            unset($htmlOptions['for']);
        }
        if (empty($htmlOptions['label'])) {
            $label = $this->getLabel($attribute);
        } else {
            $label = $htmlOptions['label'];
            unset($htmlOptions['label']);
        }

        return Html::tag('label', $htmlOptions, $label);
    }

    public function textFieldRow($attribute, $htmlOptions = array()) {
        $return = '';

        $htmlOptions['labelOptions'] = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();
        $return .= $this->label($attribute, $htmlOptions['labelOptions']);

        unset($htmlOptions['labelOptions']);

        $return .= $this->textField($attribute, $htmlOptions);

        return $return;
    }

    public function textField($attribute, $htmlOptions = array()) {
        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }

        if (empty($htmlOptions['name'])) {
            $htmlOptions['name'] = Html::resolveName($this->model, $attribute);
        }
        if (empty($htmlOptions['id'])) {
            $htmlOptions['id'] = Html::resolveId($this->model, $attribute);
        }
        if (empty($htmlOptions['value'])) {
            $htmlOptions['value'] = $this->model->getValue($attribute);
        }
        if (empty($htmlOptions['type'])) {
            $htmlOptions['type'] = 'text';
        }

        return Html::tag('input', $htmlOptions, false, false);
    }

    public function textAreaRow($attribute, $htmlOptions = array()) {
        $return = '';

        $htmlOptions['labelOptions'] = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();
        $return .= $this->label($attribute, $htmlOptions['labelOptions']);

        unset($htmlOptions['labelOptions']);

        $return .= $this->textArea($attribute, $htmlOptions);

        return $return;
    }

    public function textArea($attribute, $htmlOptions = array()) {
        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }

        if (empty($htmlOptions['name'])) {
            $htmlOptions['name'] = Html::resolveName($this->model, $attribute);
        }
        if (empty($htmlOptions['id'])) {
            $htmlOptions['id'] = Html::resolveId($this->model, $attribute);
        }
        if (empty($htmlOptions['value'])) {
            $htmlOptions['value'] = $this->model->getValue($attribute);
        }
        $value = $htmlOptions['value'];
        unset($htmlOptions['value']);

        return Html::tag('textarea', $htmlOptions, $value);
    }

    public function checkBoxRow($attribute, $htmlOptions = array()) {
        $return = '';

        $labelOptions = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();
        unset($htmlOptions['labelOptions']);

        $label                 = isset($labelOptions['label']) ? $labelOptions['label'] : $this->getLabel($attribute);
        $labelOptions['label'] = $this->checkBox($attribute, $htmlOptions) . " " . $label;

        $return .= $this->label($attribute, $labelOptions);

        return $return;
    }

    public function checkBox($attribute, $htmlOptions = array()) {
        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }

        $htmlOptions['type'] = 'checkbox';
        if (!isset($htmlOptions['value'])) {
            $htmlOptions['value'] = 1;
        }
        if ($this->model->getValue($attribute)) {
            $htmlOptions['checked'] = "checked";
        }

        return $this->textField($attribute, $htmlOptions);
    }

    public function dropDown($attribute, $data = array(), $htmlOptions = array()) {
        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }
        $options = $this->dropDownOptions($data, $this->model->getValue($attribute));

        if (empty($htmlOptions['name'])) {
            $htmlOptions['name'] = Html::resolveName($this->model, $attribute);
        }
        if (empty($htmlOptions['id'])) {
            $htmlOptions['id'] = Html::resolveId($this->model, $attribute);
        }

        return Html::tag('select', $htmlOptions, $options);
    }

    public function dropDownRow($attribute, $data = array(), $htmlOptions = array()) {
        $return = '';

        $htmlOptions['labelOptions'] = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();
        $return .= $this->label($attribute, $htmlOptions['labelOptions']);

        unset($htmlOptions['labelOptions']);

        $return .= $this->dropDown($attribute, $data, $htmlOptions);

        return $return;
    }

    public function dropDownOptions($data, $selected = null) {
        $return = "";
        foreach ($data as $value => $label) {
            $htmlOptions = array('value' => $value);
            if ($value == $selected) {
                $htmlOptions['selected'] = 'selected';
            }
            $return .= Html::tag('option', $htmlOptions, $label);
        }

        return $return;
    }

    public function radio($attribute, $data = array(), $htmlOptions = array()) {
        $i        = 0;
        $selected = $this->model->getValue($attribute);
        $return   = $this->textField($attribute, array(
                                                      'type'  => 'hidden',
                                                      'value' => $selected
                                                 ));
        foreach ($data as $value => $label) {
            $options = array(
                'type'  => 'radio',
                'value' => $value,
                'id'    => Html::resolveId($this->model, $attribute) . '_' . $i
            );
            $options = $htmlOptions + $options;
            if ($value == $selected) {
                $options['checked'] = 'checked';
            }

            $return .= Html::tag('label', array('class' => 'radio'), $this->textField($attribute, $options) . ' ' . $label);
            $i++;
        }

        return $return;
    }

    public function radioRow($attribute, $data = array(), $htmlOptions = array()) {
        $return = '';

        $labelOptions          = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();
        $labelOptions['label'] = isset($labelOptions['label']) ? $labelOptions['label'] : $this->getLabel($attribute);
        $return .= $this->label($attribute, $labelOptions);
        $return .= $this->radio($attribute, $data, $htmlOptions);

        return $return;
    }

    public function submit($label, $htmlOptions = array()) {
        $htmlOptions['type']  = isset($htmlOptions['type']) ? $htmlOptions['type'] : "submit";
        $htmlOptions['value'] = $label;

        return Html::tag('input', $htmlOptions, false, false);
    }

    public function submitRow($label, $htmlOptions = array()) {
        $htmlOptions['class'] = isset($htmlOptions['class']) ? $htmlOptions['class'] .= ' btn' : 'btn';

        return '<div class="form-actions">' . $this->submit($label, $htmlOptions) . '</div>';
    }

    public function getLabel($attribute) {
        $labels = $this->model->attributeLabels();

        return (isset($labels[$attribute])) ? $labels[$attribute] : $attribute;
    }

    public function error($attribute) {
        if ($errors = $this->model->getErrors($attribute)) {
            $return = array();
            foreach ($errors as $error) {
                $return[] = $error;
            }

            return '<span class="help-block text-error">' . implode('<br>', $return) . '</span>';
        }

        return '';
    }
}