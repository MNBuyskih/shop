<?php
/**
 * CKEditor.php
 * Date: 22.07.13
 * Time: 16:16
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class CKEditor extends Widget {
    /**
     * @var ActiveForm
     */
    public $form;

    /**
     * @var string
     */
    public $attribute;

    /**
     * @var Model
     */
    public $model;

    /**
     * @var array
     */
    public $htmlOptions = array();

    public function run() {
        $this->model                = $this->form->model;
        $this->htmlOptions['class'] = isset($this->htmlOptions['class']) ? $this->htmlOptions['class'] .= ' ckeditor' : 'ckeditor';
        echo $this->form->textAreaRow($this->attribute, $this->htmlOptions);
        echo '<script src="http://cdnjs.cloudflare.com/ajax/libs/ckeditor/4.0.1/ckeditor.js"></script>';
    }
}