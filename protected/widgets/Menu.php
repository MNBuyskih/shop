<?php
/**
 * Menu.php
 * Date: 24.07.13
 * Time: 9:12
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Menu extends Widget {
    public $items = array();

    public $htmlOptions = array();

    public function run() {
        echo $this->renderMenu($this->items);
    }

    protected function renderMenu($items) {
        $htmlOptions = $this->htmlOptions;
        Html::addClass('nav', $htmlOptions);

        $return = Html::openTag('ul', $htmlOptions);
        foreach ($items as $item) {
            if (isset($item['visible']) && !$item['visible']) {
                continue;
            }

            $itemOptions = isset($item['itemOptions']) ? $item['itemOptions'] : array();

            $linkOptions         = isset($item['linkOptions']) ? $item['linkOptions'] : array();
            $linkOptions['href'] = isset($item['url']) ? $item['url'] : '#';

            $return .= Html::openTag('li', $itemOptions);
            $return .= Html::tag('a', $linkOptions, $item['label']);

            if (isset($item['items']) && is_array($item['items']) && count($item['items'])) {
                $return .= $this->renderMenu($item['items']);
            }
            $return .= "</li>";
        }
        $return .= "</ul>";

        return $return;
    }

}