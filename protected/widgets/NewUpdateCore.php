<?php
/**
 * NewUpdateCore.php
 * Date: 13.08.13
 * Time: 13:46
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class NewUpdateCore extends Widget {

    public function run() {
        App::import("/admin/protected/models/UpdateCore");
        $available = UpdateCore::available();

        if ($available) {
            $string = 'Доступна новая версия ядра системы: ';
            $string .= $available['version'];
            $string .= '<a class="btn btn-primary pull-right" href="';
            $string .= App::app()->createUrl('/admin/updateCore');
            $string .= '">Об этом обновлени</a><div class="clearfix"></div>';

            echo Html::tag('div', array('class' => 'alert'), $string);
        }
    }

}