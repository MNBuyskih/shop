<?php
/**
 * .php
 * Date: 26.07.13
 * Time: 11:40
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class CatalogNew extends Widget {
    public function run() {
        App::import('//models/Products');
        App::import('//models/CartForm');
        App::import('//library/Pagination');

        $pagination           = new Pagination();
        $pagination->pageSize = 20;

        $products = Products::create()->findAllByQuery('SELECT
    *
FROM
    `products` `p`
JOIN `variants` `v`
    ON `v`.`product_id` = `p`.`product_id`
WHERE
    `is_new` = 1
AND
    enabled = 1
AND
    `v`.`stock` > 0
GROUP BY
    `p`.`product_id`
ORDER BY
    `order_num`
LIMIT
    :limit
OFFSET
    :offset', array(
                   "int:limit"  => $pagination->getLimit(),
                   "int:offset" => $pagination->getOffset()
              ));

        $total                  = App::app()->db()->fetch('SELECT
    COUNT(*) `count`
FROM
    `products`
WHERE
    `is_new` = 1
AND
    enabled = 1');
        $pagination->totalCount = $total['count'];

        $this->owner->widget('ListView', array(
                                              'itemView' => '_newItem',
                                              'data'     => $products
                                         ));
        $this->owner->widget('Pager', array('pagination' => $pagination));
    }
}