<?php
/**
 * ListView.php
 * Date: 25.07.13
 * Time: 15:54
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class ListView extends Widget {
    public $data = array();

    public $itemView;

    public function run() {
        if (!count($this->data)) {
            echo "<p class='muted'>Нет товаров</p>";

            return;
        }
        echo Html::tag('ul', array('class' => 'thumbnails'));
        $this->renderContent();
        echo "</ul>";
    }

    protected function renderContent() {
        foreach ($this->data as $model) {
            $this->owner->renderPartial($this->itemView, array('model' => $model));
        }
    }

}