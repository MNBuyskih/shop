<?php
/**
 * GridView.php
 * Date: 23.07.13
 * Time: 10:44
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
class GridView extends Widget {

    public $data;

    public $columns = array();

    public $emptyText = "Нет данных";

    public $htmlOptions = array();

    /**
     * @var ColumnAbstract[]
     */
    protected $objects = array();

    public function run() {
        foreach ($this->columns as $i => $column) {
            $this->prepareColumn($column);

            $this->objects[$i] = $this->getClass($column);
        }

        $return = "";
        $return .= $this->renderHeader();
        $return .= $this->renderContent();
        $return .= $this->renderFooter();

        echo $return;
    }

    protected function renderHeader() {
        Html::addClass('table table-striped table-hover', $this->htmlOptions);
        $return = Html::openTag('table', $this->htmlOptions);
        $return .= Html::openTag('thead');
        $return .= Html::openTag('tr');

        foreach ($this->objects as $object) {
            $return .= "<th>" . $object->renderHeader() . "</th>";
        }

        $return .= "</thead></tr>";

        return $return;
    }

    protected function renderContent() {
        $return = Html::openTag('tbody');

        if (!count($this->data)) {
            return Html::tag('tr', array('class' => 'empty warning'), Html::tag('td', array('colspan' => count($this->columns)), $this->emptyText));
        }

        foreach ($this->data as $i => $row) {
            $return .= Html::openTag('tr');

            foreach ($this->objects as $object) {
                $return .= "<td>" . $object->renderContent($row, $i) . "</th>";
            }

            $return .= "</tr>";
        }

        $return .= "</tbody>";

        return $return;
    }

    protected function renderFooter() {
        return "</table>";
    }

    protected function prepareColumn(&$column) {
        if (!is_array($column)) {
            $column = array(
                'class'  => 'DataColumn',
                'name'   => $column,
                'header' => $column,
            );
        }

        if (empty($column['class'])) {
            $column['class'] = "DataColumn";
        }
    }

    protected function getClass($column) {
        $className = $column['class'];
        unset($column['class']);

        App::import('//widgets/GridView/ColumnAbstract');
        App::import('//widgets/GridView/' . $className);

        return new $className($column);
    }
}