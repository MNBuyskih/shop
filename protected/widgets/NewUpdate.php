<?php
/**
 * NewUpdate.php
 * Date: 26.07.13
 * Time: 13:39
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class NewUpdate extends Widget {
    public function run() {
        App::import("/admin/protected/models/Update");
        $available = Update::available();
        if ($available) {
            ?>
            <div class="alert alert-error">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                Доступно обновление от <strong><?php echo  date('d.m.Y H:i', (string)$available) ?></strong>
                <a class="btn btn-danger pull-right" href="<?php echo  App::app()->createUrl('/admin/update') ?>"><i class="icon-refresh icon-white"></i> Обновить!</a>

                <div class="clearfix"></div>
            </div>
        <?php
        }
    }
}