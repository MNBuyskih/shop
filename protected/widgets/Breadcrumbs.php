<?php
/**
 * Breadcrumbs.php
 * Date: 22.07.13
 * Time: 13:51
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Breadcrumbs extends Widget {

    public $links = array();

    public $mainPage = "Главная";

    public $mainPageUrl = '/';

    public $separator = "\n<span class=\"divider\">/</span>\n";

    public function run() {
        $return = array();

        if (count($this->links)) {
            if ($this->mainPage) {
                $return[] = '<li><a href="' . $this->owner->createUrl($this->mainPageUrl) . '">' . $this->mainPage . '</a></li>';
            }

            foreach ($this->links as $link => $url) {
                if (!is_integer($link)) {
                    $url      = is_array($url) ? $this->owner->createUrl($url) : $url;
                    $return[] = '<li><a href="' . $url . '">' . $link . '</a></li>';
                } else {
                    $return[] = '<li class="active">' . ($url) . '</li>';
                }
            }
        } elseif ($this->mainPage) {
            $return[] = '<li class="active">' . $this->mainPage . '</li>';
        }

        $return = "<ul class='breadcrumb'>" . implode($this->separator, $return) . "</ul>";
        echo $return;
    }

}