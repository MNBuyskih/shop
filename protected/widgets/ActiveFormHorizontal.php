<?php
/**
 * ActiveFormHorizontal.php
 * Date: 22.07.13
 * Time: 15:40
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

require_once "ActiveForm.php";

class ActiveFormHorizontal extends ActiveForm {
    public function run() {
        Html::addClass('form-horizontal', $this->htmlOptions);
        parent::run();
    }

    public function textFieldRow($attribute, $htmlOptions = array()) {
        $labelOptions = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();
        Html::addClass("control-label", $labelOptions);

        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }

        unset ($htmlOptions['labelOptions']);

        return '<div class="control-group' . ($this->model->hasErrors($attribute) ? ' error' : '') . '">' . $this->label($attribute, $labelOptions) . '<div class="controls">' . $this->textField($attribute, $htmlOptions) . $this->error($attribute) . '</div></div>';
    }

    public function textAreaRow($attribute, $htmlOptions = array()) {
        $labelOptions = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();
        Html::addClass("control-label", $labelOptions);

        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }

        unset ($htmlOptions['labelOptions']);

        return '<div class="control-group' . ($this->model->hasErrors($attribute) ? ' error' : '') . '">' . $this->label($attribute, $labelOptions) . '<div class="controls">' . $this->textArea($attribute, $htmlOptions) . $this->error($attribute) . '</div></div>';
    }

    public function checkBoxRow($attribute, $htmlOptions = array()) {
        $labelOptions = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();

        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }

        unset ($htmlOptions['labelOptions']);
        $label                 = isset($labelOptions['label']) ? $labelOptions['label'] : $this->getLabel($attribute);
        $label                 = $this->checkBox($attribute, $htmlOptions) . " " . $label;
        $labelOptions['label'] = $label;
        $labelOptions['for']   = false;

        return '<div class="control-group' . ($this->model->hasErrors($attribute) ? ' error' : '') . '"><div class="controls">' . $this->label($attribute, $labelOptions) . '</div></div>';
    }

    public function radioRow($attribute, $data = array(), $htmlOptions = array()) {
        $labelOptions          = isset($htmlOptions['labelOptions']) ? $htmlOptions['labelOptions'] : array();
        $labelOptions['label'] = isset($labelOptions['label']) ? $labelOptions['label'] : $this->getLabel($attribute);
        Html::addClass("control-label", $labelOptions);
        unset ($htmlOptions['labelOptions']);

        if ($this->model->hasErrors($attribute)) {
            Html::addClass('error', $htmlOptions);
        }

        return '<div class="control-group' . ($this->model->hasErrors($attribute) ? ' error' : '') . '">' . $this->label($attribute, $labelOptions) . '<div class="controls">' . $this->radio($attribute, $data, $htmlOptions) . '</div></div>';
    }

    public function error($attribute) {
        if ($errors = $this->model->getErrors($attribute)) {
            $return = array();
            foreach ($errors as $error) {
                $return[] = $error;
            }

            return '<span class="help-inline text-error">' . implode('; ', $return) . '</span>';
        }

        return '';
    }

}