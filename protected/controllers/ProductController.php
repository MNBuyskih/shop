<?php
/**
 * ProductController.php
 * Date: 25.07.13
 * Time: 16:28
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class ProductController extends Controller {

    public $currentCatalogId;

    public function init() {
        App::import('//models/Catalog');
        App::import('//models/Products');
        parent::init();
    }

    public function actionIndex() {
        $id    = App::app()->getRequest()->getParam('id');
        $alias = App::app()->getRequest()->getParam('alias');

        if (!$id && !$alias) {
            throw new HttpException(404, 'Документ не найден');
        }

        /** @var $product Products */
        if ($alias) {
            $product = Products::create()->findByQuery("SELECT
    *
FROM
    `products` `p`
    JOIN `variants` `v`
        ON `v`.`product_id` = `p`.`product_id`
WHERE `url` = :url
    AND `v`.`stock` > 0", array(':url' => $alias));
        } else {
            $product = Products::create()->find($id);
        }

        if (!$product) {
            throw new HttpException(404, 'Товар не найден');
        }

        $this->currentCatalogId = $product->category_id;

        $product->breadcrumbs($this->breadcrumbs);

        App::import('//models/CartForm');
        $cartForm = new CartForm($product);

        $this->render('view', array(
                                   'model'    => $product,
                                   'cartForm' => $cartForm
                              ));
    }
}