<?php
/**
 * Doc.php
 * Date: 22.07.13
 * Time: 12:10
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class DocController extends Controller {
    public function actionView() {
        $alias = App::app()->getRequest()->getParam('alias');

        App::import('//models/Doc');
        $model = Doc::create()->findByAttributes(array('alias' => $alias));

        if (!$model) {
            throw new HttpException(404, 'Документ не найден');
        }

        $this->render('view', array('model' => $model));
    }
}