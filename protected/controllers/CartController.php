<?php
/**
 * OrderController.php
 * Date: 07.08.13
 * Time: 9:41
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class CartController extends Controller {

    public function actionIndex() {
        App::import('//models/CartCookie');
        $data = CartCookie::create()->getData();

        $this->render('index', array('data' => $data));
    }

    public function actionAdd() {
        if (!isset($_POST['CartForm']) || !isset($_POST['CartForm']['variant'])) {
            throw new HttpException(404);
        }

        App::import('//models/Variants');
        /** @var Variants $model */
        $model = Variants::create()->find($_POST['CartForm']['variant']);
        if (!$model) {
            throw new HttpException(404, "Вариант не найден");
        }

        App::import('//models/Products');
        /** @var Products $product */
        $product = $model->getProduct();
        if (!$product) {
            throw new HttpException(404, "Товар не найден");
        }

        App::import('//models/CartForm');
        $form = new CartForm($product);
        $form->setAttributes($_POST['CartForm']);
        $form->set();

        if (App::app()->getRequest()->isAjaxRequest()) {
            echo json_encode(array(
                                  'total'  => CartCookie::create()->getTotalCount(),
                                  'cookie' => CartCookie::create()->getCookie(),
                             ));
            App::app()->end();
        } else {
            App::app()->getRequest()->back();
        }
    }

    public function actionDelete() {
        App::import('//models/CartCookie');
        CartCookie::create()->delete(App::app()->getRequest()->getParam('variant'));
        CartCookie::create()->setCookie();
        App::app()->getRequest()->back();
    }

    public function actionCalculate() {
        App::import('//models/CartCookie');
        $counts = App::app()->getRequest()->getParam('count');
        if ($counts) {
            foreach ($counts as $variant => $count) {
                CartCookie::create()->set($variant, $count);
            }
            CartCookie::create()->setCookie();
        }

        if (isset($_POST['process'])) {
            $this->redirect(array('/order/process'));
        }
        App::app()->getRequest()->back();
    }

    public function actionClear() {
        App::import('//models/CartCookie');
        CartCookie::create()->clear();
        CartCookie::create()->setCookie();
        App::app()->getRequest()->back();
    }
}