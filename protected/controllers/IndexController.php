<?php
/**
 * IndexController.php
 * Date: 22.07.13
 * Time: 9:40
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class IndexController extends Controller {

    public function actionIndex() {
        $this->render('index');
    }

}