<?php
/**
 * OrderController.php
 * Date: 07.08.13
 * Time: 14:39
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class OrderController extends Controller {
    public function init() {
        App::import('//models/Order');
        App::import('//models/Payment');
        App::import('//models/CartCookie');
        App::import('//models/Delivery');
        App::import('//models/Variants');
        App::import('//models/Products');

        parent::init();
    }

    public function actionProcess() {
        if (!CartCookie::create()->getTotalCount()) {
            $this->redirect('/cart');
        }

        $step = App::app()->getRequest()->getParam('step', 1);

        switch ($step) {
            case 1:
                $data = CartCookie::create()->getData();
                $this->render('step1', array('data' => $data));
                break;

            case 2:
                $model = new Order();
                $model->restoreState();

                if (isset($_POST['Order'])) {
                    $model->setAttributes($_POST['Order']);
                    if ($model->validate()) {
                        $model->saveState();
                        $this->redirect(array(
                                             '/order/process',
                                             'step' => 3
                                        ));
                    }
                } else {
                    $model->comment = "Товары на общую сумму: " . CartCookie::create()->getTotalSum() . " руб.";
                }

                $this->render('step2', array('model' => $model));
                break;

            case 3:
                $model = new Order();
                $model->restoreState();

                if (isset($_POST['Order'])) {
                    $model->setAttributes($_POST['Order']);
                    if ($model->validate(array('delivery_id'))) {
                        $model->saveState();
                        $this->redirect(array(
                                             '/order/process',
                                             'step' => 4
                                        ));
                    }
                }

                $this->render('step3', array('model' => $model));
                break;

            case 4:
                $model = new Order();
                $model->restoreState();

                if (isset($_POST['Order'])) {
                    $model->setAttributes($_POST['Order']);
                    if ($model->validate(array('paytype_id'))) {
                        $model->saveState();
                        $this->redirect(array(
                                             '/order/process',
                                             'step' => 5
                                        ));
                    }
                }

                $this->render('step4', array('model' => $model));
                break;

            case 5:
                $model = new Order();
                $model->restoreState();

                $this->render('step5', array('model' => $model));
                break;

            default:
                throw new HttpException(404, 'Документ не найден');
                break;
        }
    }

    public function actionFinish() {
        $model = new Order();
        $model->restoreState();

        $result = $model->save();

        if ($result) {
            // TODO: send user email
            //$model->userEmail();
            $this->render('finish', array('model' => (object)$result['order']));
        } else {
            $this->render('error', array('model' => $model));
        }
    }

    public function actionView() {
        $code = App::app()->getRequest()->getParam('code');
        if (!$code) {
            throw new HttpException(400, 'Не верный запрос');
        }

        $data = Order::findByCode($code);
        if (!$data) {
            throw new HttpException(404, 'Заказ не найден');
        }
        $data = $data->orders[0];

        $order = new Order();
        $order->setAttributes($data);
        $_counts = (array)$data->variants;

        $counts = array();
        foreach ($_counts as $variantId => $count) {
            $counts[$variantId] = $count;
        }

        $variantsIds = array_keys($counts);
        $query       = "SELECT * FROM `variants` WHERE `variant_id` IN (" . implode(', ', $variantsIds) . ")";
        $variants    = Variants::create()->findAllByQuery($query);

        $delivery = $data->delivery;

        $this->render('view', array(
                                   'order'    => $order,
                                   'variants' => $variants,
                                   'counts'   => $counts,
                                   'delivery' => $delivery
                              ));
    }

    public function actionBank() {
        $code  = App::app()->getRequest()->getParam('code');
        $order = Order::findByCode($code);

        $model = new Order();
        $model->setAttributes($order['order']);

        echo $this->renderPartial('bank', array('model' => $model), true);
        App::app()->end();
    }
}