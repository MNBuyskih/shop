<?php
/**
 * Error.php
 * Date: 24.07.13
 * Time: 9:01
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class ErrorController extends Controller {

    public $layout = 'error';

    public function init() {
        App::app()->config['theme'] = 'default';
        App::app()->resetTheme();
        parent::init();
    }

    public function actionException(Exception $exception) {
        if (App::app()->getRequest()->isAjaxRequest()) {
            echo $exception->getMessage();
            App::app()->end();
        }

        $this->render('exception', array('exception' => $exception));
    }

    public function actionError($params) {
        $this->render('error', array('error' => $params));
    }
}