<?php
/**
 * CatalogController.php
 * Date: 25.07.13
 * Time: 14:52
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class CatalogController extends Controller {

    public $currentCatalogId;

    public function init() {
        App::import('//models/Catalog');
        App::import('//models/Products');
        App::import('//models/Brands');
        App::import('//models/Variants');
        App::import('//models/CartForm');
        parent::init();
    }

    public function actionIndex() {
        $id    = App::app()->getRequest()->getParam('id');
        $alias = App::app()->getRequest()->getParam('alias');

        if (!$id && !$alias) {
            throw new HttpException(404, 'Документ не найден');
        }

        /** @var $category Catalog */
        if ($alias) {
            $category = Catalog::create()->findByQuery("SELECT * FROM `categories` WHERE `url` COLLATE utf8_bin = :url AND `enabled` = 1", array(':url' => $alias));
        } else {
            $category = Catalog::create()->find($id);
        }

        if (!$category) {
            throw new HttpException(404, 'Документ не найден');
        }

        $this->currentCatalogId = $category->category_id;

        App::import("//library/Pagination");
        $pagination           = new Pagination();
        $pagination->pageSize = 20;

        App::import("//models/FilterForm");
        $filter = new FilterForm();

        $pagination->totalCount = $category->productCount($filter);
        $products               = $category->products($pagination->getLimit(), $pagination->getOffset(), $filter);

        /** @var Catalog[] $breadcrumbs */
        $category->breadcrumbs($this->breadcrumbs);

        $this->render('index', array(
                                    'category'   => $category,
                                    'products'   => $products,
                                    'pagination' => $pagination,
                                    'filter'     => $filter
                               ));
    }
}