<?php
$main = array(
    'name'            => 'Shop',
    'theme'           => 'default',
    'controllersPath' => '/protected/controllers',
    'view'            => array(
        'viewFile'  => '/protected/library/View.php',
        'viewClass' => 'View',
    ),
);

$params = file_exists(__DIR__ . '/db.php') ? require_once __DIR__ . '/db.php' : array();

return $main + $params;