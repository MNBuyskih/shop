<?php
/**
 * admin.php
 * Date: 22.07.13
 * Time: 12:31
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

$config                    = require 'main.php';
$config['theme']           = 'admin';
$config['name']            = 'Shop admin';
$config['controllersPath'] = '/admin/protected/controllers';

$config['update'] = array(
    'serverUrl' => 'http://mmamarket.ru/updateShop',
);
return $config;