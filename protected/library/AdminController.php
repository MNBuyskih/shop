<?php
/**
 * AdminController.php
 * Date: 24.07.13
 * Time: 9:32
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

require_once "Controller.php";

abstract class AdminController extends Controller {

    public $menu = array();

    public function init() {
        if (!App::app()->isAdmin()) {
            $this->redirect('/admin/login');
        }
    }

}