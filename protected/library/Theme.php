<?php
/**
 * Theme.php
 * Date: 22.07.13
 * Time: 10:57
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Theme {

    protected $_name;

    protected $_basePath;

    protected $_baseUrl;

    function __construct($name) {
        $this->_name = $name;
    }

    public function getBasePath() {
        if (!$this->_basePath) {
            $this->_basePath = App::app()->basePath . '/themes/' . $this->_name;
        }

        return $this->_basePath;
    }

    public function getBaseUrl() {
        if (!$this->_baseUrl) {
            $this->_baseUrl = App::app()->baseUrl . '/themes/' . $this->_name;
        }

        return $this->_baseUrl;
    }

}