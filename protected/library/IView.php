<?php
/**
 * IView.php
 * Date: 22.07.13
 * Time: 10:24
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
interface IView {
    public function assign($name, $value = null);

    public function fetch($template, $return = true);

    public function setContent($content);

    public function getFileExtension();
}