<?php
/**
 * Pagination.php
 * Date: 26.07.13
 * Time: 9:30
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Pagination {
    public $pageVar = 'page';

    public $pageSize = 10;

    public $totalCount = 0;

    public function getTotalPages() {
        return ceil($this->totalCount / $this->pageSize);
    }

    public function getCurrentPage() {
        return isset($_REQUEST[$this->pageVar]) ? $_REQUEST[$this->pageVar] : 1;
    }

    public function getLimit() {
        return $this->pageSize;
    }

    public function getOffset() {
        return ($this->getCurrentPage() - 1) * ($this->pageSize);
    }
}