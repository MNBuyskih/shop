<?php
/**
 * Db.php
 * Date: 23.07.13
 * Time: 8:44
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

abstract class Db {

    public static function create($host, $username, $password, $dbName, $className = null) {
        if (!$className) {
            $className = App::app()->config['db']['class'];
        }

        if (empty($className)) {
            throw new HttpException(500, "No Db class selected");
        }

        App::import('//library/' . $className);

        /** @var Db $db */
        $db = new $className();
        $db->connect($host, $username, $password, $dbName);

        return $db;
    }

    public abstract function connect($host, $username, $password, $dbName);

    public abstract function query($query, $params = array());

    public abstract function fetchAll($query, $params = array());

    public abstract function fetch($query, $params = array());

    public abstract function insertId();

}

class DbException extends HttpException {
    const ERROR_CONNECT = 100;

    const ERROR_SELECT_DB = 200;

    const ERROR_QUERY = 300;
}