<?php
/**
 * View.php
 * Date: 22.07.13
 * Time: 10:29
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
class View implements IView {

    protected $data = array();

    public function assign($name, $value = null) {
        if (is_array($name)) {
            foreach ($name as $n => $v) {
                $this->assign($n, $v);
            }
        } else {
            $this->data[$name] = $value;
        }

        return $this;
    }

    public function fetch($template, $return = true) {
        ob_start();
        extract($this->data);
        require $template;
        $content = ob_get_clean();

        if ($return) {
            return $content;
        } else {
            echo $content;

            return $this;
        }
    }

    public function setContent($content) {
        $this->assign('content', $content);
    }

    public function getFileExtension() {
        return 'php';
    }

}