<?php
/**
 * DbMySQL.php
 * Date: 23.07.13
 * Time: 8:52
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class DbMySQL extends Db {

    public $link;

    public function connect($host, $username, $password, $dbName) {
        $this->link = mysql_connect($host, $username, $password, true);
        if (!$this->link) {
            throw new DbException(500, "Can`t connect to mysql: " . mysql_error(), DbException::ERROR_CONNECT);
        }

        if (!mysql_select_db($dbName, $this->link)) {
            throw new DbException(500, "Can`t select mysql DB: " . mysql_error($this->link), DbException::ERROR_SELECT_DB);
        }

        $this->query('SET NAMES utf8');
    }

    public function query($query, $params = array()) {
        if (APP_DEBUG) {
            App::app()->log()->set("Execute query: <code>" . $query . "</code>");
        }

        $query  = $this->prepare($query, $params);
        $result = mysql_query($query, $this->link);
        if (!$result) {
            throw new DbException(500, "Can`t query: " . mysql_error($this->link) . ". Query: " . $query, DbException::ERROR_QUERY);
        }

        return $result;
    }

    public function fetchAll($query, $params = array()) {
        $result = $this->query($query, $params);
        $rows   = array();
        while ($row = mysql_fetch_assoc($result)) {
            $rows[] = $row;
        }

        return $rows;
    }

    public function fetch($query, $params = array()) {
        $result = $this->query($query, $params);
        if (!$result) {
            return null;
        }

        $return = mysql_fetch_assoc($result);

        return $return;
    }

    public function insertId() {
        return mysql_insert_id($this->link);
    }

    public function prepare($query, $params = array()) {
        foreach ($params as $param => $value) {
            $value = mysql_real_escape_string($value, $this->link);

            if (strpos($param, 'int') === 0) {
                $param = preg_replace('/^[^:]+:/', ':', $param);
                $value = (int)$value;
            } else {
                $value = $this->quote($value);
            }

            $query = str_replace($param, $value, $query);
        }

        return $query;
    }

    public function quote($string) {
        if (!is_numeric($string)) {
            $string = "\"$string\"";
        }

        return $string;
    }
}