<?php
/**
 * Widget.php
 * Date: 22.07.13
 * Time: 13:47
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

abstract class Widget {

    /**
     * @var Controller
     */
    protected $owner;

    public function __construct(Controller $owner) {
        $this->owner = $owner;

        $this->init();
    }

    public function init() {

    }

    public function setParams($params = array()) {
        foreach ($params as $param => $value) {
            if (!property_exists($this, $param)) {
                throw new Exception("Property {$param} not found in class " . get_class($this));
            }
            $this->$param = $value;
        }
    }

    public function run() {

    }
}