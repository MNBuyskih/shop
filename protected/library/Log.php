<?php
/**
 * Log.php
 * Date: 31.07.13
 * Time: 14:03
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Log {

    const STATUS_LOG = 100;

    const STATUS_WARNING = 200;

    const STATUS_ERROR = 300;

    protected $messages = array();

    /**
     * @param     $message
     * @param int $status
     * @param array|int $traceLevel
     */
    public function set($message, $status = self::STATUS_LOG, $traceLevel = 6) {
        $trace = array();
        if ($traceLevel !== false) {
            if (!is_array($traceLevel)) {
                $trace = array_slice(debug_backtrace(), 0, $traceLevel);
            } else {
                $trace = $traceLevel;
            }
        }

        $this->messages[] = array(
            'time'    => microtime(true),
            'message' => $message,
            'status'  => $status,
            'trace'   => $trace,
        );
    }

    public function startTime() {
        return microtime(true);
    }

    public function endTime($startTime) {
        return microtime(true) - $startTime;
    }

    public function fetch() {
        $executionTime = round(App::app()->executionTime(), 3);
        $memoryUsage   = round(App::app()->memoryUsage() / 1024 / 1024, 4);

        echo Html::openTag('div', array('class' => 'well'));
        foreach ($this->messages as $message) {
            $htmlOptions = array('class' => 'alert');
            switch ($message['status']) {
                case (self::STATUS_WARNING):
                    break;
                case (self::STATUS_ERROR):
                    Html::addClass('alert-danger', $htmlOptions);
                    break;
                default:
                    Html::addClass('alert-info', $htmlOptions);
                    break;
            }

            list($time, $microTime) = explode('.', $message['time']);

            echo Html::tag('div', $htmlOptions, Html::tag('i', array(), date('d.m.Y H:i:s', $time) . '.' . $microTime) . Html::tag('p', array(), $message['message'] . $this->fetchTrace($message['trace'])));
        }

        echo Html::tag('div', array('class' => 'alert alert-info'), "Время генерации страницы (сек)<br>Без сообщений лога: {$executionTime}<br>Вместе с сообщениями лога: " . round(App::app()->executionTime(), 3));
        echo Html::tag('div', array('class' => 'alert alert-info'), "Использовано памяти (MB)<br>Без сообщений лога: {$memoryUsage}<br>Вместе с сообщениями лога: " . round(App::app()->memoryUsage() / 1024 / 1024, 4));
        echo "</div>";
    }

    protected function fetchTrace($trace) {
        if (empty($trace)) {
            return "";
        }

        $return = "<pre>";
        foreach ($trace as $t) {
            if (isset($t['class']) && isset($t['function']) && $t['class'] !== 'Log' && $t['function'] !== "set") {
                if (isset($t['class'])) {
                    $return .= $t['class'] . "::";
                }
                if (isset($t['function'])) {
                    $return .= $t['function'] . "()";
                }

                if (isset($t['file']) && isset($t['line'])) {
                    $return .= " in " . $t['file'] . " ({$t['line']})";
                    $return .= "\n";
                }
            }

        }
        $return .= "</pre>";

        return $return;
    }

}