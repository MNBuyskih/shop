<?php
/**
 * Gets the application start timestamp.
 */
defined('APP_BEGIN_TIME') or define('APP_BEGIN_TIME', microtime(true));

/**
 * Gets the application start timestamp.
 */
defined('APP_DEBUG') or define('APP_DEBUG', false);

/**
 * App.php
 * Date: 22.07.13
 * Time: 9:30
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
class App {

    /**
     * Текущая версия ядра
     *
     * @var float
     */
    public $version = "1.95";

    /**
     * Основной путь приложения
     *
     * @var string
     */
    public $basePath;

    /**
     * Основной url приложения
     *
     * @var string
     */
    public $baseUrl;

    /**
     * Менеджер GET, POST, ... запросов
     *
     * @var Request
     */
    private $_request;

    /**
     * Экземпляр шаблонизатора
     *
     * @var IView
     */
    private $_view;

    /**
     * Экземпляр темы
     *
     * @var Theme
     */
    private $_theme;

    /**
     * Экземпляр текущего контроллера
     *
     * @var Controller
     */
    private $_controller;

    /**
     * Экземпляр класса для работы с БД
     *
     * @var Db
     */
    private $_db;

    /**
     * Экземпляр класса для работы с логами
     *
     * @var Log
     */
    private $_log;

    /**
     * Текущее приложение
     *
     * @var App
     */
    private static $_app;

    /**
     * Путь к файлу конфигурации
     *
     * @var string
     */
    private $configPath;

    /**
     * Конфигурация приложения
     *
     * @var array
     */
    public $config;

    /**
     * Конструктор. Реализован через синглтон
     *
     * @param string $configPath Путь к файлу конфигурации
     */
    private function __construct($configPath = null) {
        $this->configPath = $configPath;

        $this->basePath = $this->getBasePath();
        $this->baseUrl  = $this->getBaseUrl();

        $this->config();

        $this->setErrorHandlers();
    }

    /**
     * Автоматически определяет основной путь приложения
     *
     * @return string
     */
    public function getBasePath() {
        return pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_DIRNAME);
    }

    /**
     * Автоматически определяет основной url приложения
     *
     * @return string
     */
    public function getBaseUrl() {
        return str_replace(self::documentRoot(), '', $this->getBasePath());
    }

    /**
     * Устанавливает собственные обработчики ошибок
     */
    protected function setErrorHandlers() {
        set_error_handler('App::errorHandler');
        set_exception_handler('App::exceptionHandler');
    }

    /**
     * Обработчик исключений
     *
     * @param Exception $exception Исключение
     *
     * @see http://php.net/manual/ru/function.set-exception-handler.php
     */
    public static function exceptionHandler(Exception $exception) {
        if (APP_DEBUG) {
            App::app()->log()->set($exception->getMessage(), Log::STATUS_ERROR, $exception->getTrace());
        }

        $controller = App::app()->getController();
        if (!$controller) {
            $controller = App::app()->createController('error');
        }
        $controller->forward('exception', 'error', $exception);
    }

    /**
     * Обработчик ошибок
     *
     * @param integer $errno Код ошибки
     * @param string $errstr Сообщение ошибки
     * @param string $errfile Файл, в котором произошла ошибка
     * @param integer $errline Строка, к которой произошла ошибка
     * @param array $errContext Контекст ошибки
     *
     * @see http://php.net/manual/ru/function.set-error-handler.php
     *
     * @return bool|null
     */
    public static function errorHandler($errno, $errstr, $errfile, $errline, $errContext) {
        if (APP_DEBUG) {
            App::app()->log()->set($errstr, Log::STATUS_ERROR);
        }

        if (!(error_reporting() & $errno)) {
            return null;
        }

        $controller = App::app()->getController();
        if (!$controller) {
            $controller = App::app()->createController('error');
        }

        $controller->forward('error', 'error', array(
            'errorNumber'  => $errno,
            'errorString'  => $errstr,
            'errorFile'    => $errfile,
            'errorLine'    => $errline,
            'errorContext' => $errContext,
        ));

        return true;
    }

    /**
     * Создание экземпляра приложения.
     * Реализует патерн синглтон
     *
     * @param string $configPath Путь к файлу конфигурации
     *
     * @return App Созданное приложение
     */
    public static function createApplication($configPath) {
        if (!self::$_app) {
            self::$_app = new self($configPath);
        }

        return self::$_app;
    }

    /**
     * Возвращает текущее приложение
     *
     * @return App Текущее приложение
     */
    public static function app() {
        return self::$_app;
    }

    /**
     * @return string
     */
    public static function documentRoot() {
        $docRoot = $_SERVER['DOCUMENT_ROOT'];
        if (substr($docRoot, -1, 1) === '/') {
            $docRoot = substr($docRoot, 0, strlen($docRoot) - 1);
        }

        return $docRoot;
    }

    /**
     * Запускает приложение
     */
    public function run() {
        $this->checkInstallation();

        $controller = $this->getRequest()->getController();
        $action     = $this->getRequest()->getAction();
        $this->createController($controller)->run($action);
    }

    /**
     * Инициализирует данные конфигурации
     */
    private function config() {
        $this->config = require $this->configPath;
    }

    /**
     * Проверяет установлено приложение или нет
     */
    private function checkInstallation() {
        if (strpos($_SERVER['REQUEST_URI'], '/install') === false) {
            $config = $this->config;
            if (empty($config['db']) || empty($config['db']['user']) || empty($config['db']['host']) ||
                empty($config['db']['db'])
            ) {
                header("Location: {$this->baseUrl}/install");
                exit;
            }
        }
    }

    /**
     * Является ли текущий пользователь администратором.
     *
     * @return bool True - пользователь admin, false - в противном случае.
     */
    public function isAdmin() {
        $this->startSession();

        return isset($_SESSION) && isset($_SESSION['admin']) && $_SESSION['admin'];
    }

    /**
     * Запуск сессии
     */
    public function startSession() {
        if (!session_id()) {
            session_start();
        }
    }

    /**
     * Остановка сессии
     */
    public function closeSession() {
        session_destroy();
    }

    /**
     * Создаёт контроллер приложения
     *
     * @param string $controllerName Имя контроллера. Должно содержать только уникальное имя класса контроллера (без приствки "Controller").
     *
     * @return Controller Созданный контроллер
     * @throws HttpException Выбрасывает исключение с кодом 404 если контроллера не существует.
     */
    public function createController($controllerName) {
        if (APP_DEBUG) {
            $this->log()->set('Create controller "' . $controllerName . '"');
        }

        $className = ucfirst($controllerName) . 'Controller';
        if ($className !== 'ErrorController') {
            $fileName = $this->basePath . $this->config['controllersPath'] . '/' . $className . '.php';
        } else {
            $fileName = $this->basePath . '/protected/controllers/ErrorController.php';
        }

        if (!file_exists($fileName)) {
            throw new HttpException(404, 'Controller ' . $controllerName . ' not found');
        }

        require_once 'Controller.php';
        require_once $fileName;

        $this->_controller = new $className($controllerName);

        return $this->_controller;
    }

    /**
     * Возвращает текущий контроллер приложения
     *
     * @return Controller Текущий контроллер приложения
     */
    public function getController() {
        return $this->_controller;
    }

    /**
     * Создаёт (при необходимости) и возвращает Http-менеджер
     *
     * @return Request Http-менеджер
     */
    public function getRequest() {
        if (!$this->_request) {
            require_once "Request.php";
            $this->_request = new Request();
        }

        return $this->_request;
    }

    /**
     * Создаёт (при необходимости) и возвращает экземпляр класса вида, используемый для вывода основного шаблона.
     * Класс вида зависит от настроек. По умалчанию доступен только один - View.
     * Если есть нобходимость в собственном шаблонизаторе - можно создать новый класс, унаследовав его от интерфейса IView.
     * В файле конфигурации необходимо будет изменить настройки шаблонизатора:
     * <pre>
     * // ...
     * 'view' => array(
     *     'viewFile'  => '/path/to/MyView.php', // путь к файлу с классом шаблонизатора
     *     'viewClass' => 'View',                // имя класса шаблонизатора
     * ),
     * // ...
     * </pre>
     *
     * @return IView Класс вида.
     */
    public function getView() {
        if (!$this->_view) {
            $this->_view = $this->createView();
        }

        return $this->_view;
    }

    /**
     * Создаёт новый экземпляр класса шаблонизатора в зависимости от настроек (см. {@link App::getView()}).
     *
     * @return IView
     */
    public function createView() {
        require_once 'IView.php';
        $fileName  = $this->basePath . self::app()->config['view']['viewFile'];
        $className = self::app()->config['view']['viewClass'];
        require_once $fileName;

        return new $className();
    }

    /**
     * Создаёт (при необходимости) и возвращает основной экземпляр класса для работы с MySQL.
     * Класс выберается автоматически при инсталяции и сохраняется в файле конфигурации.
     * На данный момент (версия ядра 1.5) доступны два: {@link DbMySQL} и {@link DbPDO}
     *
     * @return Db Экземпляр класса для работы с MySQL
     */
    public function db() {
        if (!$this->_db) {
            App::import('//library/Db');
            $this->_db = Db::create(App::app()->config['db']['host'], App::app()->config['db']['user'],
                App::app()->config['db']['password'], App::app()->config['db']['db']);
        }

        return $this->_db;
    }

    /**
     * Создаёт (при необходимости) и возвращает экземпляр класса темы.
     * Имя темы задаётся в файле конфигурации.
     * По умолчанию тема - 'default'.
     *
     * @return Theme Экземпляр класса темы
     */
    public function getTheme() {
        if (!$this->_theme) {
            App::import('//library/Theme');
            $this->_theme = new Theme(App::app()->config['theme']);
        }

        return $this->_theme;
    }

    /**
     * Сбрасывает текущую тему.
     * После этого можно принудительно указать в конфиге название новой темы и заново вызванный App::app()->getTheme() вернёт новую тему.
     */
    public function resetTheme() {
        $this->_theme = null;
    }

    /**
     * Создаёт (при необходимости) и возвращает экземпляр класса лога.
     *
     * @return Log
     */
    public function log() {
        if (!$this->_log) {
            App::import('//library/Log');
            $this->_log = new Log();
        }

        return $this->_log;
    }

    /**
     * Выполняет рендер основного шаблона и выводит его в браузер.
     * Если включен дебаг ({@link APP_DEBUG}) метод выводит отладочную информацию в браузер.
     * Метод также завершает приложение.
     */
    public function output() {
        $view     = $this->getView();
        $template = $this->getTheme()->getBasePath() . '/views/layouts/' . $this->getController()->layout . '.' .
            $this->getView()->getFileExtension();
        $view->assign('controller', $this->getController());
        $view->fetch($template, false);

        if (APP_DEBUG) {
            App::app()->log()->fetch();
        }

        App::app()->end();
    }

    /**
     * Генерирует относительный URL.
     * Обязателен для применения если сайт расположен в подпапке.
     *
     * @param string|array $params
     * Если строка - маршрут.
     * Если массив - первый элемент массива - маршрут, остальные параметры URL как ключ => значение
     * Например
     * <pre>
     *    App::app()->baseUrl = "/mySite/custom/folder"; // Принудительно указываем основной URL приложения
     *    App::app()->createUrl('/foo/bar'); // Вернёт /mySite/custom/folder/foo/bar
     *    App::app()->createUrl(array('/foo/bar', 'key' => 'value', 'otherKey' => 'otherValue')); // Вернёт /mySite/custom/folder/foo/bar?key=value&otherKey=otherValue
     * </pre>
     *
     * @return string Сгенерированный URL
     */
    public function createUrl($params = array()) {
        $return = $this->baseUrl;
        if (!is_array($params)) {
            $params = array($params);
        }
        if (count($params)) {
            $route = array_shift($params);

            if (strpos($route, '/') !== 0) {
                $route = '/' . $route;
            }
            $return .= $route;
        }
        if (count($params)) {
            $return .= '?' . http_build_query($params);
        }

        return $return;
    }

    /**
     * Генерирует полный URL.
     *
     * @param string|array $params Параметры URL. См. {@link createUrl}.
     *
     * @link createUrl
     * @return string
     */
    public function createAbsoluteUrl($params = array()) {
        $url = $this->createUrl($params);

        return "http://" . $this->getRequest()->getHost() . $url;
    }

    /**
     * Импортирует библиотеку системы.
     *
     * @link getPathOfAlias
     *
     * @param string $alias Алиас пути к библиотеке. Должен быть задан в формате необходимом для {@link getPathOfAlias}
     */
    public static function import($alias) {
        if (APP_DEBUG) {
            //App::app()->log()->set("Import file " . $alias);
        }

        $path = App::app()->getPathOfAlias($alias);

        $path = $path . '.php';
        require_once $path;
    }

    /**
     * Генерериует полный путь к дирректории или файлу по его алиасу.
     *
     * @param string $alias Алиас дирректории или файла.
     * Должен начинаться с "/" если файл или дирретория расположены не в папке protected системы.
     * Должен начинаться с "//" если файл или дирректория расположены в папке protected системы.
     * В любом случае файл или дирректория должны быть расположены в папке заданной в {@link basePath}
     *
     * @return string
     */
    public function getPathOfAlias($alias) {
        $path    = $this->basePath . DIRECTORY_SEPARATOR;
        $appPath = $path . 'protected' . DIRECTORY_SEPARATOR;

        if (strpos($alias, '//') === 0) {
            $path = preg_replace('$^//$', $appPath, $alias);
        } elseif (strpos($alias, '/') === 0) {
            $path = preg_replace('$^/$', $path, $alias);
        }

        return $path;
    }

    /**
     * Выполняет строку $string как php перед этим извлекая содержимое массива $data как переменная => значение
     *
     * @param string $string Выполняемый php код. Не должен начинаться с "return" и заканчиваться ";".
     * @param array $data Дополнительные переменные передаваемые в выполняемый php код.
     *
     * @see http://php.net/manual/ru/function.eval.php
     * @see http://php.net/manual/ru/function.extract.php
     * @return mixed Результат выполнения php кода.
     */
    public function evaluate($string, $data = array()) {
        extract($data);

        return eval('return ' . $string . ';');
    }

    /**
     * Проверяет, что функция не было отключена в php.ini
     *
     * @param string $func Имя проверяемой функции.
     *
     * @return bool True - функция включена. False - отключена.
     */
    public function functionEnabled($func) {
        $disabled = explode(',', ini_get('disable_functions'));
        $disabled = array_map('trim', $disabled);

        return !in_array($func, $disabled);
    }

    /**
     * Возвращает количество использованной памяти.
     *
     * @see http://www.php.net/manual/ru/function.memory-get-usage.php
     * @return int Количество использованной памяти
     */
    public function memoryUsage() {
        return memory_get_usage();
    }

    /**
     * Возвращает время от начала отсчёта сохранённое в {@link APP_BEGIN_TIME} до вызова этого метода.
     *
     * @return float Время в секундах
     */
    public function executionTime() {
        return microtime(true) - APP_BEGIN_TIME;
    }

    /**
     * Завершает приложение.
     */
    public function end() {
        exit();
    }

}

/**
 * Class HttpException
 * Реализует исключение и отправляет заголовок с кодом статуса в браузер
 */
class HttpException extends Exception {

    /**
     * Статус код отправляемый в браузер
     *
     * @var integer
     */
    public $statusCode;

    /**
     * Конструктор
     *
     * @param integer $status Статус код отправляемый в браузер
     * @param string|null $message Сообщение исключения
     * @param integer $code Код ошибки
     */
    public function __construct($status, $message = null, $code = 0) {
        $this->statusCode = $status;
        header("HTTP/1.0 {$this->statusCode}");

        parent::__construct($message, $code);
    }
}

/**
 * Class Model
 * Основной класс для работы с моделями.
 * Используется для форм и моделей объектов данных
 */
abstract class Model {

    /**
     * Список ошибок.
     *
     * @var array
     */
    public $errors = array();

    /**
     * Имя таблицы. Используется только для моделей объектов данных.
     *
     * @var string
     */
    public $tableName;

    /**
     * Список отрибутов.
     *
     * @var array
     */
    public $attributes = array();

    /**
     * Имя атрибута используемого в качестве основного ключа.
     * Используется только для моделей объектов данных.
     *
     * @var string
     */
    public $pk = 'id';

    /**
     * Создание экземпляра класса модели.
     *
     * @param string $className Имя класса модели
     *
     * @return Model
     */
    public static function create($className = __CLASS__) {
        $model = new $className();

        return $model;
    }

    /**
     * Валидация данных.
     *
     * @param array $attributes Список атрибутов для валидации. По умолчанию - все атрибуты.
     *
     * @return bool True - если валидация прошла успешно. False - в противном случае.
     */
    public function validate($attributes = array()) {
        if (!$this->beforeValidate()) {
            return false;
        }

        return $this->hasErrors();
    }

    /**
     * Возвращает список имён атрибутов.
     *
     * @return array Список имён атрибутов
     */
    public function attributeLabels() {
        return array();
    }

    /**
     * Возвращает имя атрибута.
     *
     * @param string $attribute
     *
     * @return string Имя атрибута или атрибут, если имя атрибута не было указано в {@link attributeLabels}
     */
    public function getAttributeLabel($attribute) {
        $labels = $this->attributeLabels();

        return isset($labels[$attribute]) ? $labels[$attribute] : $attribute;
    }

    /**
     * Возвращает значение атрибута
     *
     * @param string $attribute Имя атрибута
     *
     * @return mixed Значение атрибута
     */
    public function getValue($attribute) {
        return $this->$attribute;
    }

    /**
     * Устанавливает значения атрибутов из массива $data
     *
     * @param array $data Массив данных в формате атрибут => значение
     */
    public function setAttributes($data) {
        foreach ($data as $attribute => $value) {
            if (property_exists($this, $attribute)) {
                $this->$attribute = $value;
            }
        }
    }

    /**
     * Добавляет ошибку к атрибуту.
     * Используется при валидации данных
     *
     * @param string $attribute Имя атрибута
     * @param string $message Сообщение ошибки
     *
     * @return Model Активный объект модели
     */
    public function addError($attribute, $message) {
        $this->errors[$attribute][] = $message;

        return $this;
    }

    /**
     * Проверяет наличие ошибок.
     * Если передан $attribute проверяет наличие ошибок именно в нём.
     * Если $attribute = null - наличие ошибок у всех атрибутов.
     *
     * @param string|null $attribute Имя атрибута
     *
     * @return bool True - есть ошибки. False - нет ошибок.
     */
    public function hasErrors($attribute = null) {
        if ($attribute) {
            return isset($this->errors[$attribute]);
        }

        return !(bool)count($this->errors);
    }

    /**
     * Возвращает ошибки модели.
     * Если передан атрибут - вернёт только его ошибки.
     * По умолчанию возвращает все ошибки модели.
     *
     * @param string|null $attribute Имя атрибута.
     *
     * @return array|null Список ошибок или null если ошибок нет.
     */
    public function getErrors($attribute = null) {
        if ($attribute) {
            return isset($this->errors[$attribute]) ? $this->errors[$attribute] : null;
        }

        return $this->errors;
    }

    public function beforeValidate() {
        return true;
    }

    public function afterValidate() {

    }

    public function beforeSave() {
        return true;
    }

    public function afterSave() {

    }

    public function beforeDelete() {
        return true;
    }

    public function afterDelete() {

    }

    public function beforeFind() {
        return true;
    }

    public function afterFind() {

    }

    public function save($validate = true) {
        if ($validate && !$this->validate()) {
            return false;
        }

        if (!$this->beforeSave()) {
            return false;
        }

        if (!$this->tableName && !count($this->attributes)) {
            throw new HttpException(500, "Autosave can not be used if \$tableName and \$attributes are not specified");
        }

        if (!$this->{$this->pk}) {
            $result            = $this->_insert();
            $this->{$this->pk} = App::app()->db()->insertId();
        } else {
            $result = $this->_update();
        }

        $this->afterSave();

        return $result;
    }

    public function _insert() {
        $query = "INSERT INTO `{$this->tableName}` SET " . $this->prepareFields(true);

        return App::app()->db()->query($query, $this->prepareValues());
    }

    public function _update() {
        $query = "UPDATE `{$this->tableName}` SET " . $this->prepareFields() . " WHERE {$this->pk} = :{$this->pk}";

        return App::app()->db()->query($query, $this->prepareValues());
    }

    protected function prepareFields($insert = false) {
        $return = array();
        $params = $this->attributes;

        if ($insert) {
            unset($params[$this->pk]);
        }

        foreach ($params as $param => $value) {
            $return[] = "{$param} = $value";
        }

        return implode(",\n", $return);
    }

    protected function prepareValues() {
        $return = array();
        foreach ($this->attributes as $param => $value) {
            $return[$value] = $this->{$param};
        }

        return $return;
    }

    public function delete() {
        if (!$this->beforeDelete()) {
            return false;
        }

        $query  = "DELETE FROM `{$this->tableName}` WHERE {$this->pk} = :id";
        $params = array(":id" => $this->{$this->pk});
        $result = App::app()->db()->query($query, $params);

        $this->afterDelete();

        return $result;
    }

    /**
     * @param        $id
     * @param string $throw
     *
     * @return Model
     * @throws HttpException
     */
    public function find($id, $throw = 'Документ не найден') {
        $query  = "SELECT * FROM `{$this->tableName}` WHERE `{$this->pk}` = :id";
        $params = array(':id' => $id);
        $result = $this->findByQuery($query, $params);

        if ($throw && !$result) {
            throw new HttpException(404, $throw);
        }

        return $result;
    }

    public function findByAttributes($attributes) {
        $condition = $this->prepareCondition($attributes);
        $params    = $condition['attributes'];
        $condition = $condition['condition'];

        $query = "SELECT * FROM `{$this->tableName}` WHERE {$condition} LIMIT 1";

        return $this->findByQuery($query, $params);
    }

    public function findByQuery($query, $params = array()) {
        $data = App::app()->db()->fetch($query, $params);

        if (!$data) {
            return null;
        }
        if (!$this->beforeFind()) {
            return false;
        }
        $this->setAttributes($data);
        $this->afterFind();

        return $this;
    }

    public function findAll() {
        $query = "SELECT * FROM `{$this->tableName}`";

        return $this->findAllByQuery($query);
    }

    public function findAllByAttributes($attributes) {
        $condition = $this->prepareCondition($attributes);
        $params    = $condition['attributes'];
        $condition = $condition['condition'];

        $query = "SELECT * FROM `{$this->tableName}` WHERE {$condition}";

        return $this->findAllByQuery($query, $params);
    }

    public function findAllByQuery($query, $params = array()) {
        $data = App::app()->db()->fetchAll($query, $params);

        $models = array();
        foreach ($data as $row) {
            $className = get_class($this);
            /** @var Model $model */
            $model = new $className();
            if (!$model->beforeFind()) {
                $models[] = false;
                continue;
            }
            $model->setAttributes($row);
            $model->afterFind();
            $models[] = $model;
        }

        return $models;
    }

    protected function prepareCondition($attributes) {
        $return = array();
        $params = array();
        foreach ($attributes as $attribute => $value) {
            $return[]                = "{$attribute} = :{$attribute}";
            $params[":{$attribute}"] = $value;
        }

        return array(
            'condition'  => implode(' AND ', $return),
            'attributes' => $params
        );
    }
}

class Html {

    public static function tag($tag, $htmlOptions = array(), $content = false, $closeTag = true) {
        $html = '<' . $tag . self::renderAttributes($htmlOptions);
        if ($content === false) {
            return $closeTag ? $html . ' />' : $html . '>';
        } else {
            return $closeTag ? $html . '>' . $content . '</' . $tag . '>' : $html . '>' . $content;
        }
    }

    public static function openTag($tag, $htmlOptions = array()) {
        return "<{$tag}" . self::renderAttributes($htmlOptions) . ">";
    }

    public static function closeTag($tag) {
        return "</{$tag}>";
    }

    public static function renderAttributes($attributes = array()) {
        $return = array();
        foreach ($attributes as $attribute => $value) {
            $return[] = "$attribute=\"$value\"";
        }

        return " " . implode(" ", $return);
    }

    public static function resolveId(Model $model, $attribute) {
        return get_class($model) . '_' . $attribute;
    }

    public static function resolveName(Model $model, $attribute) {
        return get_class($model) . "[{$attribute}]";
    }

    public static function addClass($class, &$htmlOptions) {
        if (!isset($htmlOptions['class'])) {
            $htmlOptions['class'] = $class;
        } else {
            $htmlOptions['class'] .= ' ' . $class;
        }
    }

    public static function listData($models, $valueAttribute, $labelAttribute) {
        $return = array();
        foreach ($models as $model) {
            $return[$model->{$valueAttribute}] = $model->{$labelAttribute};
        }

        return $return;
    }
}