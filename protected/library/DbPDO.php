<?php
/**
 * DbPDO.php
 * Date: 30.07.13
 * Time: 9:28
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class DbPDO extends Db {

    /**
     * @var PDO
     */
    public $pdo;

    public function connect($host, $username, $password, $dbName) {
        try {
            $this->pdo = new PDO("mysql:host={$host};dbname={$dbName}", $username, $password);
        } catch (PDOException $e) {
            throw new DbException(500, "Can`t connect to mysql: " . $e->getMessage(), DbException::ERROR_CONNECT);
        }
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->query("SET NAMES utf8");
    }

    /**
     * @param       $query
     * @param array $params
     *
     * @return PDOStatement
     * @throws HttpException
     */
    public function query($query, $params = array()) {
        $time = null;
        if (APP_DEBUG) {
            App::app()->log()->set("Execute query: <code>" . $query . "</code>");
            $time = App::app()->log()->startTime();
        }

        $statement = $this->pdo->prepare($query);
        foreach ($params as $param => $value) {
            if (strpos($param, 'int') === 0) {
                $type  = PDO::PARAM_INT;
                $value = (int)$value;
                $param = preg_replace('/^[^:]+:/', ':', $param);
            } else {
                $type = PDO::PARAM_STR;
            }

            $statement->bindValue($param, $value, $type);
        }
        try {
            $statement->execute();
        } catch (PDOException $e) {
            throw new HttpException(500, $e->getMessage() . "<br>Query: <pre>" . $query . "</pre><br>Params: <pre>" . var_export($params, true) . "</pre>", DbException::ERROR_QUERY);
        }

        if (APP_DEBUG) {
            App::app()->log()->set("Query execution finished. Query time (sec): " . App::app()->log()->endTime($time), Log::STATUS_LOG, false);
        }

        return $statement;
    }

    /**
     * @param       $query
     * @param array $params
     *
     * @return array
     */
    public function fetchAll($query, $params = array()) {
        $statement = $this->query($query, $params);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param       $query
     * @param array $params
     *
     * @return array
     */
    public function fetch($query, $params = array()) {
        $statement = $this->query($query, $params);

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    public function insertId() {
        return $this->pdo->lastInsertId();
    }

}