<?php
/**
 * Request.php
 * Date: 22.07.13
 * Time: 9:42
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
class Request {

    private $_requestUri;

    public function getController() {
        return $this->getParam('controller', 'index');
    }

    public function getAction() {
        return $this->getParam('action', 'index');
    }

    public function getParam($name, $default = null) {
        return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default;
    }

    public function getRequestUri() {
        if (!$this->_requestUri) {
            $requestUri        = $_SERVER['REQUEST_URI'];
            $requestUri        = preg_replace('|^' . App::app()->baseUrl . '|', '', $requestUri);
            $this->_requestUri = preg_replace('|^/|', '', $requestUri);
        }

        return $this->_requestUri;
    }

    public function redirect($route, $exit = true) {
        $route = App::app()->createUrl($route);
        header("Location: $route");
        if ($exit) {
            App::app()->end();
        }
    }

    public function back() {
        $url = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : App::app()->createUrl(array('/'));
        header("Location: " . $url);
        App::app()->end();
    }

    public function getUri() {
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        return preg_replace("|^" . App::app()->baseUrl . "|", '', $uri);
    }

    public function isAjaxRequest() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
    }

    public function getHost() {
        return $_SERVER['SERVER_NAME'];
    }

}