<?php
/**
 * Controller.php
 * Date: 22.07.13
 * Time: 9:53
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
abstract class Controller {

    public $id;

    public $action;

    public $layout = "main";

    public $pageTitle;

    public $mainTitle;

    public $mainDescription;

    public $breadcrumbs = array();

    public $menu = array();

    public function __construct($id) {
        $this->id = $id;

        $this->init();
    }

    public function init() {
        App::import("//models/Catalog");
    }

    public function run($action = 'index', $params = array()) {
        if (APP_DEBUG) {
            App::app()->log()->set("Run action \"{$action}\"");
        }

        $this->forward($action, null, $params);
    }

    /**
     * @param       $action
     * @param null  $controller
     * @param mixed $params
     *
     * @throws HttpException
     */
    public function forward($action, $controller = null, $params = array()) {
        if (APP_DEBUG) {
            $controllerName = $controller ? $controller : get_class($this);
            App::app()->log()->set("Controller \"{$controllerName}\" forward to action \"{$action}\"");
        }

        if ($controller) {
            $controller = App::app()->app()->createController($controller);
            $controller->run($action, $params);

            return;
        }

        $this->action = $action;
        $action       = "action" . ucfirst($action);

        if (!method_exists($this, $action)) {
            throw new HttpException(404, 'Unknown action ' . $this->action);
        }

        $this->$action($params);
    }

    public function render($template, $data = array()) {
        App::app()->getView()->setContent($this->renderPartial($template, $data, true));
        App::app()->output();
    }

    public function renderPartial($template, $data = array(), $return = false) {
        $view = App::app()->createView();
        $view->assign($data);
        $view->assign('controller', $this);
        $output = $view->fetch($this->getTemplateFile($template));

        if ($return) {
            return $output;
        }

        echo $output;

        return $this;
    }

    protected function getTemplateFile($template) {
        $return = App::app()->getTheme()->getBasePath() . '/views/' . App::app()->getController()->id . '/' . $template . '.' . App::app()->getView()->getFileExtension();

        if (!file_exists($return)) {
            throw new HttpException(500, "Template file '{$template}' not found");
        }

        //$return = realpath($return);

        return $return;
    }

    public function createUrl($route = array()) {
        return App::app()->createUrl($route);
    }

    public function widget($widget, $params = array(), $run = true) {
        App::import('//library/Widget');
        App::import('//widgets/' . $widget);

        /** @var Widget $widget */
        $widget = new $widget($this);
        $widget->setParams($params);

        if ($run) {
            $widget->run();
        }

        return $widget;
    }

    public function redirect($route) {
        App::app()->getRequest()->redirect($route);
    }
}