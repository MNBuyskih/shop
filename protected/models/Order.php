<?php
/**
 * Order.php
 * Date: 09.08.13
 * Time: 9:50
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Order extends Model {
    public $id;

    public $code;

    public $country;

    public $index;

    public $region;

    public $city;

    public $address;

    public $fooname;

    public $name;

    public $lastname;

    public $phone;

    public $email;

    public $comment;

    public $delivery_id;

    public $paytype_id;

    public $payment_status;

    public $totalPrice;

    public $attributes = array(
        'id'          => ':id',
        'code'        => ':code',
        'country'     => ':country',
        'index'       => ':index',
        'region'      => ':region',
        'city'        => ':city',
        'address'     => ':address',
        'fooname'     => ':fooname',
        'name'        => ':name',
        'lastname'    => ':lastname',
        'phone'       => ':phone',
        'email'       => ':email',
        'comment'     => ':comment',
        'delivery_id' => ':delivery_id',
        'paytype_id'  => ':paytype_id',
        'totalPrice'  => ':totalPrice',
    );

    /**
     * @param string $className
     *
     * @return Order
     */
    public static function create($className = __CLASS__) {
        return parent::create($className);
    }

    public function attributeLabels() {
        return array(
            'id'          => 'ID',
            'country'     => 'Страна',
            'index'       => 'Почтовый индекс',
            'region'      => 'Регион',
            'city'        => 'Город',
            'address'     => 'Адрес',
            'fooname'     => 'Фамилия',
            'name'        => 'Имя',
            'lastname'    => 'Отчество',
            'phone'       => 'Телефон',
            'email'       => 'Email',
            'comment'     => 'Коментарий',
            'delivery_id' => 'Способ доставки',
            'paytype_id'  => 'Способ оплаты',
        );
    }

    public function validate($attributes = array()) {
        foreach ($this->attributes as $param => $code) {
            $this->$param = trim($this->$param);
        }

        if (empty($this->region)) {
            $this->addError('region', 'Регион не может быть пустым');
        }
        if (empty($this->city)) {
            $this->addError('city', 'Город не может быть пустым');
        }
        if (empty($this->address)) {
            $this->addError('address', 'Адрес не может быть пустым');
        }
        if (empty($this->fooname)) {
            $this->addError('fooname', 'Фамилия не может быть пустым');
        }
        if (empty($this->name)) {
            $this->addError('name', 'Имя не может быть пустым');
        }
        if (empty($this->phone)) {
            $this->addError('phone', 'Телефон не может быть пустым');
        }
        if (empty($this->email)) {
            $this->addError('email', 'Email не может быть пустым');
        }

        if (in_array('delivery_id', $attributes)) {
            $this->delivery_id = (int)$this->delivery_id;
            if (empty($this->delivery_id)) {
                $this->addError('delivery_id', 'Необходимо выбрать способ доставки');
            } elseif (!Delivery::create()->find($this->delivery_id)) {
                $this->addError('delivery_id', 'Такой способ доставки нам не знаком');
            }
        }

        if (in_array('paytype_id', $attributes)) {
            $this->paytype_id = (int)$this->paytype_id;
            if (empty($this->paytype_id)) {
                $this->addError('paytype_id', 'Необходимо выбрать способ оплаты');
            } elseif (!Payment::create()->find($this->paytype_id)) {
                $this->addError('paytype_id', 'Такой способ оплаты нам не знаком');
            }
        }

        if (!filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            $this->addError('email', $this->getAttributeLabel('email') . ' не является правильным email адресом');
        }

        return parent::validate();
    }

    public function save($validate = true) {
        if (!$this->validate()) {
            return false;
        }

        $data = array(
            'ref'      => array(
                'ref' => App::app()->config['ref']['ref'],
                'key' => App::app()->config['ref']['key'],
            ),
            'variants' => CartCookie::create()->getCookie(),
            'order'    => array(),
        );
        foreach ($this->attributes as $attribute => $code) {
            $data['order'][$attribute] = $this->{$attribute};
        }
        $data['order']['address'] = $this->getAddress();
        $data['order']['ip']      = $_SERVER['REMOTE_ADDR'];

        $data = $this->prepareData($data);

        $ch = curl_init('http://www.mmamarket.ru/saveOrder.php');
        curl_setopt_array($ch, array(
                                    CURLOPT_POST           => true,
                                    CURLOPT_POSTFIELDS     => $data,
                                    CURLOPT_RETURNTRANSFER => true,
                               ));
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);

        if (!$result) {
            throw new HttpException(400, 'Некорректный запрос');
        }

        $this->id         = $result->id;
        $this->code       = $result->code;
        $this->totalPrice = $result->totalPrice;

        $return = array(
            'order'    => $this->toArray(),
            'variants' => CartCookie::create()->getCookie(),
        );

        CartCookie::create()->clear();
        CartCookie::create()->setCookie();

        $_SESSION['orders'][$return['order']['code']] = $return;

        return $return;
    }

    protected function prepareData($data) {
        $return = array();
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    $return["{$key}[{$k}]"] = $v;
                }
            }
        }

        return $return;
    }

    public function setAttributes($data) {
        if (isset($data->order_id)) {
            $data->id = $data->order_id;
            unset ($data->order_id);
        }
        parent::setAttributes($data);
    }

    public function saveState() {
        App::app()->startSession();
        $_SESSION['order'] = $this->toArray();
    }

    public function restoreState() {
        App::app()->startSession();
        if (isset($_SESSION['order'])) {
            foreach ($_SESSION['order'] as $param => $value) {
                $this->$param = $value;
            }
        }
    }

    public function cleanState() {
        App::app()->startSession();
        unset($_SESSION['order']);
    }

    public function toArray() {
        $data = array();
        foreach ($this->attributes as $param => $code) {
            $data[$param] = $this->$param;
        }

        return $data;
    }

    /**
     * @return Payment
     */
    public function getPaytype() {
        App::import('//models/Payment');

        return Payment::create()->find($this->paytype_id);
    }

    /**
     * @return Delivery
     */
    public function getDelivery() {
        App::import('//models/Delivery');

        return Delivery::create()->find($this->delivery_id);
    }

    public function getAddress() {
        $address = array();
        if (!empty($this->country)) {
            $address[] = $this->country;
        }
        if (!empty($this->index)) {
            $address[] = $this->index;
        }
        if (!empty($this->region)) {
            $address[] = $this->region;
        }
        if (!empty($this->city)) {
            $address[] = $this->city;
        }
        if (!empty($this->address)) {
            $address[] = $this->address;
        }

        return implode(', ', $address);
    }

    public static function findByCode($code) {
        return self::load($code);
    }

    protected static function load($code) {
        $ch = curl_init(vsprintf('http://www.mmamarket.ru/api/index.php?key=%s&ref=%s&code=%s&request=%s', array(
                                                                                                                App::app()->config['ref']['ref'],
                                                                                                                App::app()->config['ref']['key'],
                                                                                                                $code,
                                                                                                                'order'
                                                                                                           )));
        curl_setopt_array($ch, array(
                                    CURLOPT_FOLLOWLOCATION => true,
                                    CURLOPT_RETURNTRANSFER => true,
                               ));
        $result = curl_exec($ch);
        $result = json_decode($result);
        curl_close($ch);

        if (!$result) {
            throw new HttpException(400, 'Некорректный запрос');
        }

        return $result;
    }

    public function userEmail() {
        $body = App::app()->getController()->renderPartial('order_email', array('model' => $this), true);
    }
}