<?php
/**
 * MenuItem.php
 * Date: 23.07.13
 * Time: 13:29
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class MenuItem extends Model {
    public $id;

    public $title;

    public $url;

    public $position;

    public $doc_id;

    public $blank;

    public $tableName = "menu_items";

    public $attributes = array(
        'id'       => ':id',
        'title'    => ':title',
        'url'      => ':url',
        'position' => ':position',
        'doc_id'   => ':doc_id',
        'blank'    => ':blank',
    );

    /**
     * @param string $className
     *
     * @return MenuItem
     */
    public static function create($className = __CLASS__) {
        return parent::create($className);
    }

    public function attributeLabels() {
        return array(
            'title'    => 'Заголовок',
            'url'      => 'URL',
            'position' => 'Позиция',
            'blank'    => 'Открыть ссылку в новом окне',
        );
    }

    public function validate($attributes = array()) {
        $this->title    = trim($this->title);
        $this->url      = trim($this->url);
        $this->position = (int)$this->position;
        $this->doc_id   = (int)$this->doc_id;
        $this->blank    = (int)$this->blank;

        if (empty($this->title)) {
            $this->addError('title', 'Заголовок не может быть пустым');
        }
        if (empty($this->url)) {
            $this->addError('url', 'URL не может быть пустым');
        }
        if (empty($this->position)) {
            $this->addError('position', 'Позиция не может быть пустым');
        }

        return parent::validate();
    }

    public function maxPosition() {
        $max            = App::app()->db()->fetch("SELECT IF(MAX(`position`) IS NULL, 0, MAX(`position`)) + 10 AS `max` FROM `menu_items`");
        $this->position = $max['max'];
    }

    public static function menuList() {
        $return = array();
        foreach (self::create()->findAllByQuery("SELECT * FROM `menu_items` ORDER BY position") as $model) {
            /** @var $model MenuItem */
            $linkOptions = array();
            if ($model->blank) {
                $linkOptions['target'] = 'blank';
            }

            $return[] = array(
                'label'       => $model->title,
                'url'         => $model->getUrl(),
                'linkOptions' => $linkOptions
            );
        }

        return $return;
    }

    public function getUrl() {
        if (strpos($this->url, '/doc') !== 0) {
            return $this->url;
        }

        return App::app()->createUrl($this->url);
    }
}