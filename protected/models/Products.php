<?php
/**
 * Products.php
 * Date: 25.07.13
 * Time: 15:51
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Products extends Model {

    public $product_id;

    public $url;

    public $category_id;

    public $brand_id;

    public $model;

    public $description;

    public $body;

    public $enabled;

    public $hit;

    public $order_num;

    public $small_image;

    public $large_image;

    public $download;

    public $meta_title;

    public $meta_keywords;

    public $meta_description;

    public $created;

    public $modified;

    public $is_new;

    public $discount;

    public $is_edited;

    public $cells;

    public $delete;

    public $tableName = 'products';

    public $pk = "product_id";

    /**
     * @var Catalog
     */
    private $_category;

    /**
     * @param string $className
     *
     * @return Products
     */
    public static function create($className = __CLASS__) {
        return parent::create($className); // TODO: Change the autogenerated stub
    }

    public function url() {
        return App::app()->createUrl(array('/product/' . ($this->url ? $this->url : $this->product_id)));
    }

    public function category() {
        if (!$this->_category) {
            $this->_category = Catalog::create()->find($this->category_id);
        }

        return $this->_category;
    }

    public function breadcrumbs(&$breadcrumbs) {
        $this->category()->breadcrumbs($breadcrumbs);
        array_pop($breadcrumbs);
        $breadcrumbs[$this->category()->name] = $this->category()->url();
        $breadcrumbs[]                        = $this->model;
    }

    public function variants() {
        App::import('//models/Variants');
        $variants = Variants::create()->findAllByQuery("SELECT
    *
FROM
    `variants`
WHERE `product_id` = :product_id
    AND `stock` > 0
ORDER BY `position`", array(':product_id' => $this->product_id));

        return $variants;
    }

    public function photos() {
        return App::app()->db()->fetchAll("SELECT * FROM `product_fotos` WHERE `product_id` = :product_id", array(':product_id' => $this->product_id));
    }
}