<?php
/**
 * Doc.php
 * Date: 22.07.13
 * Time: 14:21
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Doc extends Model {

    public $id;

    public $title;

    public $alias;

    public $text;

    public $keywords;

    public $addMenu = false;

    public $addMain = false;

    public $attributes = array(
        "id"       => ":id",
        "title"    => ":title",
        "text"     => ":text",
        "keywords" => ":keywords",
        "addMain"  => ":addMain",
        "alias"    => ":alias",
    );

    public $tableName = "doc";

    /**
     * @param string $className
     *
     * @return Doc
     */
    public static function create($className = __CLASS__) {
        return parent::create($className);
    }

    public function attributeLabels() {
        return array(
            'title'    => 'Название',
            'text'     => 'Текст',
            'keywords' => 'Keywords',
            'addMenu'  => 'Добавить в меню',
            'addMain'  => 'Вывести этот текст на главной странице',
            'alias'    => 'URL',
        );
    }

    public function validate($attributes = array()) {
        $this->title = trim($this->title);
        if (empty($this->title)) {
            $this->addError('title', 'Заголовк не может быть пустым');
        }

        $this->alias = trim($this->alias);
        if (empty($this->alias)) {
            $this->addError('alias', 'Алиас не может быть пустым');
        } else {
            $query  = "SELECT COUNT(`id`) AS `count` FROM `doc` WHERE `alias` = :alias AND id != :id";
            $result = App::app()->db()->fetch($query, array(
                                                           ":alias" => $this->alias,
                                                           ':id'    => $this->id
                                                      ));
            if ($result['count']) {
                $this->addError('alias', 'Такой алиас уже занят');
            }
        }

        $this->addMenu = (int)$this->addMenu;
        $this->addMain = (int)$this->addMain;

        return parent::validate();
    }

    public function afterSave() {
        if ($this->addMenu) {
            App::import('//models/MenuItem');
            $menu = MenuItem::create()->findByAttributes(array('doc_id' => $this->id));
            if (!$menu) {
                $menu         = new MenuItem();
                $menu->title  = $this->title;
                $menu->url    = $this->url();
                $menu->doc_id = $this->id;
                $menu->maxPosition();
                $menu->save();
            }
        }
        parent::afterFind();
    }

    public function url() {
        return App::app()->createUrl('/doc/' . $this->alias . '.html');
    }

}