<?php
/**
 * Catalog.php
 * Date: 25.07.13
 * Time: 14:57
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Catalog extends Model {

    public $category_id;

    public $parent;

    public $name;

    public $single_name;

    public $meta_title;

    public $meta_keywords;

    public $meta_description;

    public $description;

    public $url;

    public $image;

    public $order_num;

    public $enabled;

    public $delete;

    public $pk = 'category_id';

    public $tableName = 'categories';

    /**
     * @var Catalog[]
     */
    private $_parents;

    private $_children;

    public $children_count;

    private $_childrenAll;

    private $_childrenAllIds;

    private $_products;

    private $_productsCount;

    /**
     * @param string $className
     *
     * @return Catalog
     */
    public static function create($className = __CLASS__) {
        return parent::create($className);
    }

    public function parents() {
        if ($this->_parents === null) {
            $this->_parents = self::create()->findAllByQuery("SELECT t2.*
FROM
    (
        SELECT
            @r AS _id,
            (
                SELECT
                    @r := parent
                FROM
                    categories
                WHERE
                    category_id = _id
            ) AS parent,
            @l := @l + 1 AS lvl
        FROM
            (
                SELECT
                    @r := :_child,
                    @l := 0
            ) AS vars,
            categories m
        WHERE
            @r != 0
    ) t1
JOIN
    categories t2
    ON
        t1._id = t2.category_id
WHERE
    t2.category_id != :_exclude_child
ORDER BY
    t1.lvl;", array(
                   ":_child"         => $this->category_id,
                   ":_exclude_child" => $this->category_id
              ));
        }

        return $this->_parents;
    }

    public function url() {
        return App::app()->createUrl(array('/catalog/' . ($this->url ? $this->url : $this->category_id)));
    }

    /**
     * @return Catalog[]
     */
    public function children() {
        if (!$this->_children) {
            $this->_children = self::create()->findAllByQuery("SELECT
    `c`.*,
    COUNT(`ch`.`category_id`) `children_count`
FROM
    `categories` `c`
    LEFT OUTER JOIN `categories` `ch`
        ON `ch`.`parent` = `c`.`category_id`
        AND `ch`.enabled = 1
    LEFT OUTER JOIN `products` `p`
        ON `p`.`category_id` = `c`.`category_id`
        AND `p`.enabled = 1
WHERE `c`.`parent` = :parent
    AND `c`.enabled = 1
GROUP BY `c`.`category_id`
ORDER BY `c`.`order_num` ", array(
                                 ":parent" => $this->category_id,
                            ));
        }

        return $this->_children;
    }

    /**
     * @param int $enabled
     *
     * @return Catalog[]
     */
    public function childrenRecursive($enabled = 1) {
        if (!$this->_childrenAll) {
            $this->_childrenAll = $this->children();
            foreach ($this->_childrenAll as $child) {
                $this->_childrenAll = array_merge($this->_childrenAll, $child->childrenRecursive($enabled));
            }
        }

        return $this->_childrenAll;
    }

    public function childrenAllIds($enabled = 1) {
        if (!$this->_childrenAllIds) {
            $ids = array();
            foreach ($this->childrenRecursive($enabled) as $child) {
                $ids[] = $child->category_id;
            }
            $this->_childrenAllIds = $ids;
        }

        return $this->_childrenAllIds;
    }

    public function products($limit = 10, $offset = 0, FilterForm $filter = null) {
        if (!$this->_products) {
            $ids   = $this->childrenAllIds();
            $ids[] = $this->category_id;

            $query = "
SELECT
    `p`.*
FROM
    `products` `p`
    JOIN `variants` `v`
        ON `v`.`product_id` = `p`.`product_id`
    JOIN
        `brands` `b`
        ON `b`.`brand_id` = `p`.`brand_id`
WHERE `p`.`category_id` IN (" . implode(', ', $ids) . ")
    AND `p`.`enabled` = 1
    AND `v`.`stock` > 0
    {$filter->getBrand()}
    {$filter->getSize()}
GROUP BY `p`.`product_id`
LIMIT
    :limit
OFFSET
    :offset
";

            $products = Products::create()->findAllByQuery($query, array_merge($filter->getParams(), array(
                                                                                                          "int:limit"  => $limit,
                                                                                                          "int:offset" => $offset,
                                                                                                     )));

            $this->_products = $products;
        }

        return $this->_products;
    }

    public function productCount(FilterForm $filter) {
        if (!$this->_productsCount) {
            $ids   = $this->childrenAllIds();
            $ids[] = $this->category_id;
            $query = "SELECT
    COUNT(DISTINCT `p`.`product_id`) `count`
FROM
    `products` `p`,
    `variants` `v`,
    `brands` `b`
WHERE `p`.`category_id` IN (" . implode(', ', $ids) . ")
    AND `p`.`enabled` = 1
    AND `v`.`stock` > 0
    AND `v`.`product_id` = `p`.`product_id`
    AND `b`.`brand_id` = `p`.`brand_id`
    {$filter->getBrand()}
    {$filter->getSize()}";

            $count = App::app()->db()->fetch($query, $filter->getParams());

            $this->_productsCount = $count['count'];
        }

        return $this->_productsCount;
    }

    public function breadcrumbs(&$breadcrumbs, $selfUrl = false) {
        /** @var Catalog[] $_breadcrumbs */
        $_breadcrumbs = array_reverse($this->parents());
        foreach ($_breadcrumbs as $breadcrumb) {
            $breadcrumbs[$breadcrumb->name] = $breadcrumb->url();
        }
        if ($selfUrl) {
            $breadcrumbs[$this->name] = App::app()->createUrl($this->url());
        } else {
            $breadcrumbs[] = $this->name;
        }
    }

    public static function fullMenu() {
        $return = array();

        $active = false;
        /** @var CatalogController $controller */
        $controller = App::app()->getController();
        if (App::app()->getController()->id == "product" && App::app()->getController()->action == 'index') {
            $active = $controller->currentCatalogId;
        }
        if (App::app()->getController()->id == "catalog" && App::app()->getController()->action == 'index') {
            $active = $controller->currentCatalogId;
        }

        /** @var Catalog[] $roots */
        $roots = Catalog::create()->findAllByQuery("SELECT
    `c`.*,
    COUNT(`ch`.`category_id`) `children_count`,
    COUNT(`p`.`product_id`) `prod_count`
FROM
    `categories` `c`
    LEFT OUTER JOIN `categories` `ch`
        ON `ch`.`parent` = `c`.`category_id`
        AND `ch`.enabled = 1
        AND `ch`.`category_id` IN
        (SELECT
            `p`.`category_id`
        FROM
            `products` `p`
        WHERE `p`.`enabled` = 1
        GROUP BY `p`.`category_id`)
    LEFT OUTER JOIN `products` `p`
        ON `p`.`category_id` = `c`.`category_id`
        AND `p`.enabled = 1
WHERE `c`.`parent` = 0
    AND `c`.enabled = 1
GROUP BY `c`.`category_id`
ORDER BY `c`.`order_num` ");
        foreach ($roots as $root) {
            $itemOptions = $active == $root->category_id ? array('class' => 'active') : array();
            $return[]    = array(
                'url'         => $root->url(),
                'label'       => $root->name,
                'itemOptions' => $itemOptions,
                'items'       => $root->children_count ? $root->menu($active) : array(),
            );
        }

        return $return;
    }

    public function menu($active) {
        $return = array();

        foreach ($this->children() as $child) {
            $itemOptions = ($active == $child->category_id) ? array('class' => 'active') : array();
            $return[]    = array(
                'url'         => $child->url(),
                'label'       => $child->name,
                'itemOptions' => $itemOptions,
                'items'       => $child->children_count ? $child->menu($active) : array(),
            );
        }

        return $return;
    }

}