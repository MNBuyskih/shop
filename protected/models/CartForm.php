<?php
/**
 * CartForm.php
 * Date: 26.07.13
 * Time: 12:29
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class CartForm extends Model {
    public $count;

    public $productId;

    public $variant;

    function __construct(Products $product) {
        $this->productId = $product->product_id;
    }

    public function set() {
        App::import('//models/CartCookie');
        CartCookie::create()->add($this->variant, $this->count);
    }

}