<?php
/**
 * Variants.php
 * Date: 26.07.13
 * Time: 12:50
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Variants extends Model {

    public $variant_id;

    public $product_id;

    public $sku;

    public $name;

    public $price;

    public $price_opt;

    public $stock;

    public $position;

    public $delete;

    public $tableName = "variants";

    public $pk = "variant_id";

    /**
     * @param string $className
     *
     * @return Variants
     */
    public static function create($className = __CLASS__) {
        return parent::create($className);
    }

    /**
     * @return Products
     */
    public function getProduct() {
        return Products::create()->find($this->product_id);
    }

    public static function data(Catalog $catalog, $start = array()) {
        $catalogIds   = $catalog->childrenAllIds();
        $catalogIds[] = $catalog->category_id;

        $query = "SELECT DISTINCT
    `name`
FROM
    `variants` `v`
    JOIN `products` `p`
        ON `v`.`product_id` = `p`.`product_id`
WHERE `p`.`category_id` IN (" . implode(', ', $catalogIds) . ")
    AND `v`.`stock` > 0
    AND `p`.`enabled` = 1
ORDER BY `v`.`name`";

        return $start + Html::listData(self::create()->findAllByQuery($query), 'name', 'name');
    }
}