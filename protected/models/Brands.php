<?php
/**
 * Brands.php
 * Date: 12.08.13
 * Time: 13:04
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Brands extends Model {
    public $brand_id;

    public $name;

    public $url;

    public $meta_title;

    public $meta_keywords;

    public $meta_description;

    public $description;

    public $delete;

    public $tableName = "brands";

    public $pk = "brand_id";

    /**
     * @param string $className
     *
     * @return Brands
     */
    public static function create($className = __CLASS__) {
        return parent::create($className);
    }

    public static function data(Catalog $catalog, $start = array()) {
        $catalogIds   = $catalog->childrenAllIds();
        $catalogIds[] = $catalog->category_id;

        $query = "SELECT
    `b`.*
FROM
    `brands` `b`
    JOIN `products` `p`
        ON `p`.`brand_id` = `b`.`brand_id`
WHERE `p`.`category_id` IN (" . implode(', ', $catalogIds) . ")
GROUP BY `p`.`brand_id`
ORDER BY `b`.`name`";

        return $start + Html::listData(self::create()->findAllByQuery($query), 'url', 'name');
    }
}