<?php
/**
 * FilterForm.php
 * Date: 12.08.13
 * Time: 12:54
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class FilterForm extends Model {

    public $brand;

    public $size;

    public $params = array();

    function __construct() {
        $this->brand = trim(App::app()->getRequest()->getParam('brand'));
        $this->size  = trim(App::app()->getRequest()->getParam('size'));
    }

    public function getBrand() {
        $brand = "";
        if (!empty($this->brand) && $this->brand != "*") {
            $brand                  = "AND `b`.`url` = :brand";
            $this->params[':brand'] = $this->brand;

        }

        return $brand;
    }

    public function getSize() {
        $size = "";
        if (!empty($this->size) && $this->size != "*") {
            $size                  = "AND `v`.`name` = :size";
            $this->params[':size'] = $this->size;
        }

        return $size;
    }

    public function getParams() {
        return $this->params;
    }
}