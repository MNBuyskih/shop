<?php
/**
 * Payment.php
 * Date: 09.08.13
 * Time: 14:28
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

class Payment extends Model {

    public $payment_method_id;

    public $module;

    public $name;

    public $description;

    public $currency_id;

    public $params;

    public $delete;

    public $enabled;

    public $tableName = "payment_methods";

    public $pk = "payment_method_id";

    public $attributes = array(
        'payment_method_id' => ':payment_method_id',
        'module'            => ':module',
        'name'              => ':name',
        'description'       => ':description',
        'currency_id'       => ':currency_id',
        'params'            => ':params',
        'delete'            => ':delete',
        'enabled'           => ':enabled',
    );

    /**
     * @param string $className
     *
     * @return Payment
     */
    public static function create($className = __CLASS__) {
        return parent::create($className);
    }

    public static function data($deliver_id = null) {
        if ($deliver_id) {
            $models = self::payTypes($deliver_id);
        } else {
            $query = "SELECT
    *
FROM
    payment_methods
WHERE
    enabled = 1";

            $models = self::create()->findAllByQuery($query);
        }

        $data = array();
        foreach ($models as $model) {
            /** @var $model Payment */
            $data[$model->payment_method_id] = $model->name;
        }

        return $data;
    }

    /**
     * @param $delivery_id
     *
     * @return Payment[]
     */
    public static function payTypes($delivery_id) {
        $query  = "SELECT
    *
FROM
    payment_methods
WHERE payment_method_id IN
    (SELECT
        `payment_method_id`
    FROM
        delivery_payments
    WHERE delivery_method_id = :deliver_id)
    AND enabled = 1";
        $params = array(":deliver_id" => $delivery_id);

        return self::create()->findAllByQuery($query, $params);
    }

}