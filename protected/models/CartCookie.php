<?php

/**
 * CartCookie.php
 * Date: 07.08.13
 * Time: 11:00
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */
class CartCookie {

    /**
     * @var CartCookie
     */
    protected static $_instance;

    /**
     * Список товров в корзине
     * <ID варианта> => <Количество>
     *
     * @var array
     */
    protected $storage = array();

    /**
     * Данные о товарах и вариантах, находящихся в корзине
     * <pre>array(
     *      array(
     *          'variant' => <Вариант>,
     *          'product' => <Продукт>,
     *          'count' => <Количество>,
     *      ),
     *      // ...
     * )</pre>
     *
     * @var
     */
    protected $data;

    protected function __construct() {
        $this->init();
    }

    /**
     * Возвращает синглтон объекта CartCookie
     *
     * @return CartCookie
     */
    public static function create() {
        if (!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Метод, вызываемый в конструкторе.
     * Собирает данные из кук и сохраняет в $storage
     */
    public function init() {
        if (isset($_COOKIE['cart'])) {
            $cookie = (array)json_decode($_COOKIE['cart']);
            foreach ($cookie as $key => $value) {
                $this->storage[(int)$key] = (int)$value;
            }
        }
    }

    /**
     * Добавить вариант в корзину
     *
     * @param int $variantId ID варианта
     * @param int $count Количество
     */
    public function add($variantId, $count = 0) {
        $variantId = (int)$variantId;
        $count     = (int)$count;

        $this->storage[$variantId] = $count + $this->getCount($variantId);
        $this->setCookie();
    }

    /**
     * Удалить вариант из корзины
     *
     * @param int $variantId ID варианта
     */
    public function delete($variantId) {
        $variantId = (int)$variantId;
        unset($this->storage[$variantId]);
    }

    /**
     * Очистить корзину
     */
    public function clear() {
        $this->storage = array();
    }

    /**
     * Установить определенное количество товара
     *
     * @param int $variantId ID варианта
     * @param int $count Количество
     */
    public function set($variantId, $count) {
        $variantId = (int)$variantId;
        $count     = (int)$count;

        if (!$count) {
            $this->delete($variantId);
        } else {
            $this->storage[$variantId] = $count;
        }
    }

    /**
     * Количетсво товара в корзине
     *
     * @return int
     */
    public function getTotalCount() {
        return array_sum($this->storage);
    }

    /**
     * Количество определенного товара в корзине
     *
     * @param int $variantId ID варианта
     *
     * @return int
     */
    public function getCount($variantId) {
        return isset($this->storage[$variantId]) ? $this->storage[$variantId] : 0;
    }

    /**
     * Устанавливает куки в браузер
     */
    public function setCookie() {
        $data = null;
        $time = time() - 3600;
        if (count($this->storage)) {
            $data = $this->getCookieJson();
            $time = time() + (60 * 60 * 24 * 30 * 12);
        }

        return $this->_setCookie($data, $time);
    }

    /**
     * Возвращает куки как JSON
     *
     * @return string
     */
    public function getCookieJson() {
        return json_encode($this->storage);
    }

    /**
     * Возвращает всю информацию о куках
     *
     * @return array
     */
    public function getCookie() {
        return $this->storage;
    }

    /**
     * Возвращает полную информацию о товарах и вариантах в корзине
     *
     * @return array
     */
    public function getData() {
        if (empty($this->data)) {
            App::import('//models/Variants');
            App::import('//models/Products');

            $data = array();
            foreach (CartCookie::create()->getCookie() as $variant => $count) {
                /** @var Variants $variant */
                $variant = Variants::create()->find($variant);
                /** @var Products $product */
                $product = Products::create()->find($variant->product_id);

                $data[] = array(
                    'variant' => $variant,
                    'product' => $product,
                    'count'   => $count
                );
            }

            $this->data = $data;
        }

        return $this->data;
    }

    /**
     * Возвращает сумму за все товары в корзине
     *
     * @return int
     */
    public function getTotalSum() {
        $sum = 0;
        foreach ($this->getData() as $data) {
            $sum += $data['variant']->price * $data['count'];
        }

        return $sum;
    }

    /**
     * @param $data
     * @param $time
     *
     * @return bool
     */
    private function _setCookie($data, $time) {
        return setcookie("cart", $data, $time, App::app()->createUrl('/'), '.' . App::app()->getRequest()->getHost());
    }

}