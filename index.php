<?php
/**
 * index.php
 * Date: 22.07.13
 * Time: 9:30
 *
 * @author  M.N.B. <buyskih@gmail.com>
 * @package shop
 */

defined('APP_DEBUG') or define('APP_DEBUG', false);

require_once "protected/library/App.php";
App::createApplication(dirname(__FILE__) . '/protected/config/main.php')->run();